<%@ taglib uri="/WEB-INF/jspwiki.tld" prefix="wiki" %>
<%@ page import="com.ecyrd.jspwiki.*" %>
<%@ page import="com.ecyrd.jspwiki.auth.*" %>
<%@ page import="com.ecyrd.jspwiki.auth.user.*" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>

<%
  /* see commonheader.jsp */
  String prefDateFormat = (String) session.getAttribute("prefDateFormat");
  String creationDate ="";
  String modificationDate ="";
%>
<wiki:UserProfile property="exists">
<%  
  /* dateformatting not yet supported by wiki:UserProfile tag - diy */
  WikiContext wikiContext = WikiContext.findContext(pageContext);

  UserManager manager = wikiContext.getEngine().getUserManager();
  UserProfile profile = manager.getUserProfile( wikiContext.getWikiSession() );

  SimpleDateFormat fmt = new SimpleDateFormat( prefDateFormat );
  Date cd = profile.getCreated();
  if( cd != null )
  {
    creationDate = fmt.format( cd ) ;  
  }
  Date md = profile.getLastModified();
  if( md != null )
  {
    modificationDate = fmt.format( md ) ;  
  }
%>
</wiki:UserProfile>

<wiki:Permission permission="editProfile">
<form action="<wiki:Link jsp='UserPreferences.jsp' format='url'><wiki:Param name='tab' value='userProfile'/></wiki:Link>"
          id="editProfile"
       class="wikiform"
     onsubmit="return WikiForm.submitOnce( this );"
      method="POST" accept-charset="<wiki:ContentEncoding />" >

      <h3>
      <wiki:UserProfile property="exists">Update your wiki profile here.</wiki:UserProfile>
      <wiki:UserProfile property="new">
        <p>Create a new user here.</p>
        <p>
          <strong>NOTE:</strong> After registering, you must contact
          <a href="mailto:wiki@glassfish.dev.java.net">wiki@glassfish.dev.java.net</a>
          and request that write access be granted to your account.  We apologize for
          this inconvenience, but it is a necessary step to prevent spam.
        </p>
      </wiki:UserProfile>
      </h3>
      <wiki:Messages div="error" topic="profile" prefix="Could not save profile: "/>
      <wiki:Messages action="clear" /> 
   <table>
   
     <!-- Login name -->
     <tr>
       <td><label for="loginname">Login name</label></td>
       <td>
         <wiki:UserCheck status="customAuth">
           <wiki:UserProfile property="exists">
             <div class="formvalue"><wiki:UserProfile property="loginname"/></div>
           </wiki:UserProfile>
           <wiki:UserProfile property="new">
           <input type="text" name="loginname" id="loginname" 
                  size="20" value="<wiki:UserProfile property='loginname' />" />                  
           </wiki:UserProfile>
           <div class="formhelp">
           This is your login id; once set, it cannot be changed.
           It is only used for authentication, not for page access control.
           </div>
         </wiki:UserCheck>
       </td>
     </tr>

     <!-- Password; not displayed if container auth used -->
     <wiki:UserCheck status="setPassword">
       <tr>
         <td><label for="password">
               <wiki:UserProfile property="exists">Change Password</wiki:UserProfile>
               <wiki:UserProfile property="new">Set Password</wiki:UserProfile>
         </td>
         <td>
            <%--FIXME Old PW not yet treated by JSPWiki
            <label for="password0">Old</label>&nbsp;
            <input type="password" name="password0" id="password0" size="20" value="" />
            
            &nbsp;&nbsp;--%><label for="password">New</label>&nbsp;
            <input type="password" name="password" id="password" size="20" value="" />
            &nbsp;&nbsp;<label for="password2">Verify</label>&nbsp;
            <input type="password" name="password2" id="password2" size="20" value="" />
            <div class="formhelp">
            <wiki:UserProfile property="exists">
            Change your password. It may not be blank.
            </wiki:UserProfile>
            <wiki:UserProfile property="new">
            Sets your password. It may not be blank.
            </wiki:UserProfile>
            <%-- extra validation ? min size, allowed chars? --%>
            </div>
         </td>
       </tr>
     </wiki:UserCheck>

     <!-- Wiki name -->
     <tr>
       <td><label for="wikiname">Wiki name</label></td>
       <td>
         <wiki:UserProfile property="exists">
           <div class="formvalue">
             <wiki:Translate>[<wiki:UserProfile property='wikiname' />]</wiki:Translate>
           </div>
         </wiki:UserProfile>
         <wiki:UserProfile property="new">
           <input type="text" name="wikiname" id="wikiname" size="20" value="" />
         </wiki:UserProfile>
         <div class="formhelp">
         This must be a proper WikiName (no spaces or punctuation).  Many people use a FirstLast format for their wikiName (e.g. "JohnDoe").
         <wiki:UserProfile property="exists">
         Your personal favorites are in 
         <wiki:Translate>[<wiki:UserProfile property='wikiname' />Favorites]</wiki:Translate>
         </wiki:UserProfile>
         </div>
       </td>
     </tr>
     
     <!-- Full name -->
     <tr>
       <td><label for="fullname">Full name</label></td>
       <td>
         <wiki:UserProfile property="exists">
           <div class="formvalue"><wiki:UserProfile property="fullname"/></div>
         </wiki:UserProfile>
         <wiki:UserProfile property="new">
           <input type="text" name="fullname" id="fullname"
                  size="20" value="<wiki:UserProfile property='fullname'/>" />
         </wiki:UserProfile>
         <%-- trivial help
         <div class="formhelp">This is your full name.</div>
         --%>
       </td>
     </tr>

     <!-- E-mail -->
     <tr>
       <td><label for="email">E-mail address</label></td>
       <td>
         <input type="text" name="email" id="email" 
                size="20" value="<wiki:UserProfile property='email' />" />
         <div class="formhelp">
         Your e-mail address is optional. In the future, it will be used
         by JSPWiki for resetting lost passwords.
         </div>
       </td>
     </tr>
     
     <%-- additional profile info --%>
     <wiki:UserProfile property="exists">
     <tr>
       <td><label>Roles</label></td>
       <td>
         <div class="formvalue"><wiki:UserProfile property="roles" /></div>
         <div class="formhelp">You are member of these roles.</div>
       </td>
     </tr>
     <tr>
       <td><label>Groups</label></td>
       <td>
         <div class="formvalue"><wiki:UserProfile property="groups" /></div>
         <div class="formhelp">You are member of these groups.</div>
       </td>
     </tr>

     <%-- Unfortunately we can't specify the format of the dates here :-( --%>
     <tr>
       <td><label>Creation date</label></td>
       <td class="formvalue"><%= creationDate %><%--<wiki:UserProfile property="created"/>--%></td>
     </tr>
     <tr>
       <td><label>Last modified</label></td>
       <td class="formvalue"><%= modificationDate %><%--<wiki:UserProfile property="modified"/>--%></td>
     </tr>
     </wiki:UserProfile>
     
     
   </table>
     
   <p>
   <wiki:UserProfile property="exists">
     <input type="submit" value="Save User Profile" name="ok" style="display:none;" />
     <input type="button" value="Save User Profile" name="proxy1" onclick="this.form.ok.click();" />
   </wiki:UserProfile>
   <wiki:UserProfile property="new">
     <input type="submit" value="Create User Profile" name="ok" style="display:none;" />
     <input type="button" value="Create User Profile" name="proxy1" onclick="this.form.ok.click();" />
   </wiki:UserProfile>
   <input type="hidden" name="action" value="saveProfile" />
   </p>
   
</form>
</wiki:Permission> 
