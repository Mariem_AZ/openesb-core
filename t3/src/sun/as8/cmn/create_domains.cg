#
# BEGIN_HEADER - DO NOT EDIT
# 
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)create_domains.cg - ver 1.1 - 01/04/2006
#
# Copyright 2004-2006 Sun Microsystems, Inc. All Rights Reserved.
# 
# END_HEADER - DO NOT EDIT
#

#create standard domains for jbi regression tests:

%ifndef FIXTREE_DEFS_INCLUDED %exit ${CG_INFILE}: you must include fixtree.defs to run this script.

#these are the appserver default port numbers:
HTTP_LISTENER_1 =	8080
HTTP_LISTENER_2 =	1043
HTTP_LISTENER_3 =	1053
ADMIN_LISTENER =	4848
ORB_LISTENER_1 =	3700
IIOP_LISTENER_1 =	1060
IIOP_LISTENER_2 =	1070
JMS_ADMIN_PORT =	8899
JMS_ADMIN_HOST =	localhost
JMX_PORT =	5555
JMX_CONNECTOR_PORT =	8686

#html adaptor port is enabled in set_domain_offset (fixtree.defs).
%undef HTML_ADAPTOR_PORT

%ifndef DOMAIN_LIST %echo Usage:  ${CG_INFILE}: domain...
%ifndef DOMAIN_LIST %halt 1

create_a_domain := << EOF
#create a single domain
{
    #note - set_domain_offset is defined in fixtree.defs.
    %call set_domain_offset

    #note - call check_domain_names first to prevent this error:
    %if $BAD_DOMAIN_NAME %eecho create_a_domain: bad domain name, $domain_name - ABORT
    %if $BAD_DOMAIN_NAME %halt 1

    %include create_a_domain.cg

    #next domain:
    %shift domain_name STACK_TMP
}
EOF

#install the domains:
STACK_TMP = $DOMAIN_LIST
%shift domain_name STACK_TMP
%whiledef domain_name %call create_a_domain
