/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JMSBindingLifeCycle.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.jms;

import com.sun.jbi.StringTranslator;

import com.sun.jbi.binding.jms.config.Config;
import com.sun.jbi.binding.jms.config.ConfigConstants;

import com.sun.jbi.binding.jms.deploy.EndpointRegistry;
import com.sun.jbi.binding.jms.deploy.JMSBindingSUManager;

import com.sun.jbi.binding.jms.framework.WorkManager;

import com.sun.jbi.binding.jms.mq.MQManager;

import java.io.File;

import java.util.logging.Logger;

import javax.jbi.JBIException;

import javax.jbi.component.ComponentContext;

import javax.jbi.messaging.DeliveryChannel;
import javax.jbi.messaging.MessagingException;
import java.net.URI;

/**
 * This is the Lifecycle class of the JMS Binding.   The init and the start()
 * methods of this class  are invoked by   the framework when the component is
 * started.
 *
 * @author Sun Microsystems Inc.
 * @version
 */
public class JMSBindingLifeCycle
    implements javax.jbi.component.ComponentLifeCycle, JMSBindingResources
{
    /**
     * Compoent context.
     */
    private ComponentContext mContext = null;

    /**
     * Binding Channel Object.
     */
    private DeliveryChannel mChannel;

    /**
     * Endpoint manager.
     */
    private EndpointManager mManager;

    /**
     * Registry.
     */
    private EndpointRegistry mRegistry;

    /**
     * JMS binding context.
     */
    private JMSBindingContext mJMSContext;
    /**
     * Resolver object.
     */
    private JMSBindingResolver mResolver;

    /**
     * Service unit manager.
     */
    private JMSBindingSUManager mSUManager = null;

    /**
     * Receiver.
     */
    private JMSReceiver mJMSReceiver;

    /**
     * Logger Object.
     */
    private Logger mLogger;

    /**
     * Helper for i18n.
     */
    private StringTranslator mStringTranslator;

    /**
     * Receiver thread.
     */
    private Thread mReceiverThread;

    /**
     * Flag to hold the result of init.
     */
    private boolean mInitSuccess = false;

    /**
     * Get the ObjectName for any MBean that is provided for managing this
     * binding. In this case, there is none, so a null is returned.
     *
     * @return ObjectName is always null.
     */
    public javax.management.ObjectName getExtensionMBeanName()
    {
        return null;
    }

    /**
     * Resolver object.
     *
     * @param resolver  resolver implementation.
     */
    public void setResolver(JMSBindingResolver resolver)
    {
        mResolver = resolver;
    }

    /**
     * Sets the su manager.
     *
     * @param manager  su manager implementation.
     */
    public void setSUManager(JMSBindingSUManager manager)
    {
        mSUManager = manager;
    }

    /**
     * Initialises the life cycle.
     *
     * @param jbiContext Component context.
     *
     * @throws javax.jbi.JBIException exception.
     */
    public void init(javax.jbi.component.ComponentContext jbiContext)
        throws javax.jbi.JBIException
    {
        Logger logger = jbiContext.getLogger("", null);
        init(jbiContext, logger);
    }

    /**
     * Initialize the JMS BC. This performs initialization required by the  JMS
     * BC but does not make it ready to process messages.  This method is
     * called immediately after installation of the JMS BC.  It is also called
     * when the JBI framework is starting up, and any time  the BC is being
     * restarted after previously being shut down through  a call to
     * shutdown().
     *
     * @param jbiContext the JBI environment context
     * @param logger logger object.
     *
     * @throws javax.jbi.JBIException if the BC is unable to initialize.
     */
    public void init(
        javax.jbi.component.ComponentContext jbiContext,
        Logger logger)
        throws javax.jbi.JBIException
    {
        mLogger = logger;

        try
        {
            if (null == jbiContext)
            {
                throw new javax.jbi.JBIException(mStringTranslator.getString(
                        JMS_INVALID_BINDINGCONTEXT));
            }

            mJMSContext = JMSBindingContext.getInstance();
            mJMSContext.setContext(jbiContext);
            mJMSContext.setLogger(logger);
            mContext = jbiContext;
            mStringTranslator =
                mJMSContext.getStringTranslator();
            mLogger.info(mStringTranslator.getString(JMS_INIT_START));
            mRegistry = new EndpointRegistry();

            MessageRegistry mMessageRegistry = new MessageRegistry();
            mJMSContext.setMessageRegistry(mMessageRegistry);
            mJMSContext.setRegistry(mRegistry);
            mManager = new EndpointManager();
            mJMSContext.setEndpointManager(mManager);
            mSUManager.setValues(mContext, mManager, mResolver);

            Config config =
                new Config(getConfigFileName(jbiContext.getInstallRoot()));

            // Initialize the configuration object
            config.loadConfiguration(jbiContext.getComponentName());

            if (!config.isValid())
            {
                mLogger.severe(mStringTranslator.getString(JMS_INVALID_CONIFG_INFO));
                throw new Exception(mStringTranslator.getString(JMS_INVALID_CONIFG_INFO)
                    + "\n" + config.getError());
            }

            mLogger.setLevel(config.getLogLevel());
            config.printDetails();
            mJMSContext.setConfig(config);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new javax.jbi.JBIException(mStringTranslator.getString(
                    JMS_INIT_FAILED), e);
        }

        /* This a guard variable to check if init is a success
         * Nothing should be done in start in case init is a failure
         */
        mInitSuccess = true;
    }

    /**
     * Shutdown the BC. This performs cleanup before the BC is terminated. Once
     * this has been called, init() must be called before the BC can be
     * started again with a call to start().
     *
     * @throws javax.jbi.JBIException if the BC is unable to shut down.
     */
    public void shutDown()
        throws javax.jbi.JBIException
    {
        mLogger.info(mStringTranslator.getString(JMS_SHUTDOWN_BEGIN));
        mSUManager = null;
        if (mRegistry != null)
        {
            mRegistry.clearEndpoints();
        }
        mLogger.info(mStringTranslator.getString(JMS_SHUTDOWN_END));
    }

    /**
     * Start the JMS BC. This makes the JSM BC ready to process messages.  This
     * method is called after init() completes when the JBI framework is
     * starting up, and when the BC is being restarted after a previous call
     * to shutdown(). If stop() was called previously but shutdown() was not,
     * start() can be called without a call to init().
     *
     * @throws javax.jbi.JBIException if the BC is unable to start.
     * @throws JBIException
     */
    public void start()
        throws javax.jbi.JBIException
    {
        mLogger.info(mStringTranslator.getString(JMS_START_BEGIN));
        
        /* check if init is success */
        if (!mInitSuccess)
        {
            mLogger.severe(mStringTranslator.getString(JMS_INIT_FAILED));

            return;
        }

        String compRoot = mContext.getInstallRoot();
        String schemaFile =
            compRoot + File.separatorChar + "schema" + File.separatorChar
            + ConfigConstants.DEPL_CONFIG_SCHEMA_FILE;

        if (compRoot == null)
        {
            mLogger.severe(mStringTranslator.getString(JMS_INVALID_COMPROOT));

            return;
        }

        MQManager man = null;
        
        String provurl = mJMSContext.getConfig().getProviderUrl();
        String initctx = mJMSContext.getConfig().getInitialContextFactory();
        try
        {
                     
            File f = null;
            URI  furi = null;
            try
            {
                furi = URI.create(provurl);
            }
            catch (Exception e)
            {
                ;
            }
            if (furi != null)
            {             
                if (furi.getScheme().equals("file"))
                {
                    f = new File(furi);                
                    if (!f.isDirectory())
                    {
                        /*
                        throw new JBIException(mStringTranslator.getString(
                                    JMS_OBJECTSTORE_NOT_FOUND, provurl));
                         */
                        File f1 = new File(compRoot + File.separatorChar + "imqobjects");
                        f1.mkdir();
                        mLogger.warning(mStringTranslator.getString(
                                    JMS_OBJECTSTORE_NOT_FOUND, provurl));                        
                        mLogger.warning(mStringTranslator.getString(
                                    JMS_DEFAULT_OBJECT_STORE, f1.getAbsolutePath()));
                        provurl = f1.toURI().toString();
                        
                    }
                }
            }
        }
        catch (Exception e)
        {
            /*
            throw new JBIException(mStringTranslator.getString(
                            JMS_OBJECTSTORE_NOT_FOUND, provurl));
             */
            mLogger.warning(mStringTranslator.getString(
                            JMS_OBJECTSTORE_NOT_FOUND, provurl));
        }
        
        
        man = new MQManager(mJMSContext.getConfig().getInitialContextFactory(),
                provurl,
                mJMSContext.getConfig().getSecurityPrincipal(),
                mJMSContext.getConfig().getSecurityCredentials(),
                mJMSContext.getConfig().getSecurityLevel());

        if (!man.init())
        {
            throw new JBIException(man.getError());
        }

        mJMSContext.setMQManager(man);

        if (man == null)
        {
            mLogger.severe(mStringTranslator.getString(JMS_INITIAL_CONTEXT_ERROR));
            throw new JBIException(mStringTranslator.getString(
                            JMS_INITIAL_CONTEXT_ERROR));
        }

        try
        {
            mChannel = mContext.getDeliveryChannel();
        }
        catch (MessagingException me)
        {
            mLogger.severe(mStringTranslator.getString(
                    JMS_BINDINGCHANNEL_FAILED) + me.getMessage());

            return;
        }

        WorkManager jmsworkman =
            WorkManager.getWorkManager(ConfigConstants.JMS_WORK_MANAGER);

        try
        {
            jmsworkman.setMinThreads(mJMSContext.getConfig().getMinThreadCount());
            jmsworkman.setMaxThreads(mJMSContext.getConfig().getMinThreadCount());
            jmsworkman.start();
            mJMSContext.setJMSWorkManager(jmsworkman);

            mManager.setChannel(mChannel);
            mManager.setConnectionManager(man);
            mRegistry.init();

            mJMSReceiver = new JMSReceiver(mChannel);
            mReceiverThread = new Thread(mJMSReceiver);
            mReceiverThread.start();
        }
        catch (Throwable t)
        {
            t.printStackTrace();
            throw new JBIException("**CANNOT START JMS BINDING**");
        }

        mLogger.info(mStringTranslator.getString(JMS_START_END));
    }

    /**
     * Stop the BC. This makes the BC stop accepting messages for processing.
     * After a call to this method, start() can be called again without first
     * calling init().
     *
     * @throws javax.jbi.JBIException if the BC is unable to stop.
     */
    public void stop()
        throws javax.jbi.JBIException
    {
        mLogger.info(mStringTranslator.getString(JMS_STOP_BEGIN));

        try
        {
            if (mJMSReceiver != null)
            {
                mJMSReceiver.stopReceiving();
            }

            if (mChannel != null)
            {
                mChannel.close();
            }    
            if (mManager != null)
            {
                mManager.releaseTemporarySession();
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        mLogger.info(mStringTranslator.getString(JMS_STOP_END));
    }

    /**
     * Creates the configuration file name. 
     *
     * @param componentRoot - component root directory.
     *
     * @return the configuration file name.
     */
    private String getConfigFileName(String componentRoot)
    {
        return (componentRoot + ConfigConstants.JMS_CONFIG_FILE_NAME);
    }
}
