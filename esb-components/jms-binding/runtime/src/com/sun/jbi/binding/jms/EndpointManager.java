/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EndpointManager.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.jms;

import com.sun.jbi.StringTranslator;

import com.sun.jbi.binding.jms.deploy.EndpointRegistry;

import com.sun.jbi.binding.jms.mq.MQConnection;
import com.sun.jbi.binding.jms.mq.MQDestination;
import com.sun.jbi.binding.jms.mq.MQManager;
import com.sun.jbi.binding.jms.mq.MQSession;

import com.sun.jbi.binding.jms.config.ConfigConstants;
import com.sun.jbi.binding.jms.util.UtilBase;

import java.util.Collection;
import java.util.Iterator;

import java.util.logging.Logger;


import javax.jbi.messaging.DeliveryChannel;

import javax.jbi.servicedesc.ServiceEndpoint;

import javax.xml.namespace.QName;

/**
 * Manages all the stopping and starting of endpoints.
 *
 * @author Sun Microsystems Inc.
 */
public class EndpointManager
    extends UtilBase
    implements JMSBindingResources
{
    /**
     * Sleep time for thread.
     */
    private static final long SLEEP_TIME = 50;

    /**
     * Binding Channel.
     */
    private DeliveryChannel mChannel;

    /**
     * Deployment Registry.
     */
    private EndpointRegistry mRegistry;

    /**
     * Logger object.
     */
    private Logger mLogger;
    /**
     * MQ Manager.
     */
    private MQManager mManager;

    /**
     * Helper for i18n.
     */
    private StringTranslator mStringTranslator;
    /**
     * Session thread.
     */
    private Thread mTmpSessionThread;

    /**
     * Creates a new EndpointManager object.
     */
    public EndpointManager()
    {
        mRegistry = JMSBindingContext.getInstance().getRegistry();
        mLogger = JMSBindingContext.getInstance().getLogger();
        mStringTranslator =
            JMSBindingContext.getInstance().getStringTranslator();
    }

    /**
     * Sets the channel.
     *
     * @param bc binding channel.
     */
    public void setChannel(DeliveryChannel bc)
    {
        mChannel = bc;
    }

    /**
     * Sets the connection manager.
     *
     * @param man manager.
     */
    public void setConnectionManager(MQManager man)
    {
        mManager = man;
    }

    /**
     * Returns the number of endpoints actually running.
     *
     * @return int number of running endpoints.
     */
    public int getRunningEndpointCount()
    {
        int count = 0;

        return count;
    }

    /**
     * Stops all running endpoints.
     */
    public void startAllEndpoints()
    {
        /*  We have to wait till stop timeout configured in
         * the configuration file, if we cannot stop the endpoints within
         * that time we quit
         */
        Collection eps = mRegistry.getAllEndpoints();

        Iterator iter = eps.iterator();

        while (iter.hasNext())
        {
            EndpointBean eb = (EndpointBean) iter.next();

            if (eb == null)
            {
                mLogger.severe(mStringTranslator.getString(
                        JMS_START_ENDPOINT_FAILED));
                setError(mStringTranslator.getString(
                        JMS_START_ENDPOINT_FAILED));

                continue;
            }

            if (!startEndpoint(eb))
            {
                setError("Cannot start endpoint " + eb.getUniqueName());
                stopEndpoint(eb);

                continue;
            }

            if (eb.getRole() == ConfigConstants.PROVIDER)
            {
                if (!startOutboundEndpoint(eb))
                {
                    setError("Cannot start outbound endpoint "
                        + eb.getUniqueName() + " in deployment ");

                    //stop endpoint
                    continue;
                }
            }
            else if (eb.getRole() == ConfigConstants.CONSUMER)
            {
                if (!startInboundEndpoint(eb))
                {
                    setError("Cannot start inbound endpoint "
                        + eb.getUniqueName() + " in deployment ");
                    stopInboundEndpoint(eb);

                    // stop endpoint here 
                    continue;
                }
            }
        }
    }

    /**
     * Starts a deployment.
     *
     * @param asid service unit id.
     */
    public void startDeployment(String asid)
    {
        mLogger.info(mStringTranslator.getString(JMS_START_DEPLOYMENT, asid));
        super.clear();

        Collection l = mRegistry.getEndpoints(asid);
        Iterator iter = l.iterator();

        while (iter.hasNext())
        {
            EndpointBean eb = (EndpointBean) iter.next();

            if (eb == null)
            {
                mLogger.severe(mStringTranslator.getString(
                        JMS_START_DEPLOYMENT_FAILED, asid));
                mLogger.severe(mStringTranslator.getString(
                        JMS_START_DEPLOYMENT_FAILED_BEANNULL, asid));
                setError(mStringTranslator.getString(
                        JMS_START_DEPLOYMENT_FAILED_BEANNULL, asid));

                continue;
            }

            if (!startEndpoint(eb))
            {
                setError("Cannot start endpoint " + eb.getUniqueName());
                stopEndpoint(eb);

                continue;
            }

            if (eb.getRole() == ConfigConstants.PROVIDER)
            {
                if (!startOutboundEndpoint(eb))
                {
                    setError("Cannot start outbound endpoint "
                        + eb.getUniqueName() + " in deployment " + asid);

                    //stop endpoint
                    continue;
                }
            }
            else if (eb.getRole() == ConfigConstants.CONSUMER)
            {
                if (!startInboundEndpoint(eb))
                {
                    setError("Cannot start inbound endpoint "
                        + eb.getUniqueName() + " in deployment " + asid);
                    stopInboundEndpoint(eb);

                    // stop endpoint here 
                    continue;
                }

                mLogger.info(mStringTranslator.getString(
                        JMS_ACTIVATE_INBOUND_SUCCESS, eb.getUniqueName()));
            }
        }

        mLogger.info(mStringTranslator.getString(JMS_START_DEPLOYMENT_SUCCESS,
                asid));
    }

    /**
     * Sraterts the endpoint.
     *
     * @param eb endpoint bean.
     *
     * @return true of started.
     */
    public boolean startEndpoint(EndpointBean eb)
    {
        MQConnection con =
            mManager.createConnection(eb.getValue(
                    ConfigConstants.CONNECTION_FACTORY),
                eb.getValue(ConfigConstants.CONNECTION_USER_ID),
                eb.getValue(ConfigConstants.CONNECTION_PASSWORD), eb.getStyle());

        if (con == null)
        {
            mLogger.severe("Cannot create connection using "
                + eb.getValue(ConfigConstants.CONNECTION_FACTORY));
            setError(mManager.getError() + "\n"
                + (String) eb.getValue(ConfigConstants.CONNECTION_FACTORY));

            return false;
        }

        if (mManager.destinationExists(eb.getValue(
                        ConfigConstants.DESTINATION_NAME)))
        {
            if (eb.getStyle() == ConfigConstants.QUEUE)
            {
                mLogger.severe(
                    "Destination name already exisst for a different endpoint");
                mManager.closeConnection(con);
                setError("Desintination already exists ");

                return false;
            }
        }

        MQDestination destination =
            mManager.getDestination(eb.getValue(ConfigConstants.DESTINATION_NAME));

        if (destination == null)
        {
            mLogger.severe("Cannot get destination using "
                + eb.getValue(ConfigConstants.DESTINATION_NAME));
            setError("Desintination cannot be created ");
            setError(mManager.getError());

            return false;
        }

        eb.setConnection(con);
        eb.setDestination(destination);
        return true;
    }

    /**
     * Start inbound endpoint.
     *
     * @param eb endpoint bean.
     *
     * @return true if started.
     */
    public boolean startInboundEndpoint(EndpointBean eb)
    {
        ServiceEndpoint ref = null;
        MQSession session = mManager.getSession(false, eb);
        
        mLogger.fine("Starting inbound Endpoint " + eb.getUniqueName());
        try
        {
            session.init();
        }
        catch (Exception e)
        {
            setError("Cannot initialise session ");
            setError(session.getError());

            return false;
        }

        eb.getConnection().start();

        if (!eb.getConnection().isValid())
        {
            setError(eb.getConnection().getError());

            return false;
        }

        if (!session.setReceiver())
        {
            setError(session.getError());
            mManager.releaseSession(session);

            return false;
        }

        Thread t = new Thread(session);
        eb.setReceiverSession(session);
        eb.setReceiverThread(t);
        t.start();

        mLogger.fine("Started inbound Endpoint " + eb.getUniqueName());
        return true;
    }

    /**
     * Starts the outbound endpoint. 
     *
     * @param eb endpoint bean.
     *
     * @return true if started.
     */
    public boolean startOutboundEndpoint(EndpointBean eb)
    {
        // TO DO other tasks
        ServiceEndpoint ref = null;
        mLogger.fine("Starting Outbount Endpoint " + eb.getUniqueName());

        try
        {
            ref = JMSBindingContext.getInstance().getContext().activateEndpoint(new QName(
                        eb.getValue(ConfigConstants.SERVICE_NAMESPACE),
                        eb.getValue(ConfigConstants.SERVICENAME)),
                    eb.getValue(ConfigConstants.ENDPOINTNAME));
            mLogger.info(mStringTranslator.getString(
                    JMS_ACTIVATE_OUTBOUND_SUCCESS, eb.getUniqueName()));
        }
        catch (javax.jbi.JBIException me)
        {
            mLogger.severe(mStringTranslator.getString(
                    JMS_ACTIVATE_OUTBOUND_FAILED, eb.getUniqueName()));
            mLogger.severe(me.getMessage());
            setError(me.getMessage());

            return false;
        }

        if (mTmpSessionThread == null)
        {
            MQSession tmpsession = mManager.getTemporarySession();

            mLogger.info("temp session is " + tmpsession);

            if ((!mManager.isValid()) || (tmpsession == null))
            {
                setError(mManager.getError());

                return false;
            }

            try
            {
                mLogger.info("initing temp session is " + tmpsession);
                tmpsession.init();
            }
            catch (Exception e)
            {
                setError(mManager.getError());

                return false;
            }

            if (!tmpsession.setReceiver())
            {
                mLogger.info("Setting receiver " + tmpsession);
                setError(tmpsession.getError());

                return false;
            }

            mTmpSessionThread = new Thread(tmpsession);
            mTmpSessionThread.start();
        }

        eb.setServiceEndpoint(ref);
        eb.getConnection().start();

        mLogger.fine("Started Outbount Endpoint " + eb.getUniqueName());
        return true;
    }

    /**
     * Stops all running endpoints.
     */
    public void stopAllEndpoints()
    {
        /*  We have to wait till stop timeout configured in
         * the configuration file, if we cannot stop the endpoints within
         * that time we quit
         */
        Collection eps = mRegistry.getAllEndpoints();

        Iterator iter = eps.iterator();

        while (iter.hasNext())
        {
            EndpointBean eb = (EndpointBean) iter.next();

            if (eb != null)
            {
                stopEndpoint(eb);

                if (eb.getRole() == ConfigConstants.CONSUMER)
                {
                    stopInboundEndpoint(eb);
                }
                else if (eb.getRole() == ConfigConstants.PROVIDER)
                {
                    stopOutboundEndpoint(eb);
                }
            }
        }

        if (mTmpSessionThread != null)
        {
            mManager.releaseTemporarySession();
	    mTmpSessionThread = null;
        }
    }    
    
    /**
     * Releases the temporary session when the binding stops
     *
     */
    
    public void releaseTemporarySession()
    {
        if (mTmpSessionThread != null)
        {
            mManager.releaseTemporarySession();
	    mTmpSessionThread = null;
        }        
    }
    
    /**
     * Stops a deployment.
     *
     * @param asid Application Sub assembly ID.
     */
    public void stopDeployment(String asid)
    {
        mLogger.info(mStringTranslator.getString(JMS_STOP_DEPLOYMENT, asid));
        super.clear();

        Collection eps = mRegistry.getEndpoints(asid);

        Iterator iter = eps.iterator();

        while (iter.hasNext())
        {
            EndpointBean eb = (EndpointBean) iter.next();

            if (eb != null)
            {
                stopEndpoint(eb);

                if (eb.getRole() == ConfigConstants.CONSUMER)
                {
                    stopInboundEndpoint(eb);
                }
            }
        }

        mLogger.info(mStringTranslator.getString(JMS_STOP_DEPLOYMENT_SUCCESS,
                asid));
    }

    /**
     * Stop an endpoint.
     *
     * @param eb endpoint bean.
     */
    private void stopEndpoint(EndpointBean eb)
    {
        MQConnection con = eb.getConnection();

        if (con != null)
        {
            mManager.closeConnection(con);

            if (!con.isValid())
            {
                setWarning(con.getError());
            }
        }

        MQDestination dest = eb.getDestination();
        mManager.closeDestination(dest);
        dest = null;
    }

    /**
     * Stops the inbound endpoint. 
     *
     * @param eb endpoint bean.
     */
    private void stopInboundEndpoint(EndpointBean eb)
    {
        MQSession session = eb.getReceiverSession();

        if (session != null)
        {
            session.stopReceiving();
        }

        mManager.releaseSession(session);
    }

    /**
     * Stops an outbound endpoint.
     *
     * @param eb endpoint bean.
     */
    private void stopOutboundEndpoint(EndpointBean eb)
    {
        ServiceEndpoint ref = eb.getServiceEndpoint();

        try
        {
            if (ref != null)
            {
                JMSBindingContext.getInstance().getContext().deactivateEndpoint(ref);
            }

            mLogger.info(mStringTranslator.getString(
                    JMS_STOP_ENDPOINT_SUCCESS, eb.getUniqueName()));
        }
        catch (javax.jbi.JBIException me)
        {
            mLogger.severe(mStringTranslator.getString(
                    JMS_STOP_ENDPOINT_FAILED, eb.getUniqueName()));
            mLogger.severe(me.getMessage());
            setWarning(me.getMessage());
        }
    }
}
