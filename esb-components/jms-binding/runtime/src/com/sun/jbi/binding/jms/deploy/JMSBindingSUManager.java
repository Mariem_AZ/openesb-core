/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JMSBindingSUManager.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.jms.deploy;

import com.sun.jbi.StringTranslator;

import com.sun.jbi.binding.jms.EndpointBean;
import com.sun.jbi.binding.jms.EndpointManager;
import com.sun.jbi.binding.jms.EndpointStatus;
import com.sun.jbi.binding.jms.JMSBindingContext;
import com.sun.jbi.binding.jms.JMSBindingResolver;
import com.sun.jbi.binding.jms.JMSBindingResources;

import com.sun.jbi.common.Util;

import com.sun.jbi.common.management.ComponentMessageHolder;
import com.sun.jbi.common.management.ManagementMessageBuilder;

import java.util.Collection;
import java.util.Iterator;

import java.util.logging.Logger;

import javax.jbi.component.ComponentContext;

import javax.jbi.management.DeploymentException;

/**
 * This class handles deployments to the JMS binding.
 *
 * @author Sun Microsystems Inc.
 */
public class JMSBindingSUManager
    implements javax.jbi.component.ServiceUnitManager, JMSBindingResources
{
    /**
     * Component context.
     */
    private ComponentContext mContext;
    /**
     * Deploy helper.
     */
    private DeployHelper mHelper;
    /**
     * Endpoint Manager.
     */
    private EndpointManager mManager;
    /**
     * Resolver object.
     */
    private JMSBindingResolver mResolver;
    /**
     * Logger object.
     */
    private Logger mLogger;
    /**
     * Deployer messages.
     */
    private ManagementMessageBuilder mBuildManagementMessage;
    /**
     * String translator.
     */
    private StringTranslator mStringTranslator;

    /**
     * Creates a new JMSBindingSUManager object.
     */
    public JMSBindingSUManager()
    {
        mLogger = JMSBindingContext.getInstance().getLogger();  
    }

    /**
     * Sets the translator.
     */
    private void setTranslator()
    {
        mStringTranslator =
                    JMSBindingContext.getInstance().getStringTranslator();     
    }
    /**
     * Returns a list of all Service units (SUs) currently
     * deployed in to the named component.  This method is a convenient
     * wrapper for the same operation in the DeployerMBean.
     *
     * @return array of SU UUID strings.
     */
    public String [] getDeployments()
    {
        EndpointRegistry dr = JMSBindingContext.getInstance().getRegistry();

        return dr.getAllDeployments();
    }

    /**
     * Sets values for JMSBindingSUManager.
     *
     * @param ctx component context.
     * @param manager endpoint manager.
     * @param reslv resolver.
     */
    public void setValues(
        ComponentContext ctx,
        EndpointManager manager,
        JMSBindingResolver reslv)
    {
        mContext = ctx;
        mManager = manager;
        mResolver = reslv;
        mBuildManagementMessage = Util.createManagementMessageBuilder();
    }

    /**
     * This method is called by the framework when a deployment request comes
     * for the file binding.
     *
     * @param suId service unit ID.
     * @param suPath path to the su.
     *
     * @return deploy result.
     *
     * @throws DeploymentException deployment exception.
     */
    public String deploy(
        String suId,
        String suPath)
        throws javax.jbi.management.DeploymentException
    {
        setTranslator();

        EndpointRegistry dr = JMSBindingContext.getInstance().getRegistry();
        String retMsg = null;
        if ((dr.getEndpoints(suId) != null)
                && (dr.getEndpoints(suId).size() != 0))
        {
            mLogger.severe(mStringTranslator.getString(
                    JMS_DUPLICATE_DEPLOYMENT, suId));
            throw new DeploymentException(createExceptionMessage(
                    mContext.getComponentName(), "deploy", "FAILED",
                    "JMS_30001", suId,
                    mStringTranslator.getString(JMS_DUPLICATE_DEPLOYMENT, suId),
                    new DeploymentException(mStringTranslator.getString(
                    JMS_DUPLICATE_DEPLOYMENT, suId))));
        }

        /**
         * You have to call initialize deployments here because, thats where
         * the schema check and data check are made, you want to fail the
         * deployment if the artifact fails validation. The same stuff is done
         * in init also during component starts , but if you dont do it here u
         * cannot fail deployments because of invalid data.
         */
        String warning = null;
        try
        {
            warning = initializeDeployment(suId, suPath);

            /*
             * Do not start deployments here.
             * The Deployment service will call init and start separately
             * after finidhing the deploy method.
             */
        }
        catch (DeploymentException de)
        {
            throw de;
        }
        catch (Exception e)
        {
            throw new DeploymentException(createExceptionMessage(
                    mContext.getComponentName(), "deploy", "FAILED",
                    "JMS_30009", suId,
                    mStringTranslator.getString(
                        JMS_XML_STRING_CREATION_FAILED, suId), e));
        }
        
        try
        {
            ComponentMessageHolder compMsgHolder =
                new ComponentMessageHolder("STATUS_MSG");
            compMsgHolder.setComponentName(mContext.getComponentName());
            compMsgHolder.setTaskName("deploy");
            compMsgHolder.setTaskResult("SUCCESS");
            if (warning != null)                
            {
                if (!warning.trim().equals(""))
                {  
                    String [] locParams = new String[1];
                    locParams[0] = suId;
                    compMsgHolder.setStatusMessageType("WARNING");
                    compMsgHolder.setLocToken(1, "JMS_30010");
                    compMsgHolder.setLocParam(1, locParams);
                    compMsgHolder.setLocMessage(1, warning);
                }
            }
            retMsg =
                mBuildManagementMessage.buildComponentMessage(compMsgHolder);
        }
        catch (Exception e)
        {
            mLogger.severe(mStringTranslator.getString(
                    JMS_XML_STRING_CREATION_FAILED));
            mLogger.severe(e.getMessage());
        }
        updateState(suId, EndpointStatus.DEPLOYED);
        return retMsg;
    }

    /**
     * This method is called by the framework when the deployment has to be \
     * initialised , just before starting it.
     *
     * @param suId service unit ID.
     * @param suPath service unit path.
     * @throws DeploymentException deployment exception.
     */
    public void init(
        String suId,
        String suPath)
        throws javax.jbi.management.DeploymentException
    {
        mLogger.info("**INITIALISING DEPLOYMENT** " + suId);

        /*
         * This may be called as soon as the deployment has hapened
         * or when the component starts
         * When the component starts up, we have to redeploy this SU
         * internally. This is how simple bindings and engines work. In case
         * of grown up components they might have their own ways of tracking
         * SU deployments and may not have to initialize deployments
         * everytime the component starts.
         */
        EndpointRegistry dr = JMSBindingContext.getInstance().getRegistry();

        if ((dr.getEndpoints(suId) != null)
                && (dr.getEndpoints(suId).size() == 0))
        {
            String warning =  null;
            try
            {
                warning = initializeDeployment(suId, suPath);
            }
            catch (DeploymentException de)
            {
                de.printStackTrace();
                throw de;
            }
            catch (Exception e)
            {
                e.printStackTrace();
                throw new DeploymentException(createExceptionMessage(
                        mContext.getComponentName(), "deploy", "FAILED",
                        "JMS_30009", suId,
                        mStringTranslator.getString(
                            JMS_XML_STRING_CREATION_FAILED, suId), e));
            }
            if (warning != null)
            {
                if (!warning.trim().equals(""))
                {
                    mLogger.warning(warning);
                }
            }
        }
        updateState(suId, EndpointStatus.INIT);
        mLogger.info("**INITIALISED DEPLOYMENT** " + suId);
    }

    /**
     * Shut down the service unit.
     *
     * @param suId service unit ID.
     *
     * @throws javax.jbi.management.DeploymentException deployment exception.
     */
    public void shutDown(String suId)
        throws javax.jbi.management.DeploymentException
    {        
         updateState(suId, EndpointStatus.SHUTDOWN);
    }

    /**
     * Starts the SU.
     *
     * @param suId Service unit ID.
     *
     * @throws javax.jbi.management.DeploymentException deployment exception.
     */
    public void start(String suId)
        throws javax.jbi.management.DeploymentException
    {
        setTranslator();
        mLogger.fine("**STARTING DEPLOYMENT** " + suId);

        try
        {
            mManager.startDeployment(suId);
        }
        catch (Exception e)
        {
            throw new DeploymentException(createExceptionMessage(
                    mContext.getComponentName(), "deploy", "FAILED",
                    "JMS_30009", suId,
                    mStringTranslator.getString(
                        JMS_XML_STRING_CREATION_FAILED, suId), e));
        }

        if (!mManager.isValid())
        {
            throw new DeploymentException(createExceptionMessage(
                    mContext.getComponentName(), "deploy", "FAILED",
                    "JMS_30009", suId, mManager.getError(), null));
        }
        updateState(suId, EndpointStatus.STARTED);
        mLogger.info("**STARTED DEPLOYMENT** " + suId);
    }

    /**
     * Stops the SU.
     *
     * @param suId Service unit.
     *
     * @throws javax.jbi.management.DeploymentException deployment exception.
     */
    public void stop(String suId)
        throws javax.jbi.management.DeploymentException
    {
        mManager.stopDeployment(suId);

        if (!mManager.isValid())
        {
            throw new DeploymentException(createExceptionMessage(
                    mContext.getComponentName(), "deploy", "FAILED",
                    "JMS_30009", suId, mManager.getError(), null));
        }
        updateState(suId, EndpointStatus.STOPPED);
    }

    /**
     * Undeploys the Service unit.
     *
     * @param suId service unit ID.
     * @param suPath service unit path.
     *
     * @return deploy result.
     *
     * @throws javax.jbi.management.DeploymentException deployment exception.
     */
    public String undeploy(
        String suId,
        String suPath)
        throws javax.jbi.management.DeploymentException
    {
        String retMsg = null;
        setTranslator();         
        EndpointRegistry dr = JMSBindingContext.getInstance().getRegistry(); 
        try
        {
            Collection l = dr.getEndpoints(suId);
            Iterator iter = l.iterator();

            while (iter.hasNext())
            {
                EndpointBean eb = (EndpointBean) iter.next();
                String epname = null;
                epname = eb.getUniqueName();
                dr.deregisterEndpoint(epname);
                dr.persist();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new DeploymentException(createExceptionMessage(
                    mContext.getComponentName(), "undeploy", "FAILED",
                    "JMS_30006", "", e.getMessage(), e));
        }
        try
        {
            ComponentMessageHolder compMsgHolder =
                new ComponentMessageHolder("STATUS_MSG");
            compMsgHolder.setComponentName(mContext.getComponentName());
            compMsgHolder.setTaskName("undeploy");
            compMsgHolder.setTaskResult("SUCCESS");

            retMsg =
                mBuildManagementMessage.buildComponentMessage(compMsgHolder);
        }
        catch (Exception e)
        {
            mLogger.severe(mStringTranslator.getString(
                    JMS_XML_STRING_CREATION_FAILED));
            mLogger.severe(e.getMessage());
        }
        return retMsg;
    }

    /**
     * helper method to create XML exception string.
     *
     * @param compid Component id.
     * @param oper operation like deploy or undeploy
     * @param status success r failure
     * @param loctoken some failure string token like SEQ_300001
     * @param locparam parameters for error message.
     * @param locmessage error message.
     * @param exObj stack trace of exception.
     *
     * @return XML string.
     */
    private String createExceptionMessage(
        String compid,
        String oper,
        String status,
        String loctoken,
        String locparam,
        String locmessage,
        Throwable exObj)
    {
        String [] locParams = new String[1];
        locParams[0] = locparam;
        setTranslator();
        ComponentMessageHolder msgMap =
            new ComponentMessageHolder("EXCEPTION_MSG");

        msgMap.setComponentName(compid);
        msgMap.setTaskName(oper);
        msgMap.setTaskResult(status);
        msgMap.setLocToken(1, loctoken);
        msgMap.setLocParam(1, locParams);
        msgMap.setLocMessage(1, locmessage);
        msgMap.setExceptionObject(exObj);

        String retMsg = null;

        try
        {
            retMsg = mBuildManagementMessage.buildComponentMessage(msgMap);
        }
        catch (Exception e)
        {
            mLogger.severe(mStringTranslator.getString(
                    JMS_XML_STRING_CREATION_FAILED));
            mLogger.severe(e.getMessage());
        }

        return retMsg;
    }

    /**
     * Initializes the deployments.
     *
     * @param suId Service unit id.
     * @param suPath Service unit path.
     *
     * @return string warning if any.
     * @throws javax.jbi.management.DeploymentException deployment exception.
     */
    private String initializeDeployment(
        String suId,
        String suPath)
        throws javax.jbi.management.DeploymentException
    {
        mHelper = new DeployHelper(suId, suPath, mContext, mResolver);
        mHelper.doDeploy(true);

        if (!mHelper.isValid())
        {
            mLogger.severe(mHelper.getError());

            Exception e = mHelper.getException();

            if (e != null)
            {
                throw new DeploymentException(createExceptionMessage(
                        mContext.getComponentName(), "deploy", "FAILED",
                        "JMS_30005", "", mHelper.getError(), e));
            }
            else
            {
                throw new DeploymentException(createExceptionMessage(
                        mContext.getComponentName(), "deploy", "FAILED",
                        "JMS_30005", "", mHelper.getError(), null));
            }
        }
        return mHelper.getWarning();
    }
    
    /**
     * Updates the endpoint states for all endpoints in the deployment.
     *
     * @param suId Service unit ID.
     * @param state endpoint state.
     */
    
    private void updateState(String suId, EndpointStatus state)
    {        
        EndpointRegistry dr = JMSBindingContext.getInstance().getRegistry();
        try
        { 
            Collection l = dr.getEndpoints(suId);
            Iterator iter = l.iterator();
            while (iter.hasNext())
            {
                EndpointBean eb = (EndpointBean) iter.next();
                if (eb != null)
                {
                    eb.setStatus(state);
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        
    }
}
