/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)XMLReader.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.jms.config;

import com.sun.jbi.StringTranslator;

import com.sun.jbi.binding.jms.JMSBindingContext;

import com.sun.org.apache.xpath.internal.XPathAPI;

import java.io.InputStream;

import java.util.ArrayList;
import java.util.Collection;

import java.util.logging.Logger;

import javax.jbi.JBIException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

/**
 * This configuration reader implementation maps a physical XML file
 * representation to a ConfigReader's tree representation. Users instantiate
 * an instance of XMLReader to read a XML configuration file and use the
 * ConfigElement and ConfigReader's interfaces to read the configuration data.
 * For e.g: To read the loglevel from XML file containing
 * <pre> <code>
 *       &lt;jmsbinding&gt;
 *          &lt;loglevel&gt;INFO&lt;/loglevel&gt;
 *       &lt;/jmsbinding&gt;
 * </code> </pre>
 * the user would have to write the following code
 * <pre> <code>
 *   String logLevel = xmlReader.getConfigurationValue("/jmsbinding/loglevel");
 * </code> </pre>
 *
 * @author Sun Microsystems Inc.
 */
public class XMLReader
{
    /**
     * Representation of the configuration file as a DOM object.
     */
    private Element mRootElement;

    /**
     * Handle to the logger instance.
     */
    private Logger mLogger;

    /**
     * Internal handle to the string translator.
     */
    private StringTranslator mStringTranslator;

    /**
     * Creates a new instance of XMLReader.
     */
    public XMLReader()
    {
        mLogger = JMSBindingContext.getInstance().getLogger();
        mStringTranslator =
            JMSBindingContext.getInstance().getStringTranslator();
    }

    /**
     * This returns the configuration element value as a config element.
     *
     * @param elementName - Configuration element name.
     *
     * @return the configuration element value as a config element.
     */
    public ConfigElement getConfigurationElement(String elementName)
    {
        ConfigElement result = null;

        if (mRootElement != null)
        {
            try
            {
                Element configElement =
                    (Element) XPathAPI.selectSingleNode(mRootElement,
                        elementName);

                if (configElement != null)
                {
                    result = new XMLConfigElement(configElement);
                }
            }
            catch (Exception exp)
            {
                exp.printStackTrace();
                result = null;
            }
        }

        return result;
    }

    /**
     * This returns the configuration element value as a config element array.
     *
     * @param elementName - Configuration element name.
     *
     * @return the configuration element values as a config element array.
     */
    public Collection getConfigurationElementList(String elementName)
    {
        Collection result = new ArrayList();

        if (mRootElement != null)
        {
            try
            {
                NodeList configElementList =
                    XPathAPI.selectNodeList(mRootElement, elementName);

                for (int i = 0; i < configElementList.getLength(); i++)
                {
                    result.add(new XMLConfigElement(
                            (Element) configElementList.item(i)));
                }
            }
            catch (Exception exp)
            {
                ;
            }
        }

        return result;
    }

    /**
     * This returns the configuration element value as a config element array.
     *
     * @param base is the base element from where the search is to begin.
     * @param elementName - Configuration element name.
     *
     * @return the configuration element values as a config element array.
     */
    public Collection getConfigurationElementList(
        Element base,
        String elementName)
    {
        Collection result = new ArrayList();

        if (mRootElement != null)
        {
            try
            {
                NodeList configElementList =
                    XPathAPI.selectNodeList(base, elementName);

                for (int i = 0; i < configElementList.getLength(); i++)
                {
                    result.add(new XMLConfigElement(
                            (Element) configElementList.item(i)));
                }
            }
            catch (Exception exp)
            {
                ;
            }
        }

        return result;
    }

    /**
     * This returns the configuration element value as a string. It returns the
     * first node value if there are more than one.
     *
     * @param elementName - Configuration element name.
     *
     * @return the configuration element value as a string.
     */
    public String getConfigurationValue(String elementName)
    {
        String result = null;

        if (mRootElement != null)
        {
            try
            {
                Element configElement =
                    (Element) XPathAPI.selectSingleNode(mRootElement,
                        elementName);

                if (configElement != null)
                {
                    Text textNode = (Text) configElement.getFirstChild();
                    result = textNode.getData();
                }
            }
            catch (Exception exp)
            {
                ;
            }
        }

        return result;
    }

    /**
     * This returns a configuration element values as a string array.
     *
     * @param elementName - Configuration element name.
     *
     * @return the configuration element values as a string array.
     */
    public Collection getConfigurationValueList(String elementName)
    {
        Collection result = new ArrayList();

        if (mRootElement != null)
        {
            try
            {
                NodeList configElementList =
                    XPathAPI.selectNodeList(mRootElement, elementName);

                for (int i = 0; i < configElementList.getLength(); i++)
                {
                    Element textElement = (Element) configElementList.item(i);
                    Text textNode = (Text) textElement.getFirstChild();
                    result.add(textNode.getData());
                }
            }
            catch (Exception exp)
            {
                result = null;
            }
        }

        return result;
    }

    /**
     * Reads the configuration file and stores the content as a DOM object.
     *
     * @param doc - configuration file name.
     *
     * @throws JBIException - if the reader cannot be initialized.
     */
    void init(Document doc)
        throws JBIException
    {
        try
        {
            mRootElement = doc.getDocumentElement();
        }
        catch (Exception exception)
        {
            exception.printStackTrace();

            JBIException jbiException =
                new JBIException(exception.getMessage());
            jbiException.initCause(exception);
            throw jbiException;
        }
    }

    /**
     * Reads the input stream and stores the content as a DOM object.
     *
     * @param fileStream - input file stream.
     *
     * @throws JBIException jbi exception.
     */
    void init(InputStream fileStream)
        throws JBIException
    {
        try
        {
            DocumentBuilderFactory builderFactory =
                DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder =
                builderFactory.newDocumentBuilder();
            Document configDocument = documentBuilder.parse(fileStream);
            mRootElement = configDocument.getDocumentElement();
        }
        catch (Exception exception)
        {
            JBIException jbiException =
                new JBIException(exception.getMessage());
            jbiException.initCause(exception);
            throw jbiException;
        }
    }
}
