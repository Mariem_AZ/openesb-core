/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Config.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.jms.config;

import com.sun.jbi.StringTranslator;

import com.sun.jbi.binding.jms.JMSBindingContext;
import com.sun.jbi.binding.jms.JMSBindingResources;
import com.sun.jbi.binding.jms.JMSConstants;

import com.sun.jbi.binding.jms.util.UtilBase;

import com.sun.jbi.common.ConfigFileValidator;

import java.io.File;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jbi.JBIException;

import org.w3c.dom.Document;

/**
 * This object defines the JMS binding's runtime configuration information. The
 * runtime is setup when the component is installed and is physically stored
 * in ${COMPONENT_INSTALL_ROOT)/jms_config.xml file.  This object uses the
 * XMLReader implementation to read configuration data and initialize itself.
 * The object is created when the binding component is initialized. If changes
 * are manually made to ${COMPONENT_INSTALL_ROOT)/jms_config.xml file,  the
 * component needs to be shutdown and restarted for the changes to take
 * affect.
 *
 * @author Sun Microsystems Inc.
 */
public class Config
    extends UtilBase
    implements JMSBindingResources
{
    /**
     * Defines the association between the level string names and
     * java.util.logging.Levels. The currently supported levels are
     * 
     * <UL>
     * <li>
     * SEVERE
     * </li>
     * <li>
     * WARNING
     * </li>
     * <li>
     * INFO
     * </li>
     * <li>
     * CONFIG
     * </li>
     * <li>
     * FINE
     * </li>
     * <li>
     * FINER
     * </li>
     * <li>
     * FINEST
     * </li>
     * <li>
     * OFF
     * </li>
     * <li>
     * ALL
     * </li>
     * </ul>.
     */
    private static Map sLevelCache;

    /**
     * Defines the association between the implementation names and
     * corresponding implementation. The currently supported implementations
     * are
     * 
     * <UL>
     * <li>
     * SimpleImplementation
     * </li>
     * </ul>.
     */
    private static Map sDeploymentImplCache;

    /**
     * Validator for the XML file.
     */
    private ConfigFileValidator mValidator;

    /**
     * Contains the application log level.
     */
    private Level mLogLevel;

    /**
     * The Logger.
     */
    private Logger mLogger;

    /**
     * Contains the properties configured for the deployment persistance
     * mechanism.
     */
    private Properties mDeploymentProperties;

    /**
     * Contains the configuration file name. This file name should not be
     * relative and should contain the absolute path.
     */
    private String mConfigFileName;

    /**
     * Contains the Deployment persistance implmentation class name.
     */
    private String mDeploymentImplClass;

    /**
     * Lookup value for initial context.
     */
    private String mInitialContextFactory;

    /**
     * Provider URL.
     */
    private String mProviderUrl;

    /**
     * Password for the MQ connection.
     */
    private String mReceiverConnectionPassword;

    /**
     * MQ connection user id.
     */
    private String mReceiverConnectionUser;
  
    /**
     * Desination name.
     */
    private String mReceiverDestination;

    /**
     *  Receiver factory.
     */
    private String mReceiverFactory;

    /**
     * Password.
     */
    private String mSecurityCredentials;

    /**
     * Security level.
     */
    private String mSecurityLevel;

    /**
     * User name.
     */
    private String mSecurityPrincipal;

    /**
     * Local handle to StringTranslator implementation.
     */
    private StringTranslator mStringTranslator;

    /**
     * Maximum number of threads.
     */
    private int mMaxThreads;

    /**
     * Minimum number of threads.
     */
    private int mMinThreads;

    /**
     * Stop timeout value.
     */
    private long mStopTimeout;

    {
        sLevelCache = new HashMap();
        sLevelCache.put("SEVERE", Level.SEVERE);
        sLevelCache.put("WARNING", Level.WARNING);
        sLevelCache.put("INFO", Level.INFO);
        sLevelCache.put("CONFIG", Level.CONFIG);
        sLevelCache.put("FINE", Level.FINE);
        sLevelCache.put("FINER", Level.FINER);
        sLevelCache.put("FINEST", Level.FINEST);
        sLevelCache.put("OFF", Level.OFF);
        sLevelCache.put("ALL", Level.ALL);
        sDeploymentImplCache = new HashMap();
        sDeploymentImplCache.put("SimpleImplementation",
            "com.sun.jbi.binding.jms.deploy.RegistryCacheImpl");
        sDeploymentImplCache.put("DEFAULT",
            "com.sun.jbi.binding.jms.deploy.RegistryCacheImpl");
    }

    /**
     * Creates a new instance of Config.
     *
     * @param configFileName configuration file name. This file name should not
     *        be relative and should contain the absolute path.
     */
    public Config(String configFileName)
    {
        mConfigFileName = configFileName;
        mDeploymentProperties = new Properties();
        mStringTranslator =
            JMSBindingContext.getInstance().getStringTranslator();
        mLogger = JMSBindingContext.getInstance().getLogger();

        String schema =
            JMSBindingContext.getInstance().getContext().getInstallRoot()
            + ConfigConstants.JMS_CONFIG_FILE_SCHEMA;
        mValidator = new ConfigFileValidator(schema, mConfigFileName);
    }

    /**
     * Gets the configured deployment implementation class name. This value is
     * extracted from the element
     * <pre> <code> 
     *              &lt;jmsbc:jms-binding&gt;
     *                  &lt;deployment&gt;
     *                      &lt;impl&gt;
     *                      &lt;/impl&gt;
     *                  &lt;/deployment&gt;
     *              &lt;/jmsbc:jms-binding&gt;
     *           </code> </pre>
     * present in the configuration file.  Supported runtime deployment
     * implementations
     * 
     * <UL>
     * <li>
     * SimpleImplementation
     * </li>
     * </ul>.
     * 
     *
     * @return class name.
     */
    public String getDeploymentImplClass()
    {
        return mDeploymentImplClass;
    }

    /**
     * Gets the properties configured for the deployment implementation. The
     * values are extracted from the elements
     * <pre> <code> 
     *              &lt;jmsbc:jms-binding&gt;
     *                  &lt;deployment&gt;
     *                      &lt;impl&gt;
     *                          &lt;param&gt;
     *                              &lt;paramname/&gt;
     *                              &lt;paramvalue/&gt;
     *                          &lt;/param&gt;*
     *                      &lt;/impl&gt;
     *                  &lt;/deployment&gt;
     *              &lt;/jmsbc:jms-binding&gt;
     *           </code> </pre>.
     *
     * @return configured properties.
     */
    public Properties getDeploymentProperties()
    {
        return mDeploymentProperties;
    }

    /**
     * Get the lookup for initial context factory.
     *
     * @return the String.
     */
    public String getInitialContextFactory()
    {
        return mInitialContextFactory;
    }

    /**
     * Gets the configured application log level. This value is extracted from
     * the element
     * <pre> <code>
     *              &lt;jmsbc:jms-binding&gt;
     *                  &lt;loglevel/&gt;
     *              &lt;/jmsbc:jms-binding&gt;
     *        </code> </pre>
     * present in  the configuration file.  The supported loglevel values are
     * 
     * <UL>
     * <li>
     * SEVERE
     * </li>
     * <li>
     * WARNING
     * </li>
     * <li>
     * INFO
     * </li>
     * <li>
     * CONFIG
     * </li>
     * <li>
     * FINE
     * </li>
     * <li>
     * FINER
     * </li>
     * <li>
     * FINEST
     * </li>
     * <li>
     * OFF
     * </li>
     * <li>
     * ALL
     * </li>
     * </ul>.
     * 
     *
     * @return log level.
     */
    public Level getLogLevel()
    {
        return mLogLevel;
    }

    /**
     * Gets the configured maximum thread count. This value is extracted from
     * the element
     * <pre> <code> 
     *              &lt;jmsbc:jms-binding&gt;
     *                  &lt;threadpool&gt;
     *                      &lt;maxthreads&gt;
     *                      &lt;/maxthreads&gt;
     *                  &lt;/threadpool&gt;
     *              &lt;/jmsbc:jms-binding&gt;
     *           </code> </pre>
     * present in the configuration file.
     *
     * @return maximum thread count.
     */
    public int getMaxThreadCount()
    {
        return mMaxThreads;
    }

    /**
     * Gets the configured minimum thread count. This value is extracted from
     * the element
     * <pre>  <code>
     *              &lt;jmsbc:jms-binding&gt;
     *                  &lt;threadpool&gt;
     *                      &lt;minthreads&gt;
     *                      &lt;/minthreads&gt;
     *                  &lt;/threadpool&gt;
     *              &lt;/jmsbc:jms-binding&gt;
     *           </code> </pre>
     * present in the configuration file.
     *
     * @return minimum thread count.
     */
    public int getMinThreadCount()
    {
        return mMinThreads;
    }

    /**
     * Gets the provider url.
     *
     * @return the provider url.
     */
    public String getProviderUrl()
    {
        return mProviderUrl;
    }
    
    /**
     * Returns the receiver connection password.
     *
     * @return receiver connection password.
     */
    public String getReceiverConnectionPassword()
    {
        return mReceiverConnectionPassword;
    }

    /**
     * Returns the receiver connection user.
     *
     * @return receiver connection user name.
     */
    public String getReceiverConnectionUser()
    {
        return mReceiverConnectionUser;
    }

    /**
     * Returns the receiver destination.
     *
     * @return receiver destination.
     */
    public String getReceiverDestination()
    {
        return mReceiverDestination;
    }

    /**
     * Returns the receiver factory.
     *
     * @return receiver factory.
     */
    public String getReceiverFactory()
    {
        return mReceiverFactory;
    }

    /**
     * Get the SecurityConfiguration.
     *
     * @return the String.
     */
    public String getSecurityCredentials()
    {
        return mSecurityCredentials;
    }

    /**
     * Get the Security level.
     *
     * @return the String.
     */
    public String getSecurityLevel()
    {
        return mSecurityLevel;
    }

    /**
     * Get the SecurityConfiguration.
     *
     * @return the SecurityConfig instance.
     */
    public String getSecurityPrincipal()
    {
        return mSecurityPrincipal;
    }

    /**
     * Reads data from the configuration object and initializes itself.
     *
     * @param componentId is the ComponentId obtained from the JBI Context.
     *
     * @throws JBIException - when the object cannot be initialized.
     */
    public void loadConfiguration(String componentId)
        throws JBIException
    {
        clear();

        mValidator.setValidating();
        mValidator.validate();

        Document d = mValidator.getDocument();

        if ((!mValidator.isValid()) || (d == null))
        {
            mLogger.severe(mStringTranslator.getString(
                    JMS_INVALID_INSTALL_CONFIG_FILE, mConfigFileName));
            setError(mStringTranslator.getString(JMS_INVALID_CONFIG_FILE,
                    mConfigFileName) + " " + mValidator.getError());

            return;
        }

        XMLReader reader = new XMLReader();
        reader.init(d);
        mLogLevel =
            getLevel(reader.getConfigurationValue(
                    "/jmsbc:jms-binding/log-level"));
       
        ConfigElement configElement =
            reader.getConfigurationElement("/jmsbc:jms-binding/thread-pool");

        mMinThreads =
            Integer.parseInt(configElement.getProperty("min-threads"));
        mMaxThreads =
            Integer.parseInt(configElement.getProperty("max-threads"));

        String deploymentImpl =
            reader.getConfigurationValue(
                "/jmsbc:jms-binding/deployment/impl-class");
        mDeploymentImplClass =
            (String) sDeploymentImplCache.get(deploymentImpl);

        if (mDeploymentImplClass == null)
        {
            // using default implementation
            mDeploymentImplClass = (String) sDeploymentImplCache.get("DEFAULT");
        }

        Collection deploymentPropertyList =
            reader.getConfigurationElementList(
                "/jmsbc:jms-binding/deployment/param");

        for (Iterator iter = deploymentPropertyList.iterator(); iter.hasNext();)
        {
            ConfigElement propertyElement = (ConfigElement) iter.next();
            mDeploymentProperties.setProperty(propertyElement.getProperty(
                    "param-name"), propertyElement.getProperty("param-value"));
        }

        validateProperties();

        if (!isValid())
        {
            setError(mStringTranslator.getString(
                    JMS_INVALID_DEPLOYMENT_PROPERTIES));

            return;
        }

        mDeploymentImplClass = (String) sDeploymentImplCache.get("DEFAULT");
        mInitialContextFactory =
            reader.getConfigurationValue(
                "/jmsbc:jms-binding/initial-context-factory");
        mProviderUrl =
            reader.getConfigurationValue("/jmsbc:jms-binding/provider-url");

        mSecurityPrincipal =
            reader.getConfigurationValue(
                "/jmsbc:jms-binding/security-principal");
        mSecurityCredentials =
            reader.getConfigurationValue(
                "/jmsbc:jms-binding/security-credentials");
        mSecurityLevel =
            reader.getConfigurationValue("/jmsbc:jms-binding/security-level");

        try
        {
            mStopTimeout =
                Long.parseLong(reader.getConfigurationValue(
                        "/jmsbc:jms-binding/stop-timeout"));
        }
        catch (NumberFormatException ne)
        {
            mStopTimeout = JMSConstants.DEFAULT_STOP_TIMEOUT;
        }

        mReceiverDestination =
            reader.getConfigurationValue(
                "/jmsbc:jms-binding/temporary-receiver/destination-name");
        mReceiverFactory =
            reader.getConfigurationValue(
                "/jmsbc:jms-binding/temporary-receiver/connection-factory");

        mReceiverConnectionUser =
            reader.getConfigurationValue(
                "/jmsbc:jms-binding/temporary-receiver/connection-user-name");

        mReceiverConnectionPassword =
            reader.getConfigurationValue(
                "/jmsbc:jms-binding/temporary-receiver/connection-password");

        printDetails();
    }

    /**
     * Prints the configuration details.
     */
    public void printDetails()
    {
        mLogger.fine("Log level " + mLogLevel.getName());
        mLogger.fine("Min threads " + new Integer(mMinThreads));
        mLogger.fine("Max threads " + new Integer(mMaxThreads));
        mLogger.fine("Deployment Class " + mDeploymentImplClass);
        mLogger.fine("Deploy props " + mDeploymentProperties.toString());
        mLogger.fine("Initial context factory " + mInitialContextFactory);
        mLogger.fine("Provider URL " + mProviderUrl);
        mLogger.fine("Receiver destination " + mReceiverDestination);
        mLogger.fine("Receiver destination Factory " + mReceiverFactory);
        mLogger.fine("Receiver connection user " + mReceiverConnectionUser);
        mLogger.fine("Receiver connection password"
            + mReceiverConnectionPassword);
    }

    /**
     * Clears the configuration object details.
     */
    protected void clearAll()
    {
        mLogLevel = JMSConstants.DEFAULT_LOG_LEVEL;
        mDeploymentProperties.clear();
        mDeploymentImplClass = null;
        mMinThreads = JMSConstants.DEFAULT_MIN_THREADS;
        mMaxThreads = JMSConstants.DEFAULT_MAX_THREADS;
    }

    /**
     * Gets the java.util.logging.Level associated with the configured string
     * name. If no level exists for the provided level name, it returns the
     * default level.
     *
     * @param levelName - string identifying the level.
     *
     * @return associated level;  default level if the levelName is incorrect.
     */
    private Level getLevel(String levelName)
    {
        Level logLevel;

        if (levelName == null)
        {
            mLogger.warning(mStringTranslator.getString(JMS_INVALID_LOG_LEVEL,
                    mLogLevel));
            logLevel = JMSConstants.DEFAULT_LOG_LEVEL;
        }
        else
        {
            logLevel = (Level) sLevelCache.get(levelName);

            if (logLevel == null)
            {
                mLogger.warning(mStringTranslator.getString(JMS_INVALID_LOG_LEVEL,
                    mLogLevel));
                logLevel = JMSConstants.DEFAULT_LOG_LEVEL;
            }
        }

        return logLevel;
    }

    /**
     * Validates all the properties.
     */
    private void validateProperties()
    {
        String filename =
            mDeploymentProperties.getProperty(JMSConstants.REGISTRY_FILE_NAME);
        String filelocation =
            mDeploymentProperties.getProperty(JMSConstants.REGISTRY_FILE_LOCATION);

        if ((filename == null) || (filename.trim().equals("")))
        {         
            setError(mStringTranslator.getString(JMS_REG_FILE_NOT_FOUND,
                    filename));

            return;   
        }
        
        
        if ((filelocation == null) || (filelocation.trim().equals("")))
        {         
            setError(mStringTranslator.getString(JMS_REG_FILE_NOT_FOUND,
                    filelocation));

            return;   
        }
        
        String installroot =
            JMSBindingContext.getInstance().getContext().getInstallRoot();

        String filepath =
            installroot + File.separator + filelocation + File.separator
            + filename;

        File f = new File(filepath);

        if (f == null)
        {
            setError(mStringTranslator.getString(JMS_REG_FILE_NOT_FOUND,
                    filepath));

            return;
        }

        if (!f.exists())
        {
            setError(mStringTranslator.getString(JMS_REG_FILE_NOT_FOUND,
                    filepath));

            return;
        }

        if (!f.canWrite())
        {
            setError(mStringTranslator.getString(JMS_REG_FILE_NOT_WRITABLE,
                    filepath));
        }

        if (!f.canRead())
        {
            setError(mStringTranslator.getString(JMS_REG_FILE_NOT_READABLE,
                    filepath));
        }
    }
}
