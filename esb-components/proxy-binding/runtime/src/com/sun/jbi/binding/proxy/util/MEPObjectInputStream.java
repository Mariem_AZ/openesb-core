/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MEPObjectInputStream.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.proxy.util;

import java.io.InputStream;
import java.io.IOException;
import java.io.ObjectStreamClass;


/**
 *
 * @author Sun Microsystems, Inc
 */
public class MEPObjectInputStream extends
    java.io.ObjectInputStream
{
    
    ClassLoader         cL;
    
    /** Creates a new instance of MEPObjectInputStream */
    public MEPObjectInputStream(InputStream iS) 
        throws java.io.IOException
    {
        super(iS);
    }
    
    public void setClassLoader(ClassLoader classLoader)
    {
        cL = classLoader;
    }
    
    protected Class resolveClass(ObjectStreamClass desc)
    throws IOException, ClassNotFoundException
    {
	String name = desc.getName();
	try 
        {
	    return Class.forName(name, false, cL);
	} 
        catch (ClassNotFoundException ex) 
        {
	    return (super.resolveClass(desc));
	}
     }
    
}
