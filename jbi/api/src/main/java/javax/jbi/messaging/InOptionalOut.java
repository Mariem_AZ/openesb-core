/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)InOptionalOut.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package javax.jbi.messaging;

/** Supports operations used to process an In Optional Out MEP to completion.
 *
 * @author JSR208 Expert Group
 */
public interface InOptionalOut extends MessageExchange
{
    /** Retrieves the "in" message reference from this exchange.
     *  @return in message
     */
    NormalizedMessage getInMessage();
    
    /** Retrieves the "out" message reference from this exchange.
     *  @return out message
     */
    NormalizedMessage getOutMessage();
    
    /** Specifies the "in" message reference for this exchange.
     *  @param msg in message
     *  @throws MessagingException unable to set in message
     */
    void setInMessage(NormalizedMessage msg)
        throws MessagingException;
    
    /** Specifies the "out" message reference for this exchange.
     *  @param msg out message
     *  @throws MessagingException unable to set out message
     */
    void setOutMessage(NormalizedMessage msg)
        throws MessagingException;
}
