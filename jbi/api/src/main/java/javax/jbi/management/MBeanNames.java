/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MBeanNames.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package javax.jbi.management;

import javax.management.ObjectName;

/**
 * This interface provides methods to create JMX object names for component-
 * supplied MBeans. This ensures that component-supplied MBeans follow the
 * JBI implementation-determined naming convention.
 * <p>
 * Components obtain instances of this name creator using {@link
 * javax.jbi.component.ComponentContext#getMBeanNames()}.
 *
 * @author JSR208 Expert Group
 */
public interface MBeanNames
{
    /**
     * Retrieve the default JMX Domain Name for MBeans registered in
     * this instance of the JBI implementation.
     *
     * @return the JMX domain name for this instance of the JBI implemention;
     *         must be non-null and non-empty
     */
    String getJmxDomainName();

    /**
     * Formulate and return an MBean ObjectName for a custom control
     * of this name creator's JBI component.
     * <p>
     * This is used by components to create JMX names for their own JMX
     * controls, allowing the JBI implementation to prefix the created name
     * to fit within the implementation's own naming scheme.
     * <p>
     * Standard extensions must use the following custom name constants:
     * <ul>
     *   <li>Bootstrap (installer) extension: {@link #BOOTSTRAP_EXTENSION}.</li>
     *   <li>Component life cycle extension: 
     *       {@link #COMPONENT_LIFE_CYCLE_EXTENSION}.
     *   </li>
     * </ul>
     * All other custom component MBeans must use custom names that do not
     * collide with the standard extension names.
     *
     * @param customName the name of the custom control; must be non-null and
     *        non-empty; must be legal for use in a JMX object name
     * @return the JMX ObjectName of the MBean, or <code>null</code> if 
     *         the <code>customName</code> is invalid
     */
    ObjectName createCustomComponentMBeanName(String customName);

    /** The custom name that must be used for bootstrap extensions */
    static final String BOOTSTRAP_EXTENSION = "BootstrapExtension";

    /** The custom name that must be used for component life cycle extensions */
    static final String COMPONENT_LIFE_CYCLE_EXTENSION = "LifeCycleExtension";
}
