#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)parse_ddiff_logs.pl
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

#set ENV Variables
$SRCROOT = $ENV{SRCROOT}; 
$EXTERNAL_CVSROOT = $ENV{EXTERNAL_CVSROOT};

# Open the list created by ddiff
open(FH, "<$SRCROOT/cvs_repos_logs/ddiff.log") || die "Can not open: $?";
# Open the log of new directories
open(NEWDIRS, ">>$SRCROOT/cvs_repos_logs/cvs_dir_add.list") || die "Can not open: $?";
# Open the log of new files
open(NEWFILES, ">>$SRCROOT/cvs_repos_logs/cvs_file_add.list") || die "Can not open: $?";
# Open the log of modified files
open(MODFILES, ">>$SRCROOT/cvs_repos_logs/cvs_file_modified.list") || die "Can not open: $?";
# Open the log of file to remove
open(DELFILES, ">>$SRCROOT/cvs_repos_logs/cvs_file_remove.list") || die "Can not open: $?";

while (defined ($line = <FH> )) {

    #remove whitespace from the line
    chomp $line;

    #split up the fields into an array
    @fields = split(/\s+/, $line);

    # If it is on the left, and a directory then add it to the list of directories to add 
    if ( ($fields[0] eq "LEFT") && ($fields[1] eq "D") && !($fields[3] eq "0") ) {
      print NEWDIRS "cvs -z6 -d $EXTERNAL_CVSROOT add $fields[2] \n";
    }

    # If it is on the left, and a file then add it to the list of files to add
    if ( ($fields[0] eq "LEFT") && ($fields[1] eq "F") && !($fields[3] eq "0") ) {
       open(F, "<$SRCROOT/cvs_repos_internal/${fields[2]}") || die "Can not open: $?";
       if ( -B F )
       {
         print NEWFILES "cvs -z6 -d $EXTERNAL_CVSROOT add -kb '$fields[2]'\n"
       } else {
         print NEWFILES "cvs -z6 -d $EXTERNAL_CVSROOT add '$fields[2]'\n"
       }
       close(F);
    }

    # If it is on both, and a file then add it to the list of files to modify
    if ( ($fields[0] eq "BOTH") && ($fields[1] eq "F") && !($fields[3] eq "0") ) {
      print MODFILES "cvs -d $EXTERNAL_CVSROOT ci $fields[2] \n";
    }

    # If it is on the right, and a file then add it to the list of files to remove
    if ( ($fields[0] eq "RIGHT") && ($fields[1] eq "F") && !($fields[3] eq "0") ) {
      print DELFILES "cvs -d $EXTERNAL_CVSROOT remove $fields[2] \n";
    }


#
#    for ($i = 0; $i < @fields; $i++) {
#        print "$i : $fields[$i]\n";
#    }
#

}
