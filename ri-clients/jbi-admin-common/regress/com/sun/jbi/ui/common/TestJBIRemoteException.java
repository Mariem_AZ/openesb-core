/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestJBIRemoteException.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.common;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import junit.framework.TestCase;

/**
 * test class
 * @author Sun Microsystems, Inc.
 */
public class TestJBIRemoteException extends TestCase
{
    
    /**
     * Creates a new instance of TestMgmtMessage
     * @param aTestName name
     */
    public TestJBIRemoteException(String aTestName)
    {
        super(aTestName);
    }
    
    private String getSrcRootDir()
    {
        String srcroot = System.getProperty("junit.srcroot");
        return srcroot;
    }
        
    /**
     * reads the file to a string
     * @throws Exception if an unexpected error occurs
     */    
    protected String readFromResource(String resource) throws Exception {
        StringWriter strWriter = new StringWriter();
        BufferedWriter writer = new BufferedWriter(strWriter);
        BufferedReader reader = new BufferedReader(new InputStreamReader(this.getClass().getResourceAsStream(resource)));
        for (String line; (line = reader.readLine()) != null ; ) {
            writer.write(line);
            writer.newLine();
        }
        reader.close();
        writer.close();
        strWriter.close();
        return strWriter.getBuffer().toString();
    }
    
    /**
     * reads the file to a string
     * @throws Exception if an unexpected error occurs
     */    
    protected String readFile(String filePath) throws Exception {
        StringWriter strWriter = new StringWriter();
        BufferedWriter writer = new BufferedWriter(strWriter);
        BufferedReader reader = new BufferedReader(new FileReader(filePath));
        for (String line; (line = reader.readLine()) != null ; ) {
            writer.write(line);
            writer.newLine();
        }
        reader.close();
        writer.close();
        strWriter.close();
        return strWriter.getBuffer().toString();
    }
    
    /**
     * test sucess msg
     * @throws Exception on error
     */
    public void testMessageFormatter() throws Exception
    {
        Exception ex1, ex2, ex3, ex4, ex5;
        
        ex1 = new JBIRemoteException("This is the Main Message");
        System.out.println("Main Message Only [\n"+ex1.getMessage()+"\n]\n");
        
        ex2 = new JBIRemoteException("Secondary Cause");
        ex1 = new JBIRemoteException("Main Message", ex2);
        
        System.out.println("Main Message with Cause [\n"+ex1.getMessage()+"\n]\n");
        
        ex2 = new JBIRemoteException("Secondary Cause");
        ex1 = new JBIRemoteException(ex2);
        
        System.out.println("Only Cause [\n"+ex1.getMessage()+"\n]\n");
        
        
        ex5 = new JBIRemoteException("Fifth Cause");
        ex4 = new JBIRemoteException("Fourth Cause", ex5);
        ex3 = new JBIRemoteException("Third Cause has multiple lines. \nSecond line for third cause", ex4);
        ex2 = new JBIRemoteException("Secondary Cause", ex3);
        ex1 = new JBIRemoteException("Main Message", ex2);
        
        System.out.println("Main Message with Multiple Cause [\n"+ex1.getMessage()+"\n]\n");
        
        ex5 = new JBIRemoteException("Fifth Cause");
        ex4 = new JBIRemoteException("Fourth Cause", ex5);
        ex3 = new JBIRemoteException(ex4);
        ex2 = new JBIRemoteException("Secondary Cause", ex3);
        ex1 = new JBIRemoteException("Main Message", ex2);
        
        System.out.println("Main Message with Multiple Cause and 3rd cause with no message [\n"+ex1.getMessage()+"\n]\n");

        ex4 = new JBIRemoteException("Fourth Cause");
        ex3 = new JBIRemoteException(ex4);
        ex2 = new JBIRemoteException(ex3);
        ex1 = new JBIRemoteException(ex2);
        
        System.out.println("Last Cause Message [\n"+ex1.getMessage()+"\n]\n");
        
        ex4 = new Exception("Some Exception");
        ex3 = new JBIRemoteException(ex4);
        ex2 = new JBIRemoteException(ex3);
        ex1 = new JBIRemoteException(ex2);
        
        System.out.println("Last 3rd party Cause Message [\n"+ex1.getMessage()+"\n]\n");
        
        ex4 = new JBIRemoteException((String)null);
        ex3 = new Exception("Some Exception", ex4);
        ex2 = new JBIRemoteException(ex3);
        ex1 = new JBIRemoteException(ex2);
        
        System.out.println("Middle 3rd party Cause Message [\n"+ex1.getMessage()+"\n]\n");
        
        ex4 = new JBIRemoteException((String)null);
        ex3 = new JBIRemoteException(null, ex4);
        ex2 = new JBIRemoteException(null,ex3);
        ex1 = new JBIRemoteException("Main Message", ex2);
        
        System.out.println("All Null messages [\n"+ex1.getMessage()+"\n]\n");
        
        ex3 = new JBIRemoteException(new NullPointerException());
        ex2 = new JBIRemoteException(null,ex3);
        ex1 = new JBIRemoteException(ex2);
        
        System.out.println("3rd party with no message [\n"+ex1.getMessage()+"\n]\n");
        
        ex3 = new JBIRemoteException(new NullPointerException());
        ex2 = new Exception(ex3);
        ex1 = new JBIRemoteException(ex2);
        
        System.out.println("3rd party with no message in the middle [\n"+ex1.getMessage()+"\n]\n");
        
        
//        // Caused by:
//        ex5 = new Exception("Fifth Cause");
//        ex4 = new Exception("Fourth Cause", ex5);
//        ex3 = new Exception("Third Cause", ex4);
//        ex2 = new Exception("Secondary Cause", ex3);
//        ex1 = new Exception("Main Message", ex2);
//        
//        // System.out.println("Normal Exception Message : \n" + ex1.getMessage());
//        System.out.println("Normal Exception Stacktrace : \n");
//        ex1.printStackTrace();
        
    }
    
    /**
     * test sucess msg
     * @throws Exception on error
     */
    public void testNonJbiResultXmlInRemoteException() throws Exception
    {
        String nonJbiResultXml = this.readFromResource("test_jbi.xml");
        Exception ex = new Exception(nonJbiResultXml);
        JBIRemoteException rEx = new JBIRemoteException(ex);
        
        JBIManagementMessage mgmtMsg = rEx.extractJBIManagementMessage();
        
        this.assertNull("Expecting a NULL Mmgmt object from non jbi mgmt xml", mgmtMsg );
    }    
    
    /**
     * main
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        // TODO code application logic here
        try
        {
            new TestJBIRemoteException("test").testMessageFormatter();
        } catch ( Exception ex )
        {
            ex.printStackTrace();
        }
    }
}
