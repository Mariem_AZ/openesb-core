/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestRuntimeLoggerMethods.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package java4ant;

import java.util.Map;
import java.util.logging.Level;

import javax.management.remote.*;
import javax.management.*;

/**
 */
public class TestRuntimeLoggerMethods
{
     
    private MBeanServerConnection mbns;
    private static final String RESULT_PREFIX = "##### Result of ";
    
    private static final String USER     = "jmx.user";
    private static final String PASSWORD = "jmx.password";
    private static final String PROVIDER = "jmx.provider";
    private static final String TARGET   = "target";

    private String target;
    
    public void initMBeanServerConnection()
        throws Exception
    {
        java.util.Map<String, String[]> env = new java.util.HashMap();
        String user = System.getProperty(USER);
        String pass = System.getProperty(PASSWORD);
        String[] credentials = new String[] { user, pass};
        env.put("jmx.remote.credentials", credentials);   
        
        String jmxProvider = System.getProperty(PROVIDER);
        
        JMXConnector connector = JMXConnectorFactory.connect(new JMXServiceURL(jmxProvider), env);

        mbns = connector.getMBeanServerConnection();
    }

    public ObjectName getJbiAdminCommandsUI()
        throws Exception
    {
        String admin = "com.sun.jbi:" +
            "ServiceName=JbiReferenceAdminUiService,ComponentType=System";
        return new ObjectName(admin);
    }

    public void getRuntimeLoggerLevels()
        throws Exception
    {
        target = System.getProperty(TARGET, "server");
    
        Object[] params = new Object[2];
        params[0] = target;
        params[1] = target;
    
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
    
        Map<String, Level> loggers = (Map<String, Level>)
            mbns.invoke(getJbiAdminCommandsUI(), "getRuntimeLoggerLevels", params, signature);
        
        System.out.println(RESULT_PREFIX + " getRuntimeLoggerLevels =" + loggers);
    }

    public void getRuntimeLoggerLevel(String logger, boolean expectingException)
    {
        target = System.getProperty(TARGET, "server");
    
        Object[] params = new Object[3];
        params[0] = logger;
        params[1] = target;
        params[2] = target;
    
        String[] signature = new String[3];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        signature[2] = "java.lang.String";
    
        try
        {
            Level level = (Level) mbns.invoke(getJbiAdminCommandsUI(),
                "getRuntimeLoggerLevel", params, signature);
        
            System.out.println(RESULT_PREFIX + " getRuntimeLoggerLevel =" + level);
            if (expectingException)
            {
                System.out.println("*** Was expecting exception, but didn't get one! ***");
            }
        }
        catch (Exception e)
        {
            if (expectingException)
            {
                System.out.println(RESULT_PREFIX + " getRuntimeLoggerLevel: " +
                                   "caught expected exception.");
            }
            else
            {
                System.out.println(RESULT_PREFIX + " getRuntimeLoggerLevel: Exception: "
                                    + e.getMessage());
            }
        }
    }

    public void setRuntimeLoggerLevel(String logger, Level level, boolean expectingException)
    {
        target = System.getProperty(TARGET, "server");
    
        Object[] params = new Object[3];
        params[0] = logger;
        params[1] = level;
        params[2] = target;
    
        String[] signature = new String[3];
        signature[0] = "java.lang.String";
        signature[1] = "java.util.logging.Level";
        signature[2] = "java.lang.String";
    
        try
        {
            mbns.invoke(getJbiAdminCommandsUI(), "setRuntimeLoggerLevel",
                        params, signature);
        
            System.out.println(RESULT_PREFIX + " setRuntimeLoggerLevel() called.");
            if (expectingException)
            {
                System.out.println("*** Was expecting exception, but didn't get one! ***");
            }
        }
        catch (Exception e)
        {
            if (expectingException)
            {
                System.out.println(RESULT_PREFIX + " setRuntimeLoggerLevel: " +
                                   "caught expected exception.");
            }
            else
            {
                System.out.println(RESULT_PREFIX + " setRuntimeLoggerLevel: Exception: "
                                    + e.getMessage());
                e.printStackTrace();
            }
        }
    }

    public void getRuntimeLoggerNames()
        throws Exception
    {
        target = System.getProperty(TARGET, "server");
    
        Object[] params = new Object[2];
        params[0] = target;
        params[1] = target;
    
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
    
        Map<String, String> loggers = (Map<String, String>)
            mbns.invoke(getJbiAdminCommandsUI(), "getRuntimeLoggerNames", params, signature);
        
        System.out.println(RESULT_PREFIX + " getRuntimeLoggerNames =" + loggers);
    }

    public String getRuntimeLoggerDisplayName(String logger, boolean expectingException)
    {
        target = System.getProperty(TARGET, "server");
    
        Object[] params = new Object[3];
        params[0] = logger;
        params[1] = target;
        params[2] = target;
    
        String[] signature = new String[3];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        signature[2] = "java.lang.String";
    
        String name = null;
        try
        {
            name = (String) mbns.invoke(getJbiAdminCommandsUI(),
                "getRuntimeLoggerDisplayName", params, signature);
        
            System.out.println(RESULT_PREFIX + " getRuntimeLoggerDisplayName =" + name);
            if (expectingException)
            {
                System.out.println("*** Was expecting exception, but didn't get one! ***");
            }

        }
        catch (Exception e)
        {
            if (expectingException)
            {
                System.out.println(RESULT_PREFIX + " getRuntimeLoggerDisplayName: " +
                                   "caught expected exception.");
            }
            else
            {
                System.out.println(RESULT_PREFIX + " getRuntimeLoggerDisplayName: Exception: "
                                    + e.getMessage());
            }
        }
        return name;
    }

    public static void main (String[] params)
        throws Exception 
    {
        TestRuntimeLoggerMethods test = new TestRuntimeLoggerMethods();
        String loggerName = "com.sun.jbi.framework";
        
        test.initMBeanServerConnection();
        test.getRuntimeLoggerLevels();
        test.getRuntimeLoggerLevel(loggerName, false);
        test.setRuntimeLoggerLevel(loggerName, Level.FINER, false);
        test.getRuntimeLoggerLevel(loggerName, false);
        test.setRuntimeLoggerLevel(loggerName, Level.INFO, false);
        test.getRuntimeLoggerLevel(loggerName, false);
        test.getRuntimeLoggerLevel("bogus logger", true);
        test.setRuntimeLoggerLevel("bogus logger", Level.ALL, true);
        test.getRuntimeLoggerNames();
        test.getRuntimeLoggerDisplayName(loggerName, false);
        test.getRuntimeLoggerDisplayName("bogus logger", true);
    }
    
}
