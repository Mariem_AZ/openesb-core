/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SunASEEJMXConnectionFactory.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.client.sunasee;

import com.sun.jbi.ui.client.JMXConnection;

import com.sun.jbi.ui.client.JMXConnectionFactory;
import com.sun.jbi.ui.client.JMXConnection;
import com.sun.jbi.ui.common.JMXConnectionException;
import java.util.Properties;


/**
 * DOCUMENT ME!
 *
 * @author Sun Microsystems, Inc.
 */
public class SunASEEJMXConnectionFactory extends JMXConnectionFactory
{
    /**
     * Creates a new instance of RemoteJMXConnectionFactory
     */
    public SunASEEJMXConnectionFactory()
    {
    }

    /**
     *
     *
     * @param conprops  NOT YET DOCUMENTED
     *
     * @return  NOT YET DOCUMENTED
     */
    public JMXConnection getConnection(Properties conprops)
    throws JMXConnectionException
    {
        return new SunASEEJMXConnectionImpl(conprops);
    }
}
