/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ComponentStatisticsDataWriter.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.management.common.data.helper;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import com.sun.esb.management.common.data.ComponentStatisticsData;

/**
 * @author graj
 *
 */
public class ComponentStatisticsDataWriter implements
        ComponentStatisticsDataXMLConstants, Serializable {
    
    
    static final long   serialVersionUID = -1L;
    
    static final String FILE_NAME_KEY    = "ComponentStatisticsData.xml";
    
    /** Constructor - Creates an ComponentStatisticsDataWriter */
    public ComponentStatisticsDataWriter() {
    }
    
    /**
     * 
     * @param document
     * @param directoryPath
     * @throws TransformerConfigurationException
     * @throws TransformerException
     * @throws Exception
     */
    public static void writeToFile(Document document, String directoryPath)
            throws TransformerConfigurationException, TransformerException,
            Exception {
        File file = new File(directoryPath);
        if ((file.isDirectory() == false) || (file.exists() == false)) {
            throw new Exception("Directory Path: " + directoryPath
                    + " is invalid.");
        }
        String fileLocation = file.getAbsolutePath() + File.separator
                + FILE_NAME_KEY;
        System.out.println("Writing out to file: " + fileLocation);
        File outputFile = new File(fileLocation);
        // Use a Transformer for aspectOutput
        TransformerFactory tFactory = TransformerFactory.newInstance();
        Transformer transformer = tFactory.newTransformer();
        DOMSource source = new DOMSource(document);
        StreamResult result = new StreamResult(outputFile);
        
        // indent the Output to make it more legible...
        transformer.setOutputProperty(OutputKeys.METHOD, "xml");
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        transformer.setOutputProperty(OutputKeys.MEDIA_TYPE, "text/xml");
        transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");
        transformer.setOutputProperty(
                "{http://xml.apache.org/xslt}indent-amount", "4");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        
        transformer.transform(source, result);
    }
    
    /**
     * Change the contents of text file in its entirety, overwriting any
     * existing text. This style of implementation throws all exceptions to the
     * caller.
     * 
     * @param aFile
     *            is an existing file which can be written to.
     * @throws IllegalArgumentException
     *             if param does not comply.
     * @throws FileNotFoundException
     *             if the file does not exist.
     * @throws IOException
     *             if problem encountered during write.
     */
    public static void setContents(File aFile, String aContents)
            throws FileNotFoundException, IOException {
        if (aFile == null) {
            throw new IllegalArgumentException("File should not be null.");
        }
        if (!aFile.exists()) {
            aFile.createNewFile();
        }
        if (!aFile.isFile()) {
            throw new IllegalArgumentException("Should not be a directory: "
                    + aFile);
        }
        if (!aFile.canWrite()) {
            throw new IllegalArgumentException("File cannot be written: "
                    + aFile);
        }
        
        // declared here only to make visible to finally clause; generic
        // reference
        Writer output = null;
        try {
            // use buffering
            // FileWriter always assumes default encoding is OK!
            output = new BufferedWriter(new FileWriter(aFile));
            output.write(aContents);
        } finally {
            // flush and close both "aspectOutput" and its underlying FileWriter
            if (output != null) {
                output.close();
            }
        }
    }
    
    /**
     * 
     * @param NMRStatisticsData
     *            data
     * @return XML string
     * @throws ParserConfigurationException
     * @throws TransformerException
     */
    public static String serialize(Map<String /* instanceName */, ComponentStatisticsData> dataMap)
            throws ParserConfigurationException, TransformerException {
        Document document = null;
        ComponentStatisticsDataWriter writer = new ComponentStatisticsDataWriter();
        if (dataMap != null) {
            DocumentBuilderFactory factory = DocumentBuilderFactory
                    .newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            document = builder.newDocument(); // Create from whole cloth
            
            // ////////////////////////////////
            // <ComponentStatisticsDataList>
            Element root = (Element) document
                    .createElement(COMPONENT_STATISTICS_DATA_LIST_KEY);
            // xmlns="http://java.sun.com/xml/ns/esb/management/ComponentStatisticsData"
            root.setAttribute(NAMESPACE_KEY, NAMESPACE_VALUE);
            // version = "1.0"
            root.setAttribute(VERSION_KEY, VERSION_VALUE);
            
            for (String instanceName : dataMap.keySet()) {
                ComponentStatisticsData data = dataMap.get(instanceName);
                // ////////////////////////////////
                // <ComponentStatisticsData>
                Element componentStatisticsDataElementChild = writer
                        .createComponentStatisticsDataElement(document, data);
                // </ComponentStatisticsData>
                root.appendChild(componentStatisticsDataElementChild);
                // ////////////////////////////////
            }
            
            // ////////////////////////////////
            // </ComponentStatisticsDataList>
            document.appendChild(root);
            // ////////////////////////////////
            
        }
        return writer.writeToString(document);
    }
    
    /**
     * 
     * @param document
     * @param data
     * @return
     */
    protected Element createComponentStatisticsDataElement(Document document,
            ComponentStatisticsData data) {
        Element componentStatisticsDataElement = null;
        if ((document != null) && (data != null)) {
            
            // <ComponentStatisticsData>
            componentStatisticsDataElement = document
                    .createElement(COMPONENT_STATISTICS_DATA_KEY);
            
            // <InstanceName>
            Element instanceNameElementChild = document
                    .createElement(INSTANCE_NAME_KEY);
            if (instanceNameElementChild != null) {
                instanceNameElementChild.setTextContent(data.getInstanceName());
            }
            // </InstanceName>
            componentStatisticsDataElement
                    .appendChild(instanceNameElementChild);
            
            // <ComponentUpTime>
            Element upTimeElementChild = document.createElement(COMPONENT_UPTIME_KEY);
            if (upTimeElementChild != null) {
                upTimeElementChild.setTextContent(data.getComponentUpTime() + "");
            }
            // </ComponentUpTime>
            componentStatisticsDataElement.appendChild(upTimeElementChild);
            
            // <NumberOfActivatedEndpoints>
            Element activatedEndpointsElementChild = document.createElement(NUMBER_OF_ACTIVATED_ENDPOINTS_KEY);
            if (activatedEndpointsElementChild != null) {
                activatedEndpointsElementChild.setTextContent(data.getNumberOfActivatedEndpoints() + "");
            }
            // </NumberOfActivatedEndpoints>
            componentStatisticsDataElement.appendChild(activatedEndpointsElementChild);

            // <NumberOfReceivedRequests>
            Element receivedRequestsElementChild = document.createElement(NUMBER_OF_RECEIVED_REQUESTS_KEY);
            if (receivedRequestsElementChild != null) {
                receivedRequestsElementChild.setTextContent(data.getNumberOfReceivedRequests() + "");
            }
            // </NumberOfReceivedRequests>
            componentStatisticsDataElement.appendChild(receivedRequestsElementChild);

            // <NumberOfSentRequests>
            Element sentRequestsElementChild = document.createElement(NUMBER_OF_SENT_REQUESTS_KEY);
            if (sentRequestsElementChild != null) {
                sentRequestsElementChild.setTextContent(data.getNumberOfSentRequests() + "");
            }
            // </NumberOfSentRequests>
            componentStatisticsDataElement.appendChild(sentRequestsElementChild);

            // <NumberOfReceivedReplies>
            Element receivedRepliesElementChild = document.createElement(NUMBER_OF_RECEIVED_REPLIES_KEY);
            if (receivedRepliesElementChild != null) {
                receivedRepliesElementChild.setTextContent(data.getNumberOfReceivedReplies() + "");
            }
            // </NumberOfReceivedReplies>
            componentStatisticsDataElement.appendChild(receivedRepliesElementChild);

            // <NumberOfSentReplies>
            Element sentRepliesElementChild = document.createElement(NUMBER_OF_SENT_REPLIES_KEY);
            if (sentRepliesElementChild != null) {
                sentRepliesElementChild.setTextContent(data.getNumberOfSentReplies() + "");
            }
            // </NumberOfSentReplies>
            componentStatisticsDataElement.appendChild(sentRepliesElementChild);

            // <NumberOfReceivedDones>
            Element receivedDonesElementChild = document.createElement(NUMBER_OF_RECEIVED_DONES_KEY);
            if (receivedDonesElementChild != null) {
                receivedDonesElementChild.setTextContent(data.getNumberOfReceivedDones() + "");
            }
            // </NumberOfReceivedDones>
            componentStatisticsDataElement.appendChild(receivedDonesElementChild);

            // <NumberOfSentDones>
            Element sentDonesElementChild = document.createElement(NUMBER_OF_SENT_DONES_KEY);
            if (sentDonesElementChild != null) {
                sentDonesElementChild.setTextContent(data.getNumberOfSentDones() + "");
            }
            // </NumberOfSentDones>
            componentStatisticsDataElement.appendChild(sentDonesElementChild);

            // <NumberOfReceivedFaults>
            Element receivedFaultsElementChild = document.createElement(NUMBER_OF_RECEIVED_FAULTS_KEY);
            if (receivedFaultsElementChild != null) {
                receivedFaultsElementChild.setTextContent(data.getNumberOfReceivedFaults() + "");
            }
            // </NumberOfReceivedFaults>
            componentStatisticsDataElement.appendChild(receivedFaultsElementChild);

            // <NumberOfSentFaults>
            Element sentFaultsElementChild = document.createElement(NUMBER_OF_SENT_FAULTS_KEY);
            if (sentFaultsElementChild != null) {
                sentFaultsElementChild.setTextContent(data.getNumberOfSentFaults() + "");
            }
            // </NumberOfSentFaults>
            componentStatisticsDataElement.appendChild(sentFaultsElementChild);

            // <NumberOfReceivedErrors>
            Element receivedErrorsElementChild = document.createElement(NUMBER_OF_RECEIVED_ERRORS_KEY);
            if (receivedErrorsElementChild != null) {
                receivedErrorsElementChild.setTextContent(data.getNumberOfReceivedErrors() + "");
            }
            // </NumberOfReceivedErrors>
            componentStatisticsDataElement.appendChild(receivedErrorsElementChild);
            
            // <NumberOfSentErrors>
            Element sentErrorsElementChild = document.createElement(NUMBER_OF_SENT_ERRORS_KEY);
            if (sentErrorsElementChild != null) {
                sentErrorsElementChild.setTextContent(data.getNumberOfSentErrors() + "");
            }
            // </NumberOfSentErrors>
            componentStatisticsDataElement.appendChild(sentErrorsElementChild);            

            // <NumberOfCompletedExchanges>
            Element completedExchangesElementChild = document.createElement(NUMBER_OF_COMPLETED_EXCHANGES_KEY);
            if (completedExchangesElementChild != null) {
                completedExchangesElementChild.setTextContent(data.getNumberOfCompletedExchanges() + "");
            }
            // </NumberOfCompletedExchanges>
            componentStatisticsDataElement.appendChild(completedExchangesElementChild);            
            
            // <NumberOfActiveExchanges>
            Element completedActiveElementChild = document.createElement(NUMBER_OF_ACTIVE_EXCHANGES_KEY);
            if (completedActiveElementChild != null) {
                completedActiveElementChild.setTextContent(data.getNumberOfActiveExchanges() + "");
            }
            // </NumberOfActiveExchanges>
            componentStatisticsDataElement.appendChild(completedActiveElementChild);            
            
            // <NumberOfErrorExchanges>
            Element completedErrorElementChild = document.createElement(NUMBER_OF_ERROR_EXCHANGES_KEY);
            if (completedErrorElementChild != null) {
                completedErrorElementChild.setTextContent(data.getNumberOfErrorExchanges() + "");
            }
            // </NumberOfErrorExchanges>
            componentStatisticsDataElement.appendChild(completedErrorElementChild);            
            
            // <ExtendedTimingStatisticsFlagEnabled>
            Element extendedTimingStatisticsFlagEnabledElementChild = document.createElement(EXTENDED_TIMING_STATISTICS_FLAG_ENABLED_KEY);
            if (extendedTimingStatisticsFlagEnabledElementChild != null) {
                extendedTimingStatisticsFlagEnabledElementChild.setTextContent(data.isExtendedTimingStatisticsFlagEnabled() + "");
            }
            // </ExtendedTimingStatisticsFlagEnabled>
            componentStatisticsDataElement.appendChild(extendedTimingStatisticsFlagEnabledElementChild);            

            // <MessageExchangeResponseTimeAverage>
            Element responseTimeAverageElementChild = document.createElement(MESSAGE_EXCHANGE_RESPONSE_TIME_AVERAGE_KEY);
            if (responseTimeAverageElementChild != null) {
                responseTimeAverageElementChild.setTextContent(data.getMessageExchangeResponseTimeAverage() + "");
            }
            // </MessageExchangeResponseTimeAverage>
            componentStatisticsDataElement.appendChild(responseTimeAverageElementChild);            

            // <MessageExchangeComponentTimeAverage>
            Element componentTimeAverageElementChild = document.createElement(MESSAGE_EXCHANGE_COMPONENT_TIME_AVERAGE_KEY);
            if (componentTimeAverageElementChild != null) {
                componentTimeAverageElementChild.setTextContent(data.getMessageExchangeComponentTimeAverage() + "");
            }
            // </MessageExchangeComponentTimeAverage>
            componentStatisticsDataElement.appendChild(componentTimeAverageElementChild);            

            // <MessageExchangeDeliveryChannelTimeAverage>
            Element deliveryChannelTimeAverageElementChild = document.createElement(MESSAGE_EXCHANGE_DELIVERY_CHANNEL_TIME_AVERAGE_KEY);
            if (deliveryChannelTimeAverageElementChild != null) {
                deliveryChannelTimeAverageElementChild.setTextContent(data.getMessageExchangeDeliveryChannelTimeAverage() + "");
            }
            // </MessageExchangeDeliveryChannelTimeAverage>
            componentStatisticsDataElement.appendChild(deliveryChannelTimeAverageElementChild);            

            // <MessageExchangeMessageServiceTimeAverage>
            Element messageServiceTimeAverageElementChild = document.createElement(MESSAGE_EXCHANGE_MESSAGE_SERVICE_TIME_AVERAGE_KEY);
            if (messageServiceTimeAverageElementChild != null) {
                messageServiceTimeAverageElementChild.setTextContent(data.getMessageExchangeMessageServiceTimeAverage() + "");
            }
            // </MessageExchangeMessageServiceTimeAverage>
            componentStatisticsDataElement.appendChild(messageServiceTimeAverageElementChild);
            
            // <ComponentExtensionStatus>
            Element componentExtensionStatusElementChild = document.createElement(COMPONENT_EXTENSION_STATUS_KEY);
            if (componentExtensionStatusElementChild != null) {
                componentExtensionStatusElementChild.setTextContent(data.getComponentExtensionStatusAsString());
            }
            // </ComponentExtensionStatus>
            componentStatisticsDataElement.appendChild(componentExtensionStatusElementChild);

        }
        
        return componentStatisticsDataElement;
    }
    
    /**
     * @param document
     * @return
     * @throws TransformerException
     */
    protected String writeToString(Document document)
            throws TransformerException {
        // Use a Transformer for aspectOutput
        TransformerFactory tFactory = TransformerFactory.newInstance();
        Transformer transformer = tFactory.newTransformer();
        DOMSource source = new DOMSource(document);
        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        
        transformer.setOutputProperty(OutputKeys.METHOD, "xml");
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        transformer.setOutputProperty(OutputKeys.MEDIA_TYPE, "text/xml");
        transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");
        
        // indent the aspectOutput to make it more legible...
        transformer.setOutputProperty(
                "{http://xml.apache.org/xslt}indent-amount", "4");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.transform(source, result);
        
        return result.getWriter().toString();
    }
    
    /**
     * @param args
     */
    public static void main(String[] args) {
        String uri = "C:/test/schema/componentstatistics/ComponentStatisticsData.xml";
        try {
            Map<String /* instanceName */, ComponentStatisticsData> map = null;
            map = ComponentStatisticsDataReader.parseFromFile(uri);
            for (String instanceName : map.keySet()) {
                System.out.println(map.get(instanceName).getDisplayString());
            }
            
            String content = ComponentStatisticsDataWriter.serialize(map);
            System.out.println(content);
            ComponentStatisticsDataWriter.setContents(new File(uri), content);
            
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }
    
}
