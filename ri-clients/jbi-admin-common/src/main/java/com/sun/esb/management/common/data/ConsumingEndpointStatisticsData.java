/* 
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ConsumingEndpointStatisticsData.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */

package com.sun.esb.management.common.data;

import java.io.Serializable;

/**
 * Provides Consuming Endpoint Statistics
 * 
 * @author graj
 * 
 */
public class ConsumingEndpointStatisticsData extends EndpointStatisticsData
        implements IEndpointStatisticsData, Serializable {
    static final long          serialVersionUID         = -1L;
    
    public static final String NUM_SENT_REQUESTS_KEY    = "NumSentRequests";
    
    public static final String NUM_RECEIVED_REPLIES_KEY = "NumReceivedReplies";
    
    public static final String ME_STATUS_TIME_AVG_KEY   = "MessageExchangeStatusTime Avg (ns)";
    
    protected long             numberOfSentRequests;
    
    protected long             numberOfReceivedReplies;
    
    protected long           messageExchangeStatusTimeAverage;
    
    /** Constructor - creates a ConsumingEndpointStatisticsData object */
    public ConsumingEndpointStatisticsData() {
        this.setProvisioningEndpoint(false);
    }
    
    /**
     * Constructor - creates a ConsumingEndpointStatisticsData object
     * 
     * @param endpointData
     */
    public ConsumingEndpointStatisticsData(EndpointStatisticsData endpointData) {
        super(endpointData);
        this.setProvisioningEndpoint(false);
        ConsumingEndpointStatisticsData data = (ConsumingEndpointStatisticsData) endpointData;
        this.setMessageExchangeStatusTimeAverage(data
                .getMessageExchangeStatusTimeAverage());
        this.setNumberOfReceivedReplies(data.getNumberOfReceivedReplies());
        this.setNumberOfSentRequests(data.getNumberOfSentRequests());
    }
    
    /**
     * @return the numberOfSentRequests
     */
    public long getNumberOfSentRequests() {
        return this.numberOfSentRequests;
    }
    
    /**
     * @param numberOfSentRequests
     *            the numberOfSentRequests to set
     */
    public void setNumberOfSentRequests(long numberOfSentRequests) {
        this.numberOfSentRequests = numberOfSentRequests;
    }
    
    /**
     * @return the numberOfReceivedReplies
     */
    public long getNumberOfReceivedReplies() {
        return this.numberOfReceivedReplies;
    }
    
    /**
     * @param numberOfReceivedReplies
     *            the numberOfReceivedReplies to set
     */
    public void setNumberOfReceivedReplies(long numberOfReceivedReplies) {
        this.numberOfReceivedReplies = numberOfReceivedReplies;
    }
    
    /**
     * @return the messageExchangeStatusTimeAverage
     */
    public long getMessageExchangeStatusTimeAverage() {
        return this.messageExchangeStatusTimeAverage;
    }
    
    /**
     * @param messageExchangeStatusTimeAverage
     *            the messageExchangeStatusTimeAverage to set
     */
    public void setMessageExchangeStatusTimeAverage(
            long messageExchangeStatusTimeAverage) {
        this.messageExchangeStatusTimeAverage = messageExchangeStatusTimeAverage;
    }
    
    /**
     * Return a displayable string of values
     * 
     * @return
     */
    public String getDisplayString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("\n    Number Of Sent Requests" + "="
                + this.getNumberOfSentRequests());
        buffer.append("\n    Number Of Received Replies" + "="
                + this.getNumberOfReceivedReplies());
        buffer.append("\n    Message Exchange Status Time Average" + "="
                + this.getMessageExchangeStatusTimeAverage());
        buffer.append(super.getDisplayString());
        buffer.append("\n");
        return buffer.toString();
    }
    
    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        
    }
    
}
