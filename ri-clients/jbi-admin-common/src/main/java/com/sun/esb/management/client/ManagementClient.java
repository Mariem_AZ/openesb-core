/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ManagementClient.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.management.client;

import java.io.Serializable;

import javax.management.MBeanServerConnection;

import com.sun.esb.management.api.administration.AdministrationService;
import com.sun.esb.management.api.configuration.ConfigurationService;
import com.sun.esb.management.api.deployment.DeploymentService;
import com.sun.esb.management.api.installation.InstallationService;
import com.sun.esb.management.api.notification.NotificationService;
import com.sun.esb.management.api.performance.PerformanceMeasurementService;
import com.sun.esb.management.api.runtime.RuntimeManagementService;
import com.sun.esb.management.common.ImplementationMap;
import com.sun.esb.management.common.ManagementRemoteException;
import com.sun.esb.management.common.Service;
import com.sun.esb.management.impl.administration.AdministrationServiceImpl;
import com.sun.esb.management.impl.configuration.ConfigurationServiceImpl;
import com.sun.esb.management.impl.deployment.DeploymentServiceImpl;
import com.sun.esb.management.impl.installation.InstallationServiceImpl;
import com.sun.esb.management.impl.notification.NotificationServiceImpl;
import com.sun.esb.management.impl.performance.PerformanceMeasurementServiceImpl;
import com.sun.esb.management.impl.runtime.RuntimeManagementServiceImpl;
import com.sun.jbi.ui.common.I18NBundle;


/**
 * Retrieves the various services associated with JBI Management and Monitoring
 * administration commands.
 * 
 * @author graj
 */
public class ManagementClient implements Serializable {
    
    /** i18n */
    private static I18NBundle       I18NBUNDLE      = null;
    
    static final long               serialVersionUID = -1L;
    
    /**
     * gives the I18N bundle
     * 
     * @return I18NBundle object
     */
    protected static I18NBundle getI18NBundle() {
        // lazzy initialize the Client
        if (I18NBUNDLE == null) {
            I18NBUNDLE = new I18NBundle("com.sun.caps.management.client");
        }
        return I18NBUNDLE;
    }
    
    /** is this a local or remote connection */
    boolean                         isRemoteConnection;
    
    /** remote MBeanServer connection */
    transient MBeanServerConnection remoteConnection;
    
    /** Constructor */
    public ManagementClient() {
        this(null, false);
    }
    
    /**
     * Constructor
     * 
     * @param serverConnection
     */
    public ManagementClient(MBeanServerConnection serverConnection) {
        this(serverConnection, false);
    }
    
    /**
     * Constructor
     * 
     * @param serverConnection
     * @param isRemoteConnection
     */
    public ManagementClient(MBeanServerConnection serverConnection,
            boolean isRemoteConnection) {
        this.remoteConnection = serverConnection;
        this.isRemoteConnection = isRemoteConnection;
    }
    
    /**
     * Get the Administration Service
     * @return Administration Service object
     * @throws ManagementRemoteException
     */
    public AdministrationService getAdministrationService() throws ManagementRemoteException {
        return new AdministrationServiceImpl(this.remoteConnection, this.isRemoteConnection);
    }

    /**
     * Get the Configuration Service
     * @return Configuration Service object
     * @throws ManagementRemoteException
     */
    public ConfigurationService getConfigurationService() throws ManagementRemoteException {
        return new ConfigurationServiceImpl(this.remoteConnection, this.isRemoteConnection);
    }
    

    /**
     * Get the Deployment Service
     * @return Deployment Service object
     * @throws ManagementRemoteException
     */
    public DeploymentService getDeploymentService() throws ManagementRemoteException {
        return new DeploymentServiceImpl(this.remoteConnection, this.isRemoteConnection);
    }    

    /**
     * Get the Installation Service
     * @return Installation Service object
     * @throws ManagementRemoteException
     */
    public InstallationService getInstallationService() throws ManagementRemoteException {
        return new InstallationServiceImpl(this.remoteConnection, this.isRemoteConnection);
    }
    
    /**
     * Get the Performance Measurement Service
     * @return Performance Measurement Service object
     * @throws ManagementRemoteException
     */
    public PerformanceMeasurementService getPerformanceMeasurementService() throws ManagementRemoteException {
        return new PerformanceMeasurementServiceImpl(this.remoteConnection, this.isRemoteConnection);
    }    
    
    /**
     * Get the Runtime Management Service
     * @return Runtime Management Service object
     * @throws ManagementRemoteException
     */
    public RuntimeManagementService getRuntimeManagementService() throws ManagementRemoteException {
        return new RuntimeManagementServiceImpl(this.remoteConnection, this.isRemoteConnection);
    }    
    
    /**
     * Get the Notification Service
     * @return Notification Service object
     * @throws ManagementRemoteException
     */
    public NotificationService getNotificationService() throws ManagementRemoteException {
        return new NotificationServiceImpl(this.remoteConnection, this.isRemoteConnection);
    }    
    
    /**
     * is this a local or remote connection
     * 
     * @return true if remote, false if local
     */
    public boolean isRemoteConnection() {
        return this.isRemoteConnection;
    }    
    
    /**
     * Get the generic service
     *  
     * @param mapperClass
     * @return <CLASS_TYPE> instance or null if the Service could not be obtained
     */
    public <CLASS_TYPE> CLASS_TYPE getService(Class<?> mapperClass) {
        ImplementationMap map = mapperClass.getAnnotation(ImplementationMap.class);
        Class<?> implementationClass = map.implementationClass();
        CLASS_TYPE theObject = null;
        Service theService = null;
        
        try {
            theService = (Service)implementationClass.newInstance();
            theObject = theService.<CLASS_TYPE>getService(remoteConnection,isRemoteConnection);
        } catch (InstantiationException e) {
        } catch (IllegalAccessException e) {
        } catch(ManagementRemoteException e) {
        }
        return theObject;
    }
    
    /**
     * Get the client API version
     * @param mapperClass
     * @return the version of the Client API
     */
    public String getClientAPIVersion(Class mapperClass) {
        String version = "";
        ImplementationMap map = (ImplementationMap)mapperClass.getAnnotation(ImplementationMap.class);
        if ( map!=null ) {
            return map.clientAPIVersion();
        }
        return version;
    }
    
}
