/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestUtil.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.common;

import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.Class;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import junit.framework.TestCase;

/**
 * test class
 * @author Sun Microsystems, Inc.
 */
public class TestUtil extends TestCase
{
    
    private int intType;
    private float floatType;
    
    private Integer integerObjType;
    private Float floatObjType;
    
    /**
     * Creates a new instance of TestMgmtMessage
     * @param aTestName name
     */
    public TestUtil(String aTestName)
    {
        super(aTestName);
    }
    
    public void setIntType( int i)
    {
        this.intType = i;
    }
    
    public int getIntType()
    {
        return this.intType;
    }
    
    public void setFloatType( float f)
    {
        this.floatType = f;
    }
    
    public float getFloatType()
    {
        return this.floatType;
    }
    
    public void setIntegerObjType(Integer iObj)
    {
        this.integerObjType = iObj;
    }
    
    public Integer getIntegerObjType()
    {
        return this.integerObjType;
    }
    
    public void setFloatObjType( Float fObj)
    {
        this.floatObjType = fObj;
    }
    
    public Float getFloatObjType()
    {
        return this.floatObjType;
    }
    
    public static void assignValue(Object obj, String methodName, Class paramType, Object value)
    {
        try
        {
            Method method = obj.getClass().getMethod(methodName, new Class[] { paramType} );
            method.invoke(obj, new Object[] {value});
        }
        catch ( Exception ex )
        {
            ex.printStackTrace();
        }
    }
    
    /**
     * test sucess msg
     * @throws Exception on error
     */
    public void testNewInstance() throws Exception
    {
        Object testObj = this;
        
        Object valueObj = Util.newInstance("int", "200");
        assignValue(testObj, "setIntType", Integer.TYPE, valueObj);
        
        valueObj = Util.newInstance("java.lang.Integer", "300");
        assignValue(testObj, "setIntegerObjType", Integer.class, valueObj);
        
        valueObj = Util.newInstance("float", "2.20");
        assignValue(testObj, "setFloatType", Float.TYPE, valueObj);
        
        valueObj = Util.newInstance("java.lang.Float", "3.30");
        assignValue(testObj, "setFloatObjType", Float.class, valueObj);
        
        this.assertTrue(" int value Expected = 200", (200 == this.getIntType()) );
        
        this.assertTrue(" Integer value Expected = 300",
            (300 == this.getIntegerObjType()) );
        
        this.assertTrue(" float value Expected = 2.2",
            (((float)2.2) == this.getFloatType()) ) ;
        
        this.assertTrue(" Float Object value Expected = 3.3",
            ( ((float)3.3) == this.getFloatObjType() ) );
        
        Object fileObj = Util.newInstance("java.io.File", "temp");
        this.assertTrue("java.io.File type expected ", (fileObj instanceof File) );
        this.assertTrue("File Object Value expected is temp ",
            fileObj.toString().equals("temp"));
        
    }
    
    public void testForLocalHost() throws Exception
    {
        
            this.assertTrue(
                "expected localhost true for : host=empty ", 
                Util.isLocalHost(""));
            
            this.assertTrue(
                "expected localhost true for : host=null ", 
                Util.isLocalHost(null));
            
            
            this.assertTrue(
                "expected localhost true for : host=localhost " ,
                Util.isLocalHost("localhost"));
        
            this.assertTrue(
                "expected localhost true for : host=InetAddress.getByName(localhost).getHostName()",
                Util.isLocalHost(InetAddress.getByName("localhost").getHostName()) );
            
            this.assertTrue(
                "expected localhost true for : host=InetAddress.getByName(localhost).getCanonicalHostName()",
                Util.isLocalHost(InetAddress.getByName("localhost").getCanonicalHostName()) );
            
            
            this.assertTrue(
                "expected localhost true for : host=InetAddress.getByName(localhost).getHostAddress()",
                Util.isLocalHost(InetAddress.getByName("localhost").getHostAddress())  );

            this.assertTrue(
                "expected localhost true for : host=InetAddress.getLocalHost().getHostName()", 
                Util.isLocalHost(InetAddress.getLocalHost().getHostName()) );
            
            this.assertTrue(
                "expected localhost true for : host=InetAddress.getLocalHost().getCanonicalHostName()", 
                Util.isLocalHost(InetAddress.getLocalHost().getCanonicalHostName()) );
            

            this.assertTrue(
                "expected localhost true for : host=InetAddress.getLocalHost().getHostAddress()", 
                Util.isLocalHost(InetAddress.getLocalHost().getHostAddress()) );
            
        
    }
    
    /**
     * main
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        // TODO code application logic here
        try
        {
            new TestUtil("test").testNewInstance();
            new TestUtil("test").testForLocalHost();
        }
        catch ( Exception ex )
        {
            ex.printStackTrace();
        }
    }
}
