#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbi-admin-cli00001.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#



#----------------------------------------------------------------------------------
# Perform any necessary cleanup to restore the repository back to its initial state.
#----------------------------------------------------------------------------------
test_cleanup()
{
  cleanup server
}


#----------------------------------------------------------------------------------
# Main function called that will run the test
#----------------------------------------------------------------------------------
run_test()
{
  initilize_test 
 
  # Clean up the (Remove uninstall everything so this regression test will work
  # test_cleanup
   
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Install shared library cli_test_sns1"
  echo "-------------------------------------------------------------------"
  echo "install-jbi-shared-library cli-test-sns1.jar"
  $AS8BASE/bin/asadmin install-jbi-shared-library --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS $UI_REGRESS_DIST_DIR/cli-test-sns1.jar 
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Install service engine cli_test_engine4"
  echo "-------------------------------------------------------------------"
  echo "install-jbi-component cli-test-engine4.jar"
  $AS8BASE/bin/asadmin install-jbi-component      --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS $UI_REGRESS_DIST_DIR/cli-test-engine4.jar
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Start the component cli_test_engine4"
  echo "-------------------------------------------------------------------"
  echo "start-jbi-component cli_test_engine4"
  $AS8BASE/bin/asadmin start-jbi-component --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_engine4
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Shut Down the component cli_test_engine4"
  echo "-------------------------------------------------------------------"
  echo "shut-down-jbi-component cli_test_engine4"
  $AS8BASE/bin/asadmin shut-down-jbi-component --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_engine4
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Show the components"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-service-engine cli_test_engine4"
  $AS8BASE/bin/asadmin show-jbi-service-engine --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_engine4
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Upgrade component cli_test_engine4 with cli_test_engine4-1"
  echo "-------------------------------------------------------------------"
  echo "upgrade-jbi-component --upgradefile=ant-test-engine4-1.jar cli_test_engine4"
  $AS8BASE/bin/asadmin upgrade-jbi-component --upgradefile=$UI_REGRESS_DIST_DIR/cli-test-engine4-1.jar --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_engine4
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Show the components"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-service-engine cli_test_engine4"
  $AS8BASE/bin/asadmin show-jbi-service-engine --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_engine4
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Start the component cli_test_engine4"
  echo "-------------------------------------------------------------------"
  echo "start-jbi-component cli_test_engine4"
  $AS8BASE/bin/asadmin start-jbi-component --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_engine4
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Show the components"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-service-engine cli_test_engine4"
  $AS8BASE/bin/asadmin show-jbi-service-engine --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_engine4
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Stop the component cli_test_engine4"
  echo "-------------------------------------------------------------------"
  echo "stop-jbi-component cli_test_engine4"
  $AS8BASE/bin/asadmin stop-jbi-component --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_engine4
    
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Shut Down the component cli_test_engine4"
  echo "-------------------------------------------------------------------"
  echo "shut-down-jbi-component cli_test_engine4"
  $AS8BASE/bin/asadmin shut-down-jbi-component --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_engine4
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Show the components"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-service-engine cli_test_engine4"
  $AS8BASE/bin/asadmin show-jbi-service-engine --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_engine4
    
}


#----------------------------------------------------------------------------------
# Perform the regression setup and call the function that will run the test
#----------------------------------------------------------------------------------
TEST_NAME="jbi-admin-cli00007"
TEST_DESCRIPTION="Test The Component Upgrade Command"
. ./regress_defs.ksh
run_test

exit 0


