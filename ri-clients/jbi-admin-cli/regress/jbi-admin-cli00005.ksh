#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbi-admin-cli00001.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#


#----------------------------------------------------------------------------------
# Perform any necessary cleanup to restore the repository back to its initial state.
#----------------------------------------------------------------------------------
test_cleanup()
{
  cleanup server
}


#----------------------------------------------------------------------------------
# Main function called that will run the test
#----------------------------------------------------------------------------------
run_test()
{
  initilize_test 
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Try to install a shared library with no operand given."
  echo "-------------------------------------------------------------------"
  echo "install-jbi-shared-library "
  $AS8BASE/bin/asadmin install-jbi-shared-library --target=server --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Try to install a binding component with no operand given."
  echo "-------------------------------------------------------------------"
  echo "install-jbi-component"
  $AS8BASE/bin/asadmin install-jbi-component      --target=server --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS 
      
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Try to install a shared library with a bogus operand given."
  echo "-------------------------------------------------------------------"
  echo "install-jbi-shared-library --target=server fred"
  $AS8BASE/bin/asadmin install-jbi-shared-library --target=server --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS fred 
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Try to install a component with a bogus operand given"
  echo "-------------------------------------------------------------------"
  echo "install-jbi-component  --target=server fred"
  $AS8BASE/bin/asadmin install-jbi-component      --target=server --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS fred 
  
}


#----------------------------------------------------------------------------------
# Perform the regression setup and call the function that will run the test
#----------------------------------------------------------------------------------
TEST_NAME="jbi-admin-cli00005"
TEST_DESCRIPTION="Test Invalid input for install"
. ./regress_defs.ksh
run_test

exit 0


