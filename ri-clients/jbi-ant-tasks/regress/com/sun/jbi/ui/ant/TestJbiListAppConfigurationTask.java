/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestJbiListAppConfigurationTask.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.ant;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import junit.framework.TestCase;

/**
 * test class
 * @author Sun Microsystems, Inc.
 */
public class TestJbiListAppConfigurationTask extends TestCase
{
    
    /**
     * Creates a new instance of TestJbiListAppConfigurationTask
     * @param aTestName name
     */
    public TestJbiListAppConfigurationTask(String aTestName)
    {
        super(aTestName);
    }
    
    /**
     * Test the code that creates a component-task-result from a plain-text exception message
     */
    public void testJbiListAppVariablesFormat()
       throws Exception
    {
        JbiListApplicationConfigurationsTask theTask = new JbiListApplicationConfigurationsTask();
        Class clazz = theTask.getClass();

        java.lang.reflect.Method
               mtd = clazz.getDeclaredMethod("printApplicationConfigurationRecursively",
                   new Class[]{java.util.Properties.class, java.io.PrintWriter.class, java.lang.String.class, java.lang.Integer.TYPE} );
        mtd.setAccessible(true);
	java.io.StringWriter stringWriter = new java.io.StringWriter();
        java.io.PrintWriter msgWriter = new java.io.PrintWriter(stringWriter);
        java.util.Properties theProp = getTheProperties();
        mtd.invoke(theTask, new Object[]{theProp, msgWriter, "", new java.lang.Integer(-1)});
        msgWriter.close();
        java.lang.System.out.print(stringWriter.getBuffer().toString());
    }

    java.util.Properties getTheProperties()
    {
        java.util.Properties theProp = new java.util.Properties();

        theProp.put("name", "testOne");
        theProp.put("Port", "12345");
        theProp.put("username", "empid=scott,empno=101,empstatus=employed");
        theProp.put("jndienv.1", "key11=jndi11,key12=jndi12,key13=jndi13");
        theProp.put("jndienv.2", "key21=jndi21,key22=jndi22,key13=jndi23");
        theProp.put("jmsenv.1", "key11=jms11,key12=jms12,key13=jms13");
        theProp.put("jmsenv.2", "key11=jms21,key12=jms12,key13=jms13");
        theProp.put("mixedentry", "mixedarray1.1=v1,mixedarray1.2=v2,mixedarray1.3=v3,mixedarray2.1=vl1,mixedarray2.2=vl2,mixedarray2.3=vl3");
    
        return theProp;
    }

    /**
     * main
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        // TODO code application logic here
        try
        {
            new TestJbiListAppConfigurationTask("test").testJbiListAppVariablesFormat();
        } catch ( Exception ex )
        {
            ex.printStackTrace();
        }
    }
}
