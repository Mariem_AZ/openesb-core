/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestJbiListAppVariablesTask.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.ant;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import junit.framework.TestCase;

/**
 * test class
 * @author Sun Microsystems, Inc.
 */
public class TestJbiListAppVariablesTask extends TestCase
{
    
    /**
     * Creates a new instance of TestJbiListAppVariablesTask
     * @param aTestName name
     */
    public TestJbiListAppVariablesTask(String aTestName)
    {
        super(aTestName);
    }
    
    /**
     * Test the code that creates a component-task-result from a plain-text exception message
     */
    public void testJbiListAppVariablesFormat()
       throws Exception
    {
        JbiListApplicationVariablesTask theTask = new JbiListApplicationVariablesTask();
        Class clazz = theTask.getClass();

        java.lang.reflect.Method
               mtd = clazz.getDeclaredMethod("printApplicationVariables",
                   new Class[]{ java.lang.String.class, java.util.List.class, java.util.Properties.class, java.lang.String.class, java.io.PrintWriter.class, java.lang.Boolean.TYPE} );
        mtd.setAccessible(true);
        java.util.Properties theProp = getTheProperties();
        java.io.StringWriter stringWriter = new java.io.StringWriter();
        java.io.PrintWriter msgWriter = new java.io.PrintWriter(stringWriter);

        mtd.invoke(theTask, new Object[]{"TestComponent", null, theProp, "MyTarget", msgWriter, new java.lang.Boolean(false)});
        mtd.invoke(theTask, new Object[]{"TestComponent", null, theProp, "MyTarget", msgWriter, new java.lang.Boolean(true)});

        msgWriter.close();
        java.lang.System.out.println(stringWriter.getBuffer().toString());

    }

    java.util.Properties getTheProperties()
    {
        java.util.Properties theProp = new java.util.Properties();

        theProp.put("naaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaame", "testOne");
        theProp.put("Port", "[Number]123455555555555555555555555555555555555555555555555");
        return theProp;
    }

    /**
     * main
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        // TODO code application logic here
        try
        {
            new TestJbiListAppVariablesTask("test").testJbiListAppVariablesFormat();
        } catch ( Exception ex )
        {
            ex.printStackTrace();
        }
    }
}
