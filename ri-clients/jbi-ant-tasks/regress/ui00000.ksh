#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)ui00000.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

#regress setup
. ./regress_defs.ksh

asadmin create-node-agent  -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT ant_agent 2>&1

sleep 10

asadmin create-cluster  -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT ant_cluster 2>&1

sleep 10

asadmin create-instance  -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --cluster ant_cluster --nodeagent ant_agent instance11 2>&1

sleep 10

asadmin create-instance  -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --nodeagent ant_agent instance13 2>&1

sleep 10

asadmin start-node-agent  -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --startinstances=true ant_agent 2>&1

sleep 30

exit 0
