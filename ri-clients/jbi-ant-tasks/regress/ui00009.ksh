#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)ui00009.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

#regress setup
. ./regress_defs.ksh

JBI_ANT="$JBI_ANT -Djbi.task.fail.on.error=false"
echo $JBI_ANT

#
# test components
#

test_partial_deployment()
{

### list all objects. all lists should be empty  ###############################
$JBI_ANT list-shared-libraries
$JBI_ANT list-service-engines
$JBI_ANT list-binding-components
$JBI_ANT list-service-assemblies

### install components/slibs ###################################################
$JBI_ANT -Djbi.install.file=$UI_REGRESS_DIST_DIR/ant-test-sns1.jar install-shared-library
$JBI_ANT -Djbi.install.file=$UI_REGRESS_DIST_DIR/ant-test-binding1.jar  install-component
$JBI_ANT -Djbi.install.file=$UI_REGRESS_DIST_DIR/ant-test-engine3.jar  install-component

### list components should be in shutdown state. ###############################
$JBI_ANT list-shared-libraries
$JBI_ANT list-binding-components
$JBI_ANT list-service-engines

### start components ###########################################################
$JBI_ANT -Djbi.component.name="ant_test_binding1" start-component
$JBI_ANT -Djbi.component.name="ant_test_engine3" start-component

### list components should be in started state. ################################
$JBI_ANT list-binding-components
$JBI_ANT list-service-engines

### deploy service assembly. should print partial success ######################
$JBI_ANT -Djbi.deploy.file=$UI_REGRESS_DIST_DIR/ant-test-sa4.zip deploy-service-assembly

### list service assembly. should be in shutdown state #########################
$JBI_ANT list-service-assemblies

### start service assembly. ####################################################
$JBI_ANT -Djbi.service.assembly.name="ant_test_sa4" start-service-assembly

### list service assembly. should be in started state ##########################
$JBI_ANT list-service-assemblies

### stop service assembly. #####################################################
$JBI_ANT -Djbi.service.assembly.name="ant_test_sa4" stop-service-assembly

### list service assembly. should be in stopped state ##########################
$JBI_ANT list-service-assemblies

### shutdown service assembly. #################################################
$JBI_ANT -Djbi.service.assembly.name="ant_test_sa4" shut-down-service-assembly

### list service assembly. should be in shutdown state  ########################
$JBI_ANT list-service-assemblies

### undeploy service assembly ##################################################
$JBI_ANT -Djbi.service.assembly.name="ant_test_sa4" undeploy-service-assembly

### list service assembly. should be in shutdown state  ########################
$JBI_ANT list-service-assemblies

### force undeploy service assembly ##################################################
$JBI_ANT -Djbi.service.assembly.name="ant_test_sa4" -Djbi.force.task=true undeploy-service-assembly

### list service assembly. should be in shutdown state  ########################
$JBI_ANT list-service-assemblies

### stop component #############################################################
$JBI_ANT -Djbi.component.name="ant_test_binding1" stop-component
$JBI_ANT -Djbi.component.name="ant_test_engine3" stop-component

### list components should be in stop state. ###################################
$JBI_ANT list-binding-components
$JBI_ANT list-service-engines

### shutdown component #############################################################
$JBI_ANT -Djbi.component.name="ant_test_binding1" shut-down-component
$JBI_ANT -Djbi.component.name="ant_test_engine3" shut-down-component

### list components should be in shutdown state. ###############################
$JBI_ANT list-binding-components
$JBI_ANT list-service-engines

### uninstall components #######################################################
$JBI_ANT -Djbi.component.name=ant_test_engine3 -Djbi.force.task=true uninstall-component
$JBI_ANT -Djbi.component.name=ant_test_binding1 uninstall-component
$JBI_ANT -Djbi.shared.library.name="ant_test_sns1" uninstall-shared-library

### list all objects. all lists should be empty ################################
$JBI_ANT list-shared-libraries
$JBI_ANT list-service-engines
$JBI_ANT list-binding-components
$JBI_ANT list-service-assemblies

}


run_test()
{
# ant -emacs -debug -f $JV_SVC_REGRESS/scripts/build-test-components.ant
build_test_artifacts
test_partial_deployment
}

################## MAIN ##################
####
# Execute the test
####

#this is to correct for differences in ant behavior from version 1.5->1.6.  RT 6/18/05
run_test | tr -d '\r' | sed -e '/^$/d'

exit 0
