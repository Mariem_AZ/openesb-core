/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JbiUndeployServiceAssemblyTask.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.ant;

import org.apache.tools.ant.BuildException;


/** This class is a ant task for undeploying a service assembly.
 *
 * @author Sun Microsystems, Inc.
 */
public class JbiUndeployServiceAssemblyTask extends JbiTargetTask
{    
    /**
     * success msg key
     */
    private static final String PARTIAL_SUCCESS_STATUS_KEY = "jbi.ui.ant.undeploy.partial.success";
    
    /**
     * success msg key
     */
    private static final String SUCCESS_STATUS_KEY = "jbi.ui.ant.undeploy.successful";
    /**
     * failure msg key
     */
    private static final String FAILED_STATUS_KEY = "jbi.ui.ant.undeploy.failed";
    
    /**
     * Holds value of property KeepArchive.
     */
    private boolean mKeepArchive = false; 
        
    /** Holds value of property ServiceAssemblyName. */
    private String mServiceAssemblyName;
    
    /**
     * Holds value of property force.
     */
    private boolean mForce = false; 
    
    /**
     * Getter for property forced.
     * @return Value of property forced.
     */
    public boolean isForce()
    {

        return this.mForce;
    }

    /**
     * Setter for property forced.
     * @param force New value of property forced.
     */
    public void setForce(boolean force)
    {

        this.mForce = force;
    }
    
    /** Getter for property ServiceAssemblyName.
     * @return Value of property ServiceAssemblyName.
     */
    public String getName()
    {
        return this.mServiceAssemblyName;
    }
    
    /** Setter for property ServiceAssemblyName.
     * @param name New value of property ServiceAssemblyName.
     */
    public void setName(String name)
    {
        this.mServiceAssemblyName = name;
    }
    
    /**
     * Getter for property KeepArchive.
     * @return Value of property KeepArchive.
     */
    public boolean isKeepArchive()
    {

        return this.mKeepArchive;
    }

    /**
     * Setter for property KeepArchive.
     * @param keepArchive New value of property KeepArchive.
     */
    public void setKeepArchive(boolean keepArchive)
    {

        this.mKeepArchive = keepArchive;
    }
    
    /** executes the undeploy service assembly task. Ant Task framework calls this method to
     * excute the task.
     * @throws BuildException if error or exception occurs.
     */
    public void executeTask() throws BuildException
    {
        
        String saName = getValidServiceAssemblyName(getName());
        String target = getValidTarget();
        boolean force = isForce();
        boolean keepArchive = isKeepArchive();
        String result = null;
                        
        try
        {
            result = this.getJBIAdminCommands().undeployServiceAssembly(saName, force, keepArchive, target);
        }
        catch (Exception ex )
        {
            processTaskException(ex);
        }
        
        processTaskResult(result);
        
    }
    
    /**
     * returns i18n key. tasks implement this method.
     * @return i18n key for the success status
     */
    protected String getTaskFailedStatusI18NKey()
    {
        return FAILED_STATUS_KEY;
    }
    /**
     * returns i18n key. tasks implement this method.
     * @return i18n key for the failed status
     */
    protected String getTaskSuccessStatusI18NKey()
    {
        return SUCCESS_STATUS_KEY;
    }
    
    /**
     * return i18n key for the partial success
     * @return i18n key for the partial success
     */
    protected String getTaskPartialSuccessStatusI18NKey() 
    {
        return PARTIAL_SUCCESS_STATUS_KEY;
    }
    
}
