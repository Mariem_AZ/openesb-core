/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JbiExportApplicationEnvTask.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.ant;

import org.apache.tools.ant.BuildException;


/** This class is an ant task for exporting the application environment
 *
 * @author Sun Microsystems, Inc.
 */
public class JbiExportApplicationEnvTask extends JbiTargetTask
{
    /**
     * success msg key
     */
    private static final String SUCCESS_STATUS_KEY = "jbi.ui.ant.export.app.env.successful";
    /**
     * failure msg key
     */
    private static final String FAILED_STATUS_KEY = "jbi.ui.ant.export.app.env.failed";
    
    
    /** Holds value of property serviceassemblyname. */
    private String mServiceAssemblyName;
    
    /** Holds value of property configdir . */
    private String mConfigDir;
    
    /** Getter for property serviceassemblyname
     * @return Value of property serviceassemblyname.
     *
     */
    public String getServiceAssemblyName()
    {
        return this.mServiceAssemblyName;
    }
    
    /**
     * Setter for property serviceassemblyName.
     * @param serviceAssemblyName service assembly name.
     */
    public void setServiceAssemblyName(String serviceAssemblyName)
    {
        this.mServiceAssemblyName = serviceAssemblyName;
    }

    /** Getter for property configdir
     * @return Value of property configdir.
     *
     */
    public String getConfigDir()
    {
        return this.mConfigDir;
    }
   
    /**
     * Setter for property configDir.
     * @param configDir the export direcotry for the config files.
     */
    public void setConfigDir(String configDir)
    {
        this.mConfigDir = configDir;
    }

    /** executes the stop componentnt task. Ant Task framework calls this method to
     * excute the task.
     * @throws BuildException if error or exception occurs.
     */
    public void executeTask() throws BuildException
    {

        String target = getValidTarget();
                
        try
        {
            String appName = "" + getServiceAssemblyName();
            String configDir = "" + getConfigDir();

	    this.logDebug("appName: " + appName +
                          " target: " + target +
                          " getConfigDir: " + configDir);
            String result =
                this.getJBIAdminCommands().exportApplicationConfiguration(
                                   appName,
                                   target,
                                   configDir);
	    this.logDebug("Result: " + result);

            String returnMsg = getI18NBundle().getMessage(
                                "jbi.ui.ant.export.app.env.return.msg",
                                new String [] { configDir } );
            printTaskSuccess(returnMsg);
            
        }
        catch (Exception ex )
        {
            processTaskException(ex);
        }
        
    }
    
    /**
     * returns i18n key. tasks implement this method.
     * @return i18n key for the success status
     */
    protected String getTaskFailedStatusI18NKey()
    {
        return FAILED_STATUS_KEY;
    }
    /**
     * returns i18n key. tasks implement this method.
     * @return i18n key for the failed status
     */
    protected String getTaskSuccessStatusI18NKey()
    {
        return SUCCESS_STATUS_KEY;
    }
}
