/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JbiSetComponentLoggersTask.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.ant;

import com.sun.jbi.ui.common.JBIManagementMessage;
import com.sun.jbi.ui.common.JBIResultXmlBuilder;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Properties;
import org.apache.tools.ant.BuildException;

/** This class is an ant task for updating service engine or binding component.
 *
 * @author Sun Microsystems, Inc.
 */
public class JbiSetComponentLoggersTask extends JbiTargetTask
{
    /**
     * logger success msg key
     */
    private static final String LOGGER_SUCCESS_STATUS_KEY = "jbi.ui.ant.set.logger.successful";

    /**
     * logger failure msg key
     */
    private static final String LOGGER_FAILED_STATUS_KEY = "jbi.ui.ant.set.logger.failed";
   
    /**
     * logger partial success msg key
     */
    private static final String LOGGER_PARTIAL_SUCCESS_STATUS_KEY = "jbi.ui.ant.set.logger.partial.success";

    /** Holds Nested element list of &lt;logger> */
    private List mLoggerList;
    
    /** Holds value of property componentName. */
    private String mComponentName = null;
    
    /**
     * Getter for property componentName.
     * @return Value of property componentName.
     */
    public String getComponentName()
    {
        return this.mComponentName;
    }
    
    /**
     * Setter for property componentName.
     * @param componentName name of the component.
     */
    public void setComponentName(String componentName)
    {
        this.mComponentName = componentName;
    }
    
    private void debugPrintParams(Properties params) {
        if( params == null ) {
            this.logDebug("Set Configuration params are NULL");
            return;
        }
        StringWriter stringWriter = new StringWriter();
        PrintWriter out = new PrintWriter(stringWriter);
        params.list(out);
        out.close();
        this.logDebug(stringWriter.getBuffer().toString());
    }
    
    private String createFormatedSuccessJbiResultMessage(String i18nKey, Object[] args) {
        
        String msgCode = getI18NBundle().getMessage(i18nKey + ".ID");
        String msg = getI18NBundle().getMessage(i18nKey, args);
        
        String jbiResultXml =
            JBIResultXmlBuilder.getInstance()
            .createJbiResultXml("JBI_ANT_TASK_SET_CONFIG",
            JBIResultXmlBuilder.SUCCESS_RESULT,
            JBIResultXmlBuilder.INFO_MSG_TYPE,
            msgCode, msg, args, null);
        
        JBIManagementMessage mgmtMsg = null;
        mgmtMsg = JBIManagementMessage.createJBIManagementMessage(jbiResultXml);
        return (mgmtMsg != null) ? mgmtMsg.getMessage() : msg ;
    }
    
    
    private void executeSetComponentLoggers(String componentName)
	throws BuildException
    {

        try
        {
	    // Go throught the static/constant configuration paramter elements
	    String	target		= getValidTarget();
	    List	loggerList	= this.getLoggerList();

            this.logDebug("Executing Set logger level ....");

            // Go throught the logger elements
            Iterator it = loggerList.iterator();
            boolean emptyFlag = true;

            while(it.hasNext())
            {
                Logger logger = (Logger) it.next();
                
                String theName = "" + logger.getName();
                if (theName.compareTo("") != 0)
                {
                    if ((""+logger.getLevel()).toUpperCase().compareTo("DEFAULT") != 0)
                    {
                        this.logDebug("component name: " + componentName +
                                " logger name: " + logger.getName() +
                                " logger level: " +
                                java.util.logging.Level.parse(logger.getLevel()) +
                                " target: " + target);
                        this.getJBIAdminCommands().setComponentLoggerLevel(componentName,
                                logger.getName(),
                                java.util.logging.Level.parse(logger.getLevel()),
                                target,
                                target);
                    }
                    else
                    {
                        this.getJBIAdminCommands().setComponentLoggerLevel(componentName,
                                logger.getName(),
                                null,
                                target,
                                target);
                    }

                    emptyFlag = false;
                }
            }

            if (emptyFlag == true)
            {
                String msg =
                    createFailedFormattedJbiAdminResult(
                        "jbi.ui.ant.task.error.no.input.component.logger.data.found",
                        null);
                throw new BuildException(msg,getLocation());
            }

            String i18nKey = "jbi.ui.ant.task.info.logger.comp.done.on.target";
            Object[] args = {componentName, target};
            this.printTaskSuccess(createFormatedSuccessJbiResultMessage(i18nKey, args));
        }
        catch (Exception ex )
        {
            processTaskException(ex);
        }                
    }

    /** executes the install task. Ant Task framework calls this method to
     * excute the task.
     * @throws BuildException if error or exception occurs.
     */
    public void executeTask() throws BuildException
    {        
        this.logDebug("Executing Set Configuration Task....");
        String compName = getComponentName();
        
        executeSetComponentLoggers(compName);
    }
    
    /**
     * returns i18n key. tasks implement this method.
     * @return i18n key for the success status
     */
    protected String getTaskFailedStatusI18NKey()
    {
        return LOGGER_FAILED_STATUS_KEY;
    }

    /**
     * returns i18n key. tasks implement this method.
     * @return i18n key for the failed status
     */
    protected String getTaskSuccessStatusI18NKey()
    {
        return LOGGER_SUCCESS_STATUS_KEY;
    }

    /**
     * return i18n key for the partial success
     * @return i18n key for the partial success
     */
    protected String getTaskPartialSuccessStatusI18NKey() 
    {
        return LOGGER_PARTIAL_SUCCESS_STATUS_KEY;
    }    

    /**
     * returns nested element list of &lt;logger>
     * @return Paramter List
     */
    protected List getLoggerList()
    {
        if ( this.mLoggerList == null )
        {
            this.mLoggerList = new ArrayList();
        }
        return this.mLoggerList;
    }

    /**
     * factory method for creating the nested element &lt;logger>
     * @return Param Object
     */
    public Logger createLogger()
    {
        Logger logger = new Logger();
        this.getLoggerList().add(logger);
        return logger;
    }
}
