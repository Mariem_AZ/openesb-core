/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JbiDeleteApplicationConfigurationsTask.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.ant;

import com.sun.jbi.ui.common.JBIManagementMessage;
import com.sun.jbi.ui.common.JBIResultXmlBuilder;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.TreeMap;
import java.util.Properties;
import org.apache.tools.ant.BuildException;

/** This class is an ant task for updating service engine or binding component.
 *
 * @author Sun Microsystems, Inc.
 */
public class JbiDeleteApplicationConfigurationsTask extends JbiTargetTask
{
    /**
     * appconfig success msg key
     */
    private static final String APPCONFIG_SUCCESS_STATUS_KEY =
				"jbi.ui.ant.delete.appconfig.successful";

    /**
     * appconfig failure msg key
     */
    private static final String APPCONFIG_FAILED_STATUS_KEY =
				"jbi.ui.ant.delete.appconfig.failed";
   
    /**
     * appconfig success msg key
     */
    private static final String APPCONFIG_PARTIAL_SUCCESS_STATUS_KEY =
				"jbi.ui.ant.delete.appconfig.partial.success";
    
    /** Holds appconfig Nested elements */
    private List mAppConfigList;

    /** Holds value of property componentName. */
    private String mComponentName = null;
    
    /**
     * Getter for property componentName.
     * @return Value of property componentName.
     */
    public String getComponentName()
    {
        return this.mComponentName;
    }
    
    /**
     * Setter for property componentName.
     * @param componentName name of the component.
     */
    public void setComponentName(String componentName)
    {
        this.mComponentName = componentName;
    }
    
    private void debugPrintParams(Properties params) {
        if( params == null ) {
            this.logDebug("Set Configuration params are NULL");
            return;
        }
        StringWriter stringWriter = new StringWriter();
        PrintWriter out = new PrintWriter(stringWriter);
        params.list(out);
        out.close();
        this.logDebug(stringWriter.getBuffer().toString());
    }
    
    private String createFormatedSuccessJbiResultMessage(String i18nKey, Object[] args) {
        
        String msgCode = getI18NBundle().getMessage(i18nKey + ".ID");
        String msg = getI18NBundle().getMessage(i18nKey, args);
        
        String jbiResultXml =
            JBIResultXmlBuilder.getInstance()
            .createJbiResultXml("JBI_ANT_TASK_SET_CONFIG",
            JBIResultXmlBuilder.SUCCESS_RESULT,
            JBIResultXmlBuilder.INFO_MSG_TYPE,
            msgCode, msg, args, null);
        
        JBIManagementMessage mgmtMsg = null;
        mgmtMsg = JBIManagementMessage.createJBIManagementMessage(jbiResultXml);
        return (mgmtMsg != null) ? mgmtMsg.getMessage() : msg ;
    }
    
    private void executeDeleteApplicationConfigurations(List appConfigList,
							String compName, String target)
        throws Exception, BuildException
    {
        // Go throught the appconfig elements
	Properties appConfigParamProps = new Properties();

        Iterator it = appConfigList.iterator();
        while(it.hasNext())
        {
	    Param	appConfig = (Param) it.next();
	    String appConfigName = appConfig.getName();
            this.logDebug("Delete application configuration, component name: " + compName +
                        " Application Configuration name: " + appConfigName +
                        " target: " + target);

            String rtnXml =
		this.getJBIAdminCommands().deleteApplicationConfiguration(compName,
							target,
							appConfigName);

	    JBIManagementMessage mgmtMsg =
			JBIManagementMessage.createJBIManagementMessage(rtnXml);
            if ( mgmtMsg.isFailedMsg() )
            {
	        throw new Exception(rtnXml);
            }
            else
            {
                // print success message
                printTaskSuccess(mgmtMsg);
            }
	}
    }

    /** executes the install task. Ant Task framework calls this method to
     * excute the task.
     * @throws BuildException if error or exception occurs.
     */
    public void executeTask() throws BuildException
    {        
	try
	{
	    String	compName	= getComponentName();
	    String	target		= getValidTarget();
            List	appConfigList	= this.getAppConfigList();

	    if ((compName == null) || (compName.compareTo("") == 0))
	    {
		String errMsg = createFailedFormattedJbiAdminResult(
						"jbi.ui.ant.task.error.nullCompName",
						null);
		throw new BuildException(errMsg);
	    }

            this.logDebug("Executing delete application configurations Task....");
            executeDeleteApplicationConfigurations(appConfigList, compName, target);
	}
	catch (Exception ex )
        {
            processTaskException(ex);
        }
    }
    
    /**
     * returns i18n key. tasks implement this method.
     * @return i18n key for the success status
     */
    protected String getTaskFailedStatusI18NKey()
    {
	return APPCONFIG_FAILED_STATUS_KEY;
    }

    /**
     * returns i18n key. tasks implement this method.
     * @return i18n key for the failed status
     */
    protected String getTaskSuccessStatusI18NKey()
    {
        return APPCONFIG_SUCCESS_STATUS_KEY;
    }

    /**
     * return i18n key for the partial success
     * @return i18n key for the partial success
     */
    protected String getTaskPartialSuccessStatusI18NKey() 
    {
        return APPCONFIG_PARTIAL_SUCCESS_STATUS_KEY;
    }    

    /**
     * returns AppConfig element list
     * @return AppConfig List
     */
    protected List getAppConfigList()
    {
        if ( this.mAppConfigList == null )
        {
            this.mAppConfigList = new ArrayList();
        }
        return this.mAppConfigList;
    }

    /**
     * factory method for creating the nested element &lt;appconfig>
     * @return AppConfig Object
     */
    public Param createAppConfig()
    {
	Param appConfig = new Param();
	this.getAppConfigList().add(appConfig);
	return appConfig;
    }
}
