<!--
 DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 
 Copyright 1997-2008 Sun Microsystems, Inc. All rights reserved.
 
 The contents of this file are subject to the terms of either the GNU
 General Public License Version 2 only ("GPL") or the Common Development
 and Distribution License("CDDL") (collectively, the "License").  You
 may not use this file except in compliance with the License. You can obtain
 a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 language governing permissions and limitations under the License.
 
 When distributing the software, include this License Header Notice in each
 file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 Sun designates this particular file as subject to the "Classpath" exception
 as provided by Sun in the GPL Version 2 section of the License file that
 accompanied this code.  If applicable, add the following below the License
 Header, with the fields enclosed by brackets [] replaced by your own
 identifying information: "Portions Copyrighted [year]
 [name of copyright owner]"
 
 Contributor(s):
 
 If you wish your version of this file to be governed by only the CDDL or
 only the GPL Version 2, indicate your decision by adding "[Contributor]
 elects to include this software in this distribution under the [CDDL or GPL
 Version 2] license."  If you don't indicate a single choice of license, a
 recipient has the option to distribute your version of this file under
 either the CDDL, the GPL Version 2 or to extend the choice of license to
 its licensees as provided above.  However, if you add GPL Version 2 code
 and therefore, elected the GPL Version 2 license, then the option applies
 only if the new code is made subject to such option by the copyright
 holder.
-->
<!-- jbi/pe/inc/sharedDepsTable.inc -->

 <sun:form id="sharedDepsTableForm">

                <sun:title
                    id="jbiDependenciesPage"
                    title='#{sessionScope.sharedShowName} - #{sessionScope.isJbiLibDep ? "$resource{i18n.jbi.show.lib.dependencies.page.title.suffix.text}" : "$resource{i18n.jbi.show.comp.dependencies.sl.page.title.suffix.text}"}'
                    helpText='#{sessionScope.isJbiLibDep ? "$resource{i18n.jbi.show.lib.dependencies.page.help.inline.text}" : "$resource{i18n.jbi.show.comp.dependencies.sl.page.help.inline.text}"}'
                    >
                </sun:title>

<!-- TBD: move this to a shared inc/*.inc file; parameterize for SA, BC|SE, or SL usage 
-->
	        <sun:table id="sharedDepsTable" title='#{sessionScope.isJbiLibDep ? "$resource{i18n.jbi.show.lib.dependencies.table.title.text}" : "$resource{i18n.jbi.show.comp.dependencies.table.title.text}"}'
                    paginateButton="#{true}"
                    paginationControls="#{true}"
                    >
 		    

                    <sun:tableRowGroup id="sharedDepsTableRowGroup"
                         aboveColumnHeader="#{true}"
			 sourceData="#{ListBean.sharedDepsTableData}"
			 sourceVar="sourceVarRow" 
                         >
                         <sun:tableColumn id="sharedDepsNamesTableColumn" 
                              headerText="$resource{i18n.jbi.shared.deps.table.column.name.text}"
                              rowHeader="#{true}"
                              sort="#{sourceVarRow.value.name}"                             
                              >
                              <sun:hyperlink id="sharedDepsTableNameHyperlink"
                                    rendered="#{!((ShowBean.name == sourceVarRow.value.name) && (sessionScope['renderShow'])) }"
                                    style="font-weight:normal"
                                    toolTip="$resource{i18n.jbi.shared.deps.table.name.link.tooltip}" 
                                    value="#{sourceVarRow.value.name}"
			            url='#{(("service-engine" == sourceVarRow.value.type)||("binding-component" == sourceVarRow.value.type))? "showBindingOrEngine.jsf" : "showLibrary.jsf"}?type=#{sourceVarRow.value.type}&name=#{sourceVarRow.value.name}' 
                                    />   
                         </sun:tableColumn>

                         <sun:tableColumn id="sharedTypeTableColumn" 
                              headerText="$resource{i18n.jbi.shared.deps.table.column.type.text}"
 		              rendered='#{sessionScope.isJbiLibDep}'
                              rowHeader="#{true}"
                              sort="#{sourceVarRow.value.type}"                             
                              >

                              <sun:staticText id="sharedTypeBindingText"
	                          rendered="#{'binding-component' == sourceVarRow.value.type}" 
                                  value="$resource{i18n.jbi.list.type.binding-component}"
                                  />

                              <sun:staticText id="sharedTypeEngineText"
	                          rendered="#{'service-engine' == sourceVarRow.value.type}" 
                                  value="$resource{i18n.jbi.list.type.service-engine}"
                                  />

                         </sun:tableColumn>

                         <sun:tableColumn id="sharedDescriptionTableColumn" 
                              headerText="$resource{i18n.jbi.shared.deps.table.column.description.text}"
                              rowHeader="#{true}"
                              sort="#{sourceVarRow.value.description}"                             
                              >
                              <sun:staticText id="sharedDescriptionText" 
                                  value="#{sourceVarRow.value.description}"
                                  />
                         </sun:tableColumn>
                         
                    </sun:tableRowGroup>
                </sun:table>

</sun:form>
