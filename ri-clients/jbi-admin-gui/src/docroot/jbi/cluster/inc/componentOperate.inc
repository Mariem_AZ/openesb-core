<!--
 DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 
 Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 
 The contents of this file are subject to the terms of either the GNU
 General Public License Version 2 only ("GPL") or the Common Development
 and Distribution License("CDDL") (collectively, the "License").  You
 may not use this file except in compliance with the License. You can obtain
 a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 language governing permissions and limitations under the License.
 
 When distributing the software, include this License Header Notice in each
 file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 Sun designates this particular file as subject to the "Classpath" exception
 as provided by Sun in the GPL Version 2 section of the License file that
 accompanied this code.  If applicable, add the following below the License
 Header, with the fields enclosed by brackets [] replaced by your own
 identifying information: "Portions Copyrighted [year]
 [name of copyright owner]"
 
 Contributor(s):
 
 If you wish your version of this file to be governed by only the CDDL or
 only the GPL Version 2, indicate your decision by adding "[Contributor]
 elects to include this software in this distribution under the [CDDL or GPL
 Version 2] license."  If you don't indicate a single choice of license, a
 recipient has the option to distribute your version of this file under
 either the CDDL, the GPL Version 2 or to extend the choice of license to
 its licensees as provided above.  However, if you add GPL Version 2 code
 and therefore, elected the GPL Version 2 license, then the option applies
 only if the new code is made subject to such option by the copyright
 holder.
-->
<!-- jbi/cluster/inc/componentOperate.inc -->
<!beforeCreate
  <!-- compute padding-left pixel size in order to line up the values in the property sheet -->
  if (#{ShowBean.showVersionInfo}) {
     setSessionAttribute(key="paddingPixel" value="padding-left: 30px")
     setSessionAttribute(key="blankSpace" value="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;")
  }
  
  if (!#{ShowBean.showVersionInfo}) {
     setSessionAttribute(key="paddingPixel" value="padding-left: 20px")
     setSessionAttribute(key="blankSpace" value="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;")
  }
  
  if (#{ShowBean.noTarget}) {
    setSessionAttribute(key="actionsHelpTxt" value='#{ShowBean.type=="service-assembly" ? "$resource{i18n.jbi.cluster.sa.general.manage.target.helpInline}" : "$resource{i18n.jbi.cluster.component.general.manage.target.helpInline}"}')    
  }
  
  if (!#{ShowBean.noTarget}) {
     setSessionAttribute(key="actionsHelpTxt" value='#{ShowBean.type=="service-assembly" ? "$resource{i18n.jbi.cluster.sa.general.actions.helpInline}" : "$resource{i18n.jbi.cluster.component.general.actions.helpInline}"}')  
  }

  setSessionAttribute(key="showBreadcrumbText", value='#{"service-assembly"==sessionScope.sharedShowType ? "$resource{i18n.jbi.show.targets.page.title.deployment.suffix.text}" : ("binding-component"==sessionScope.sharedShowType ?  "$resource{i18n.jbi.show.targets.page.title.binding.suffix.text}" : ("service-engine"==sessionScope.sharedShowType ?  "$resource{i18n.jbi.show.targets.page.title.engine.suffix.text}" :  "$resource{i18n.jbi.show.targets.page.title.library.suffix.text}"))}')    
/>

 <sun:propertySheet id="statePropertySheet">			 
    <!-- Text Field section -->               
    <sun:propertySheetSection id="statePropertySheetSection" label="$resource{i18n.jbi.component.operation.label}" >
        <sun:property id="showStatus"  
            label="$resource{i18n.jbi.wizard.status.label}"
            labelAlign="left" 
            noWrap="#{true}" 
            overlapLabel="#{false}" 
            rendered="#{(ShowBean.type == 'binding-component') || (ShowBean.type == 'service-engine')|| (ShowBean.type == 'service-assembly')}"            
            >  
            <sun:staticText id="showStateText"
                style="#{paddingPixel}"  
                text="#{ShowBean.summaryStatus}"
                />                                     
        </sun:property>
        
        <sun:property id="showActions"  
            label="$resource{i18n.jbi.show.actions.property.label}"
            labelAlign="left" 
            noWrap="#{true}" 
            overlapLabel="#{false}" 
            helpText="#{blankSpace}#{actionsHelpTxt}"
            >

            <!-- this tag is a place holder to line up the value -->
            <sun:staticText id="showBlankText"
                 style="#{paddingPixel}"
                 text=""
            />
              
            <sun:panelGroup id="topButtons">
            <sun:button id="manageTargetButton"                 
                 primary="true"
                 text="$resource{i18n2.button.ManageTarget}"
                 rendered="#{ShowBean.noTarget}"                 
            >
       <!command
           navigate(page="jbi/cluster/manageJBITargets.jsf");
       />            
            </sun:button>
                       
            <sun:button id="enableButton"                 
                 primary="#{ShowBean.state == ShowBean.STARTED_STATE ? $boolean{false} : $boolean{true}}"
                 text="$resource{i18n2.button.Enable}"
                 rendered="#{ShowBean.noTarget == 'false'}"                 
            >
            
<!command    
    jbiOperateClusteredComponent(componentBean="#{ShowBean}", isEnabled="$boolean{true}", isAlertNeeded=>$session{isJbiAlertNeeded}, alertSummary=>$session{jbiAlertSummary}, alertDetails=>$session{jbiAlertDetails});
    jbiIncrementAlertCountIfNeeded(isAlertNeeded='$session{isJbiAlertNeeded}')
    setPageSessionAttribute(key="anchor", value="#{sessionScope.isJbiAlertNeeded ? 'list'  : 'show' }")
    redirect(page="#{sessionScope.showNameLinkUrl}?type=#{ShowBean.type}&name=#{ShowBean.name}");    
/>            
            </sun:button>
            
            <sun:button id="disableButton"
                 primary="#{ShowBean.state == ShowBean.STARTED_STATE ? $boolean{true} : $boolean{false}}"
                 text="$resource{i18n2.button.Disable}"
                 rendered="#{ShowBean.noTarget == 'false'}"
            >
<!command    
    jbiOperateClusteredComponent(componentBean="#{ShowBean}", isEnabled="$boolean{false}", isAlertNeeded=>$session{isJbiAlertNeeded}, alertSummary=>$session{jbiAlertSummary}, alertDetails=>$session{jbiAlertDetails});
    jbiIncrementAlertCountIfNeeded(isAlertNeeded='$session{isJbiAlertNeeded}')
    setPageSessionAttribute(key="anchor", value="#{sessionScope.isJbiAlertNeeded ? 'list'  : 'show' }")
    redirect(page="#{sessionScope.showNameLinkUrl}?type=#{ShowBean.type}&name=#{ShowBean.name}");    
/>
            </sun:button>
            </sun:panelGroup>
            
        </sun:property>
                
    </sun:propertySheetSection>
 </sun:propertySheet>
