/*
 * An XML document type.
 * Localname: types
 * Namespace: http://www.w3.org/ns/wsdl
 * Java type: org.w3.ns.wsdl.TypesDocument
 *
 * Automatically generated - do not modify.
 */
package org.w3.ns.wsdl.impl;
/**
 * A document containing one types(@http://www.w3.org/ns/wsdl) element.
 *
 * This is a complex type.
 */
public class TypesDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements org.w3.ns.wsdl.TypesDocument
{
    
    public TypesDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName TYPES$0 = 
        new javax.xml.namespace.QName("http://www.w3.org/ns/wsdl", "types");
    
    
    /**
     * Gets the "types" element
     */
    public org.w3.ns.wsdl.TypesType getTypes()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.TypesType target = null;
            target = (org.w3.ns.wsdl.TypesType)get_store().find_element_user(TYPES$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "types" element
     */
    public void setTypes(org.w3.ns.wsdl.TypesType types)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.TypesType target = null;
            target = (org.w3.ns.wsdl.TypesType)get_store().find_element_user(TYPES$0, 0);
            if (target == null)
            {
                target = (org.w3.ns.wsdl.TypesType)get_store().add_element_user(TYPES$0);
            }
            target.set(types);
        }
    }
    
    /**
     * Appends and returns a new empty "types" element
     */
    public org.w3.ns.wsdl.TypesType addNewTypes()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.TypesType target = null;
            target = (org.w3.ns.wsdl.TypesType)get_store().add_element_user(TYPES$0);
            return target;
        }
    }
}
