/*
 * XML Type:  ElementReferenceType
 * Namespace: http://www.w3.org/ns/wsdl
 * Java type: org.w3.ns.wsdl.ElementReferenceType
 *
 * Automatically generated - do not modify.
 */
package org.w3.ns.wsdl.impl;
/**
 * An XML ElementReferenceType(@http://www.w3.org/ns/wsdl).
 *
 * This is a union type. Instances are of one of the following types:
 *     org.apache.xmlbeans.XmlQName
 *     org.w3.ns.wsdl.ElementReferenceType$Member
 */
public class ElementReferenceTypeImpl extends org.apache.xmlbeans.impl.values.XmlUnionImpl implements org.w3.ns.wsdl.ElementReferenceType, org.apache.xmlbeans.XmlQName, org.w3.ns.wsdl.ElementReferenceType.Member
{
    
    public ElementReferenceTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected ElementReferenceTypeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
    /**
     * An anonymous inner XML type.
     *
     * This is an atomic type that is a restriction of org.w3.ns.wsdl.ElementReferenceType$Member.
     */
    public static class MemberImpl extends org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx implements org.w3.ns.wsdl.ElementReferenceType.Member
    {
        
        public MemberImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected MemberImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
