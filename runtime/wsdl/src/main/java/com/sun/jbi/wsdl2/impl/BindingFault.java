/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)BindingFault.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.wsdl2.impl;

import org.w3.ns.wsdl.BindingFaultType;

/**
 * Abstract implementation of
 * WSDL 2.0 Binding Fault container
 *
 * @author Sun Microsystems, Inc.
 */
abstract class BindingFault extends ExtensibleDocumentedComponent
    implements com.sun.jbi.wsdl2.BindingFault
{
    /**
     * Get the XML bean for this binding fault component.
     *
     * @return The definitions XML bean for this component.
     */
    protected final BindingFaultType getBean()
    {
        return (BindingFaultType) this.mXmlObject;
    }

    /**
     * Construct an abstract Binding fault implementation base component.
     * 
     * @param bean The XML bean for this Binding Fault component
     */
    BindingFault(BindingFaultType bean)
    {
        super(bean);
    }

}

// End-of-file: BindingFault.java
