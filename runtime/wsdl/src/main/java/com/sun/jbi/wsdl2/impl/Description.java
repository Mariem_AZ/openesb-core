/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Description.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.wsdl2.impl;

import javax.xml.namespace.QName;
import org.w3.ns.wsdl.DescriptionType;

/**
 * Abstract implementation of
 * WSDL 2.0 Description container
 *
 * @author Sun Microsystems, Inc.
 */
abstract class Description extends ExtensibleDocumentedComponent
    implements com.sun.jbi.wsdl2.Definitions
{
    /**
     * Get the XML bean for this definitions component.
     *
     * @return The definitions XML bean for this component.
     */
    protected final DescriptionType getBean()
    {
        return (DescriptionType) this.mXmlObject;
    }

    /**
     * Construct an abstract Description fault reference implementation base component.
     * 
     * @param bean The XML bean for this Description component
     */
    Description(DescriptionType bean)
    {
        super(bean);
    }

    /**
     * Find named binding in this definition or the imported/included
     * bindings.
     *
     * @param name Name of binding to find.
     * @return Named Binding; null if none found.
     */
    public abstract com.sun.jbi.wsdl2.Binding findBinding(
        QName name);

    /**
     * Find named interface in this definition or the imported/included
     * interfaces.
     *
     * @param name Name of interface to find.
     * @return Named Interface; null if none found.
     */
    public abstract com.sun.jbi.wsdl2.Interface findInterface(
        QName name);

    /**
     * Find named service in this definition or the imported/included
     * services.
     *
     * @param name Name of service to find.
     * @return Named Service; null if none found.
     */
    public abstract com.sun.jbi.wsdl2.Service findService(
        QName name);

    /**
     * Create a new binding component, appended to this definition's binding
     * list.
     *
     * @param name Name of binding to create.
     * @return Newly created binding, appended to the bindings list.
     */
    public abstract com.sun.jbi.wsdl2.Binding addNewBinding(
        String name);

    /**
     * Create a new import component, appended to this definition's import
     * list.
     *
     * @return Newly created import, appended to the imports list.
     */
    public abstract com.sun.jbi.wsdl2.Import addNewImport();

    /**
     * Create a new include, appended to this definition's include list.
     *
     * @return Newly created include, appended to the includes list.
     */
    public abstract com.sun.jbi.wsdl2.Include addNewInclude();

    /**
     * Create a new interface component, appended to this definition's
     * interface list.
     *
     * @param name Name of interface to create.
     * @return Newly created interface, appended to interfaces list.
     */
    public abstract com.sun.jbi.wsdl2.Interface addNewInterface(
        String name);

    /**
     * Create a new service component, appended to this definition's service
     * list.
     *
     * @param name Name of service to create.
     * @return Newly created service, appended to the services list.
     */
    public abstract com.sun.jbi.wsdl2.Service addNewService(
        String name);

    /**
     * Create a new types component, replacing the existing types component
     * of this definition, if necessary.
     *
     * @return Newly created Types component.
     */
    public abstract com.sun.jbi.wsdl2.Types newTypes();

    /**
     * Return this WSDL definition as an XML string.
     *
     * @return This definition, serialized as an XML string.
     */
    public abstract String toXmlString();

    /**
     * Return this document as a DOM document. The DOM tree is a copy;
     * altering it will not affect this definitions component.
     *
     * @return This definition, as a DOM document.
     */
    public abstract org.w3c.dom.Document toXmlDocument();

}

// End-of-file: Description.java
