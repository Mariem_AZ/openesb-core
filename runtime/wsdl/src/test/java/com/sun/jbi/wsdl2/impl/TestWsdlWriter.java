/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestWsdlWriter.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.wsdl2.impl;

import com.sun.jbi.wsdl2.Description;
import com.sun.jbi.wsdl2.Definitions;
import com.sun.jbi.wsdl2.Interface;

import java.io.StringWriter;
import com.sun.jbi.wsdl2.InterfaceOperation;

/**
 * Unit tests for the WSDL factory.
 * 
 * @author Sun Microsystems Inc.
 */
public class TestWsdlWriter extends junit.framework.TestCase
{
    /** Writer to be used in various tests */
    private com.sun.jbi.wsdl2.WsdlWriter mWriter = null;

    /** Old schemaorg_apache_xmlbeans.system property value for java.protocol.handler.pkgs */
    private String mOldJavaProtocolHandlerPkgs = null;

    /**
     * Set up for each test. 
     * 
     * @exception Exception when set up fails for any reason. This tells JUnit 
     *                      that it cannot run the test.
     */
    public void setUp() throws Exception
    {
        super.setUp();

        com.sun.jbi.wsdl2.WsdlFactory factory = WsdlFactory.newInstance();

        this.mWriter = factory.newWsdlWriter();

        this.mOldJavaProtocolHandlerPkgs = System.setProperty( 
            "java.protocol.handler.pkgs", 
            "com.sun.jbi.wsdl2.impl");
    }

    /**
     * Clean up after each test case.
     * 
     * @exception Exception when clean up fails for any reason.
     */
    public void tearDown() throws Exception
    {
        if (this.mOldJavaProtocolHandlerPkgs != null)
        {
            System.setProperty(
                "java.protocol.handler.pkgs",
                this.mOldJavaProtocolHandlerPkgs);
        }
    }

    /**
     * Test use of a WsldWriter instance.
     * 
     * @exception Exception when any unexpected error occurs.
     */
    public void testWriter() throws Exception
    {
        final String                  tns      = "uri:org.test:TestWsldWriter";
        final String                  binding1 = "binding1";
        final String                  op1      = "op1";
        final String                  if1      = "if1";
        com.sun.jbi.wsdl2.WsdlFactory factory  = WsdlFactory.newInstance();
        Description                   defs     = factory.newDescription(tns);
        Interface                     iface    = defs.addNewInterface(if1);
        StringWriter                  sw       = new StringWriter();
        InterfaceOperation            ifop     = iface.addNewOperation();

        ifop.setName(op1);
        defs.addNewBinding(binding1).addNewOperation().setInterfaceOperation(
            ifop.getQualifiedName());

        mWriter.writeDescription(defs, sw);
        sw.flush();
        sw.close();

        String result = sw.getBuffer().toString();

        assertTrue( result.length() > 0 );
        assertTrue( result.indexOf( "description" ) > 0 );
        assertTrue( result.indexOf( tns ) > 0 );
        assertTrue( result.indexOf( "\"" + binding1 + "\"" ) > 0 );
        assertTrue( result.indexOf( "\"" + op1      + "\"" ) > 0 );
        assertTrue( result.indexOf( Constants.WSDL_NAMESPACE_NAME ) > 0 );

        mWriter.writeWsdl((Definitions) defs, sw);
        sw.flush();
        sw.close();

        String result1 = sw.getBuffer().toString();

        assertTrue( result1.length() > 0 );
        assertTrue( result1.indexOf( "description" ) > 0 );
        assertTrue( result1.indexOf( tns ) > 0 );
        assertTrue( result1.indexOf( "\"" + binding1 + "\"" ) > 0 );
        assertTrue( result1.indexOf( "\"" + op1      + "\"" ) > 0 );
        assertTrue( result1.indexOf( Constants.WSDL_NAMESPACE_NAME ) > 0 );
    }
}
