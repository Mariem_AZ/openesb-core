/*
 * BEGIN_HEADER - DO NOT EDIT

 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.

 * You can obtain a copy of the license at
 * http://opensource.org/licenses/cddl1.php.
 * See the License for the specific language governing
 * permissions and limitations under the License.

 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * http://opensource.org/licenses/cddl1.php.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */
package org.openesb.runtime.tracking;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import org.openesb.runtime.tracking.tasks.MessageTrackingThreadFactory;

/**
 *
 * Copyright 2011 Alexander Lomov.
 */
// Not sure if we need this class
public class MessageTrackingExecutor {

    private static MessageTrackingExecutor me_;
    private final static ReentrantReadWriteLock rwl = new ReentrantReadWriteLock();
    private final static Lock rl = rwl.readLock();
    private final static Lock wl = rwl.writeLock();
    private static final String DEFAULT = "DEFAULT";
    private ConcurrentHashMap<String, Executor> executors;

    private MessageTrackingExecutor(){
        executors.put(DEFAULT, ExecutorFactory.getDefault());
    }

    public static MessageTrackingExecutor get() {
        try {
            rl.lock();
            if (me_ == null) {
                wl.lock();
                if (me_ == null) {
                    me_ = new MessageTrackingExecutor();
                }
            }
            return me_;
        } finally {
            wl.unlock();
            rl.unlock();
        }
    }

    public Executor getExecutor(String key) {
        return (key == null || key.equals(""))
                ? executors.get(DEFAULT)
                : executors.get(key);
    }

    public Executor getExecutor() {
        return getExecutor(DEFAULT);
    }

    private static class ExecutorFactory {

        private static final String defaultName = "DefaultMonitoringThread-";

        public static Executor getDefault(){
            final int availProcs = Runtime.getRuntime().availableProcessors();
            LinkedBlockingQueue<Runnable> q = new LinkedBlockingQueue<Runnable>();
            ThreadPoolExecutor e =
                    new ThreadPoolExecutor(4, availProcs < 4 ? 4 : availProcs, 
                    30, TimeUnit.SECONDS, q, new MessageTrackingThreadFactory(defaultName));
            return e;
        }
    }
    
}
