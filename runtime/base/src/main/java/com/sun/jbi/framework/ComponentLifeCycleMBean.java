/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ComponentLifeCycleMBean.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

import javax.management.ObjectName;

/**
 * This ComponentLifeCycleMBean extends the public interface to add a
 * getter for the DeployerMBean name.
 *
 * @author Sun Microsystems, Inc.
 */
public interface ComponentLifeCycleMBean
    extends com.sun.jbi.management.ComponentLifeCycleMBean
{
    /**
     * Get the JMX ObjectName for the DeployerMBean for this component. If
     * there is none, return null.
     * @throws javax.jbi.JBIException if there is a failure getting component
     * information from the Component Registry.
     * @return ObjectName the JMX object name of the DeployerMBean or null
     * if there is no DeployerMBean.
     */
    ObjectName getDeploymentMBeanName() throws javax.jbi.JBIException;
}
