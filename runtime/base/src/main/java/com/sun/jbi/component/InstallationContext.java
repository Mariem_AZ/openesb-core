/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)InstallationContext.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.component;

/**
 * This context contains information necessary for a JBI component to perform
 * its installation/uninstallation processing. This context is provided to the
 * <code>init()</code> method of the component's bootstrap implementation.
 *
 * @author Sun Microsystems, Inc.
 */
public interface InstallationContext
    extends javax.jbi.component.InstallationContext
{
    /**
     * Constant for specifying this context is for a Binding Component.
     */
    int BINDING = 1;

    /**
     * Constant for specifying this context is for a Service Engine.
     */
    int ENGINE = 2;
    
    /**
     * Constant for specifying self first classloading of a component's
     * bootstrap or runtime resources.
     */
    String SELF_FIRST = "self-first"; 

    /**
     * Get a list of elements that comprise the class path for this component.
     * Each element represents either a directory (containing class files) or a
     * jar/zip file. The elements are absolute paths and are returned with a
     * forward slash as their file separators.
     * @return a list of String objects containing class path elements.
     */
    java.util.List getAbsoluteClassPathElements();

    /**
     * Return the description of the component.
     * @return the description of the component from the installation
     * descriptor.
     */
    String getDescription();

    /**
     * Return the workspace root directory for the component.
     * @return the workspace root directory path.
     */
    String getWorkspaceRoot();

    /**
     * Return an indication as to whether the component being installed is a
     * Binding Component.
     * @return true if the component is a BC, false if not.
     */
    boolean isBinding();

    /**
     * Return an indication as to whether the bootstrap class loader should
     * use the normal hierarchy (parent-first) or an inverted hierarchy
     * (self-first).
     * @return true if the bootstrap class loader should use the self-first
     * hierarchy, false if it should use parent-first. 
     */
    boolean isBootstrapClassLoaderSelfFirst();

    /**
     * Return an indication as to whether the component class loader should
     * use the normal hierarchy (parent-first) or an inverted hierarchy
     * (self-first).
     * @return true if the component class loader should use the self-first
     * hierarchy, false if it should use parent-first. 
     */
    boolean isComponentClassLoaderSelfFirst();

    /**
     * Return an indication as to whether the component being installed is a
     * Service Engine.
     * @return true if the component is a SE, false if not.
     */
    boolean isEngine();

    /**
     * Set the reference to the ComponentContext for this component.
     * @param context is the component context.
     */
    void setContext(javax.jbi.component.ComponentContext context);
}
