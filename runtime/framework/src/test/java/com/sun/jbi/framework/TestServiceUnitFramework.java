/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestServiceUnitFramework.java
 * Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

import com.sun.jbi.ServiceUnitState;
import com.sun.jbi.management.ComponentInstallationContext;

import java.util.ArrayList;

/**
 * Tests for the ServiceUnitFramework.
 *
 * @author Sun Microsystems, Inc.
 */
public class TestServiceUnitFramework
    extends junit.framework.TestCase
{
    /**
     * Current test name.
     */
    private String mTestName;

    /**
     * Current SRCROOT path.
     */
    private String mSrcroot;

    /**
     * Helper class to setup environment for testing.
     */
    private EnvironmentSetup mSetup;

    /**
     * Instance of the EnvironmentContext class.
     */
    private EnvironmentContext mContext;

    /**
     * Local instance of Component.
     */
    private Component mComponent;

    /**
     * Local instance of the ComponentInstallationContext class.
     */
    private ComponentInstallationContext mInstallContext;

    /**
     * Instance of the ComponentFramework class.
     */
    private ComponentFramework mCompFW;

    /**
     * Local instance of the ComponentRegistry class.
     */
    private ComponentRegistry mCompReg;

    /**
     * Local instance of the ServiceUnitFramework class.
     */
    private ServiceUnitFramework mSuFW;

    /**
     * The constructor for this testcase, forwards the test name to
     * the jUnit TestCase base class.
     * @param aTestName String with the name of this test.
     */
    public TestServiceUnitFramework(String aTestName)
    {
        super(aTestName);
        mTestName = aTestName;
    }

    /**
     * Setup for the test. This creates the ComponentRegistry instance
     * and other objects needed for the tests.
     * @throws Exception when set up fails for any reason.
     */
    public void setUp()
        throws Exception
    {
        super.setUp();
        System.err.println("***** START of test " + mTestName);
        mSrcroot = "";

        // Create and initialize the EnvironmentContext. Create, initialize,
        // and start up the Component Registry and Framework.

        mSetup = new EnvironmentSetup();
        mContext = mSetup.getEnvironmentContext();
        System.err.println("Setting log level to " +
                           java.util.logging.Level.FINEST.toString() +
                           " for logger " + mContext.getLogger().getName());
        mContext.getLogger().setLevel(java.util.logging.Level.FINEST);
        System.err.println("Log level is now " +
                           mContext.getLogger().getLevel().toString() +
                           " for logger " + mContext.getLogger().getName());
        mSetup.startup(true, true);
        mCompReg = mContext.getComponentRegistry();
        mCompFW = mContext.getComponentFramework();

        // Create component class path lists

        ArrayList bootstrapClassPath = new ArrayList();
        bootstrapClassPath.add(mSrcroot + Constants.BC_BOOTSTRAP_CLASS_PATH);
        ArrayList componentClassPath = new ArrayList();
        componentClassPath.add(Constants.BC_LIFECYCLE_CLASS_PATH);

        // Create installation context

        mInstallContext =
            new ComponentInstallationContext(
                Constants.BC_NAME,
                ComponentInstallationContext.BINDING,
                Constants.BC_LIFECYCLE_CLASS_NAME,
                componentClassPath,
                null);
        mInstallContext.setInstallRoot(mSrcroot);
        mInstallContext.setIsInstall(true);

        // Install the test component

        mCompFW.loadBootstrap(
            mInstallContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            bootstrapClassPath,
            null, false);
        mCompFW.installComponent(mInstallContext);

        // Get the Component instance and start the component

        mComponent = mCompReg.getComponent(Constants.BC_NAME);
        mCompFW.startComponent(mComponent);

        // Create ServiceUnitFramework instance

        mSuFW = new ServiceUnitFramework(mContext);
    }

    /**
     * Cleanup for the test.
     * @throws Exception when tearDown fails for any reason.
     */
    public void tearDown()
        throws Exception
    {
        super.tearDown();
        mCompReg.unregisterComponent(Constants.BC_NAME);
        mSetup.shutdown(true, true);
        System.err.println("***** END of test " + mTestName);
    }

// =============================  test methods ================================

    /**
     * Tests initializeServiceUnits with a good result.
     * @throws Exception if an unexpected error occurs.
     */
    public void testInitializeServiceUnitsGood()
        throws Exception
    {
        // Register an SU
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME,
            Constants.SU_NAME, Constants.SU_ROOT);
        ServiceUnit su = mComponent.getServiceUnit(Constants.SU_NAME);

        // Initialize the service unit
        su.setDesiredState(ServiceUnitState.STOPPED);
        mSuFW.initializeServiceUnits(mComponent);
        assertTrue("Failure initializing Service Unit, expected state " +
            su.getStateAsString(ServiceUnitState.STOPPED) + " but state is " +
            su.getStateAsString(),
            su.isStopped());
    }

    /**
     * Tests initializeServiceUnits with an exception thrown by the component's
     * Service Unit manager.
     * @throws Exception if an unexpected error occurs.
     */
    public void testInitializeServiceUnitsException()
        throws Exception
    {
        // Register the SU
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME,
            Constants.SU_NAME_INIT_EXCEPTION, Constants.SU_ROOT);
        ServiceUnit su =
            mComponent.getServiceUnit(Constants.SU_NAME_INIT_EXCEPTION);

        // Initialize the service unit
        su.setDesiredState(ServiceUnitState.STOPPED);
        mSuFW.initializeServiceUnits(mComponent);
        assertTrue("Service Unit initialized but should have thrown exception",
            su.isShutdown());
    }

    /**
     * Tests initializeServiceUnits with a timeout in the component's
     * Service Unit manager.
     * @throws Exception if an unexpected error occurs.
     */
    public void testInitializeServiceUnitsTimeout()
        throws Exception
    {
        // Register the SU
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME,
            Constants.SU_NAME_INIT_TIMEOUT, Constants.SU_ROOT);
        ServiceUnit su =
            mComponent.getServiceUnit(Constants.SU_NAME_INIT_TIMEOUT);

        // Initialize the service unit
        su.setDesiredState(ServiceUnitState.STOPPED);
        mSuFW.initializeServiceUnits(mComponent);
        assertTrue("Service Unit initialized but should have timed out",
            su.isShutdown());
    }

    /**
     * Tests initializeServiceUnits with consecutive timeouts in the component's
     * Service Unit manager.
     * @throws Exception if an unexpected error occurs.
     */
    public void testInitializeServiceUnitsTimeoutLimit()
        throws Exception
    {
        // This deploys 3 SUs that will timeout on their init calls plus one
        // SU that will not. The object is to make sure that after 3 consecutive
        // timeouts the remaining SUs are not initialized.

        String suName;

        // Register the first SU
        suName = Constants.SU_NAME_INIT_TIMEOUT + "_1";
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME, suName, Constants.SU_ROOT);
        ServiceUnit su1 =
            mComponent.getServiceUnit(suName);

        // Register the second SU
        suName = Constants.SU_NAME_INIT_TIMEOUT + "_2";
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME, suName, Constants.SU_ROOT);
        ServiceUnit su2 =
            mComponent.getServiceUnit(suName);

        // Register the third SU
        suName = Constants.SU_NAME_INIT_TIMEOUT + "_3";
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME, suName, Constants.SU_ROOT);
        ServiceUnit su3 =
            mComponent.getServiceUnit(suName);

        // Register the fourth SU
        suName = Constants.SU_NAME_FIRST;
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME, suName, Constants.SU_ROOT);
        ServiceUnit su4 =
            mComponent.getServiceUnit(suName);

        // Initialize the service units. The expected result is that the first
        // three will timeout, preventing the fourth from being initialized.
        su1.setDesiredState(ServiceUnitState.STOPPED);
        su2.setDesiredState(ServiceUnitState.STOPPED);
        su3.setDesiredState(ServiceUnitState.STOPPED);
        su4.setDesiredState(ServiceUnitState.STOPPED);
        mSuFW.initializeServiceUnits(mComponent);
        assertTrue("Service Unit " + su1.getName() + 
            " initialized but should have timed out",
            su1.isShutdown());
        assertTrue("Service Unit " + su2.getName() + 
            " initialized but should have timed out",
            su2.isShutdown());
        assertTrue("Service Unit " + su3.getName() + 
            " initialized but should have timed out",
            su3.isShutdown());
        assertTrue("Service Unit " + su4.getName() + 
            " initialized but should have been skipped",
            su4.isShutdown());
    }

    /**
     * Tests startServiceUnits with a good result.
     * @throws Exception if an unexpected error occurs.
     */
    public void testStartServiceUnitsGood()
        throws Exception
    {
        // Register the SU
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME,
            Constants.SU_NAME, Constants.SU_ROOT);
        ServiceUnit su = mComponent.getServiceUnit(Constants.SU_NAME);

        // Set the service unit to initialized, and start it
        su.setStopped();
        su.setDesiredState(ServiceUnitState.STARTED);
        mSuFW.startServiceUnits(mComponent);
        assertTrue("Failure starting Service Unit, expected state " +
            su.getStateAsString(ServiceUnitState.STARTED) + " but state is " +
            su.getStateAsString(),
            su.isStarted());
    }

    /**
     * Tests startServiceUnits with an exception thrown by the component's
     * Service Unit manager.
     * @throws Exception if an unexpected error occurs.
     */
    public void testStartServiceUnitsException()
        throws Exception
    {
        // Register the SU
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME,
            Constants.SU_NAME_START_EXCEPTION, Constants.SU_ROOT);
        ServiceUnit su =
            mComponent.getServiceUnit(Constants.SU_NAME_START_EXCEPTION);

        // Set the service unit to initialized, and start it. The start should
        // fail.
        su.setStopped();
        su.setDesiredState(ServiceUnitState.STARTED);
        mSuFW.startServiceUnits(mComponent);
        assertFalse("Service Unit started but should have thrown exception",
            su.isStarted());
    }

    /**
     * Tests startServiceUnits with a timeout in the component's
     * Service Unit manager.
     * @throws Exception if an unexpected error occurs.
     */
    public void testStartServiceUnitsTimeout()
        throws Exception
    {
        // Register the SU
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME,
            Constants.SU_NAME_START_TIMEOUT, Constants.SU_ROOT);
        ServiceUnit su =
            mComponent.getServiceUnit(Constants.SU_NAME_START_TIMEOUT);

        // Set the service unit to initialized, and start it. The start should
        // fail.
        su.setStopped();
        su.setDesiredState(ServiceUnitState.STARTED);
        mSuFW.startServiceUnits(mComponent);
        assertFalse("Service Unit started but should have timed out",
            su.isStarted());
    }

    /**
     * Tests startServiceUnits with consecutive timeouts in the component's
     * Service Unit manager.
     * @throws Exception if an unexpected error occurs.
     */
    public void testStartServiceUnitsTimeoutLimit()
        throws Exception
    {
        // This deploys 3 SUs that will timeout on their start calls plus one
        // SU that will not. The object is to make sure that after 3 consecutive
        // timeouts the remaining SUs are not started.

        String suName;

        // Register the first SU
        suName = Constants.SU_NAME_START_TIMEOUT + "_1";
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME, suName, Constants.SU_ROOT);
        ServiceUnit su1 =
            mComponent.getServiceUnit(suName);

        // Register the second SU
        suName = Constants.SU_NAME_START_TIMEOUT + "_2";
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME, suName, Constants.SU_ROOT);
        ServiceUnit su2 =
            mComponent.getServiceUnit(suName);

        // Register the third SU
        suName = Constants.SU_NAME_START_TIMEOUT + "_3";
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME, suName, Constants.SU_ROOT);
        ServiceUnit su3 =
            mComponent.getServiceUnit(suName);

        // Register the fourth SU
        suName = Constants.SU_NAME_FIRST;
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME, suName, Constants.SU_ROOT);
        ServiceUnit su4 =
            mComponent.getServiceUnit(suName);

        // Set the service units to initialized.
        su1.setStopped();
        su1.setDesiredState(ServiceUnitState.STARTED);
        su2.setStopped();
        su2.setDesiredState(ServiceUnitState.STARTED);
        su3.setStopped();
        su3.setDesiredState(ServiceUnitState.STARTED);
        su4.setStopped();
        su4.setDesiredState(ServiceUnitState.STARTED);

        // Start the service units. The expected result is that the first
        // three will timeout, preventing the fourth from being started.
        mSuFW.startServiceUnits(mComponent);
        assertFalse("Service Unit " + su1.getName() + 
            " started but should have timed out",
            su1.isStarted());
        assertFalse("Service Unit " + su2.getName() + 
            " started but should have timed out",
            su2.isStarted());
        assertFalse("Service Unit " + su3.getName() + 
            " started but should have timed out",
            su3.isStarted());
        assertFalse("Service Unit " + su4.getName() + 
            " started but should have been skipped",
            su4.isStarted());
    }

    /**
     * Tests stopServiceUnits with a good result.
     * @throws Exception if an unexpected error occurs.
     */
    public void testStopServiceUnitsGood()
        throws Exception
    {
        // Register the SU
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME,
            Constants.SU_NAME, Constants.SU_ROOT);
        ServiceUnit su = mComponent.getServiceUnit(Constants.SU_NAME);

        // Set the service unit to started, and stop it.
        su.setStopped();
        su.setStarted();
        su.setDesiredState(ServiceUnitState.STOPPED);
        mSuFW.stopServiceUnits(mComponent);
        assertTrue("Failure stopping Service Unit, expected state " +
            su.getStateAsString(ServiceUnitState.STOPPED) + " but state is " +
            su.getStateAsString(),
            su.isStopped());
    }

    /**
     * Tests stopServiceUnits with an exception thrown by the component's
     * Service Unit manager.
     * @throws Exception if an unexpected error occurs.
     */
    public void testStopServiceUnitsException()
        throws Exception
    {
        // Register the SU
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME,
            Constants.SU_NAME_STOP_EXCEPTION, Constants.SU_ROOT);
        ServiceUnit su =
            mComponent.getServiceUnit(Constants.SU_NAME_STOP_EXCEPTION);

        // Set the service unit to started, and stop it. The stop should fail.
        su.setStopped();
        su.setStarted();
        su.setDesiredState(ServiceUnitState.STOPPED);
        mSuFW.stopServiceUnits(mComponent);
        assertFalse("Service Unit stopped but should have thrown exception",
            su.isStopped());
    }

    /**
     * Tests stopServiceUnits with a timeout in the component's
     * Service Unit manager.
     * @throws Exception if an unexpected error occurs.
     */
    public void testStopServiceUnitsTimeout()
        throws Exception
    {
        // Register the SU
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME,
            Constants.SU_NAME_STOP_TIMEOUT, Constants.SU_ROOT);
        ServiceUnit su =
            mComponent.getServiceUnit(Constants.SU_NAME_STOP_TIMEOUT);

        // Set the service unit to started, and stop it. The stop should time
        // out.
        su.setStopped();
        su.setStarted();
        su.setDesiredState(ServiceUnitState.STOPPED);
        mSuFW.stopServiceUnits(mComponent);
        assertFalse("Service Unit stopped but should have timed out",
            su.isStopped());
    }

    /**
     * Tests stopServiceUnits with consecutive timeouts in the component's
     * Service Unit manager.
     * @throws Exception if an unexpected error occurs.
     */
    public void testStopServiceUnitsTimeoutLimit()
        throws Exception
    {
        // This deploys 3 SUs that will timeout on their stop calls plus one
        // SU that will not. The object is to make sure that after 3 consecutive
        // timeouts the remaining SUs are not stopped.

        String suName;

        // Register the first SU
        suName = Constants.SU_NAME_STOP_TIMEOUT + "_1";
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME, suName, Constants.SU_ROOT);
        ServiceUnit su1 =
            mComponent.getServiceUnit(suName);

        // Register the second SU
        suName = Constants.SU_NAME_STOP_TIMEOUT + "_2";
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME, suName, Constants.SU_ROOT);
        ServiceUnit su2 =
            mComponent.getServiceUnit(suName);

        // Register the third SU
        suName = Constants.SU_NAME_STOP_TIMEOUT + "_3";
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME, suName, Constants.SU_ROOT);
        ServiceUnit su3 =
            mComponent.getServiceUnit(suName);

        // Register the fourth SU
        suName = Constants.SU_NAME_FIRST;
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME, suName, Constants.SU_ROOT);
        ServiceUnit su4 =
            mComponent.getServiceUnit(suName);

        // Set the service units to started.
        su1.setStopped();
        su1.setStarted();
        su1.setDesiredState(ServiceUnitState.STOPPED);
        su2.setStopped();
        su2.setStarted();
        su2.setDesiredState(ServiceUnitState.STOPPED);
        su3.setStopped();
        su3.setStarted();
        su3.setDesiredState(ServiceUnitState.STOPPED);
        su4.setStopped();
        su4.setStarted();
        su4.setDesiredState(ServiceUnitState.STOPPED);

        // Stop the service units. The expected result is that the first
        // three will timeout, preventing the fourth from being stopped.
        mSuFW.startServiceUnits(mComponent);
        assertFalse("Service Unit " + su1.getName() + 
            " stopped but should have timed out",
            su1.isStopped());
        assertFalse("Service Unit " + su2.getName() + 
            " stopped but should have timed out",
            su2.isStopped());
        assertFalse("Service Unit " + su3.getName() + 
            " stopped but should have timed out",
            su3.isStopped());
        assertFalse("Service Unit " + su4.getName() + 
            " stopped but should have been skipped",
            su4.isStopped());
    }

    /**
     * Tests shutDownServiceUnits with a good result.
     * @throws Exception if an unexpected error occurs.
     */
    public void testShutDownServiceUnitsGood()
        throws Exception
    {
        // Register the SU
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME,
            Constants.SU_NAME, Constants.SU_ROOT);
        ServiceUnit su = mComponent.getServiceUnit(Constants.SU_NAME);

        // Set the service unit to initialized and then shut it down.
        su.setStopped();
        su.setDesiredState(ServiceUnitState.SHUTDOWN);
        mSuFW.shutDownServiceUnits(mComponent);
        assertTrue("Failure shutting down Service Unit, expected state " +
            su.getStateAsString(ServiceUnitState.SHUTDOWN) + " but state is " +
            su.getStateAsString(),
            su.isShutdown());
    }

    /**
     * Tests shutDownServiceUnits with an exception thrown by the component's
     * Service Unit manager.
     * @throws Exception if an unexpected error occurs.
     */
    public void testShutDownServiceUnitsException()
        throws Exception
    {
        // Register the SU
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME,
            Constants.SU_NAME_SHUTDOWN_EXCEPTION, Constants.SU_ROOT);
        ServiceUnit su =
            mComponent.getServiceUnit(Constants.SU_NAME_SHUTDOWN_EXCEPTION);

        // Set the service unit to initialized and then shut it down. The
        // shutdown should fail.
        su.setStopped();
        su.setDesiredState(ServiceUnitState.SHUTDOWN);
        mSuFW.shutDownServiceUnits(mComponent);
        assertFalse("Service Unit shut down but should have thrown exception",
            su.isShutdown());
    }

    /**
     * Tests shutDownServiceUnits with a timeout in the component's
     * Service Unit manager.
     * @throws Exception if an unexpected error occurs.
     */
    public void testShutDownServiceUnitsTimeout()
        throws Exception
    {
        // Register the SU
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME,
            Constants.SU_NAME_SHUTDOWN_TIMEOUT, Constants.SU_ROOT);
        ServiceUnit su =
            mComponent.getServiceUnit(Constants.SU_NAME_SHUTDOWN_TIMEOUT);

        // Set the service unit to initialized and then shut it down. The
        // shutdown should time out. 
        su.setStopped();
        su.setDesiredState(ServiceUnitState.SHUTDOWN);
        mSuFW.shutDownServiceUnits(mComponent);
        assertFalse("Service Unit shut down but should have timed out",
            su.isShutdown());
    }

    /**
     * Tests shutDownServiceUnits with consecutive timeouts in the component's
     * Service Unit manager.
     * @throws Exception if an unexpected error occurs.
     */
    public void testShutDownServiceUnitsTimeoutLimit()
        throws Exception
    {
        // This deploys 3 SUs that will timeout on their shutDown calls plus one
        // SU that will not. The object is to make sure that after 3 consecutive
        // timeouts the remaining SUs are not shut down.

        String suName;

        // Register the first SU
        suName = Constants.SU_NAME_SHUTDOWN_TIMEOUT + "_1";
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME, suName, Constants.SU_ROOT);
        ServiceUnit su1 =
            mComponent.getServiceUnit(suName);

        // Register the second SU
        suName = Constants.SU_NAME_SHUTDOWN_TIMEOUT + "_2";
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME, suName, Constants.SU_ROOT);
        ServiceUnit su2 =
            mComponent.getServiceUnit(suName);

        // Register the third SU
        suName = Constants.SU_NAME_SHUTDOWN_TIMEOUT + "_3";
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME, suName, Constants.SU_ROOT);
        ServiceUnit su3 =
            mComponent.getServiceUnit(suName);

        // Register the fourth SU
        suName = Constants.SU_NAME_FIRST;
        mCompReg.registerServiceUnit(
            Constants.BC_NAME, Constants.SA_NAME, suName, Constants.SU_ROOT);
        ServiceUnit su4 =
            mComponent.getServiceUnit(suName);

        // Set the service units to initialized.
        su1.setStopped();
        su1.setDesiredState(ServiceUnitState.SHUTDOWN);
        su2.setStopped();
        su2.setDesiredState(ServiceUnitState.SHUTDOWN);
        su3.setStopped();
        su3.setDesiredState(ServiceUnitState.SHUTDOWN);
        su4.setStopped();
        su4.setDesiredState(ServiceUnitState.SHUTDOWN);

        // Shut down the service units. The expected result is that the first
        // three will timeout, preventing the fourth from being shut down.
        mSuFW.shutDownServiceUnits(mComponent);
        assertFalse("Service Unit " + su1.getName() + 
            " shut down but should have timed out",
            su1.isShutdown());
        assertFalse("Service Unit " + su2.getName() + 
            " shut down but should have timed out",
            su2.isShutdown());
        assertFalse("Service Unit " + su3.getName() + 
            " shut down but should have timed out",
            su3.isShutdown());
        assertFalse("Service Unit " + su4.getName() + 
            " shut down but should have been skipped",
            su4.isShutdown());
    }

}
