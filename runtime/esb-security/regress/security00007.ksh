#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)security00007.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
. ./regress_defs.ksh

DIST_DIR=${SEC_BLD_REGRESS_DIR}/dist/

##########################################################################################################
# This test packages and installs a test Binding Component. The Binding component uses the Security      #
# Service to secure and validate messages.                                                               #
##########################################################################################################

# Create the Binding1 distribution dir
if [ $OPENESB_BUILD -eq 1 ]; then
    ant -emacs -q -Djce_provider_dir="$JV_SRCROOT/vendor-libs/bouncycastle/jars" -f ${SEC_REGRESS_DIR}/scripts/build-test-components.ant pkg_binding1
else
    ant -emacs -q -f ${SEC_REGRESS_DIR}/scripts/build-test-components.ant pkg_binding1
fi

# Install the Binding
asant -emacs -q -f $JBI_ADMIN_XML -Djbi.username=$AS_ADMIN_USER -Djbi.password=$AS_ADMIN_PASSWD -Djbi.port=$JBI_ADMIN_PORT  -Djbi.install.file=$DIST_DIR/security-test-binding1.jar install-component

# Start the binding
asant -emacs -q -f $JBI_ADMIN_XML -Djbi.username=$AS_ADMIN_USER -Djbi.password=$AS_ADMIN_PASSWD -Djbi.port=$JBI_ADMIN_PORT  -Djbi.component.name="Secure_test_binding1" start-component

# Stop the  binding
asant -emacs -q -f $JBI_ADMIN_XML -Djbi.username=$AS_ADMIN_USER -Djbi.password=$AS_ADMIN_PASSWD -Djbi.port=$JBI_ADMIN_PORT -Djbi.component.name="Secure_test_binding1" stop-component

# Shutdown the Binding
asant -emacs -q -f $JBI_ADMIN_XML -Djbi.username=$AS_ADMIN_USER -Djbi.password=$AS_ADMIN_PASSWD -Djbi.port=$JBI_ADMIN_PORT -Djbi.component.name="Secure_test_binding1" shut-down-component

# Uninstall the Binding
asant -emacs -q -f $JBI_ADMIN_XML -Djbi.username=$AS_ADMIN_USER -Djbi.password=$AS_ADMIN_PASSWD -Djbi.port=$JBI_ADMIN_PORT -Djbi.component.name="Secure_test_binding1" uninstall-component

# Remove the Dist Dir
#rm -rf ${DIST_DIR}
