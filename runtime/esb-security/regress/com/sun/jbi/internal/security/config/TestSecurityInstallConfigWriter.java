/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestSecurityInstallConfigWriter.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  TestSecurityInstallConfig.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on December 17, 2004, 12:53 AM
 */

package com.sun.jbi.internal.security.config;

import com.sun.jbi.internal.security.Util;

/**
 *
 * @author Sun Microsystems, Inc.
 */
public class TestSecurityInstallConfigWriter extends junit.framework.TestCase
{
    
    /** Creates a new instance of TestSecurityInstallConfigWriter */
    public TestSecurityInstallConfigWriter (String testname)
    {
        super(testname);
    }
    
    
    public void testSecurityInstallonfig()
        throws Exception
    {
        SecurityInstallConfig siCfg = new SecurityInstallConfig(
            Util.getStringTranslator("com.sun.jbi.internal.security"));
        
        siCfg.addUserDomain("ud1", "com.sun.jbi.UserDomainImpl");
        siCfg.addParameterToUserDomain("ud1", "parameter_1", "value_1");
        siCfg.addParameterToUserDomain("ud1", "parameter_2", "value_2");
        
        siCfg.addKeyStoreManager("ks1", "com.sun.jbi.KeyStoreManager1");
        siCfg.addParameterToKeyStoreManager("ks1", "parameter_1", "value_1");
        siCfg.addParameterToKeyStoreManager("ks1", "parameter_3", "value_3");
        siCfg.addParameterToKeyStoreManager("ks1", "parameter_3", "value_3");
        
        siCfg.setSSLClientAlias("nikita");
        siCfg.setSSLClientProtocol("TLSv1");
        siCfg.setSSLClientUseDefault(false);
        siCfg.setSSLServerRequireClientAuth(true);
        
        siCfg.setDefaultUserDomainName("ud1");
        siCfg.setDefaultKeyStoreManagerName("ks1");

        SecurityInstallConfigWriter.write(siCfg, System.out);
        
    }  
    
}
