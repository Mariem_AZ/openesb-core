/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SecurityHelper.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  SecurityHelper.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on September 1, 2004, 12:43 PM
 */

package com.sun.jbi.internal.security.test.binding1.rt;

import java.security.Security;
import java.security.Provider;

/**
 *
 * @author Sun Microsystems, Inc.
 */
public class SecurityHelper
{
    /**
     * Provider Class Name 
     */
    public static final String 
        BOUNCY_CASTLE_PROVIDER = "org.bouncycastle.jce.provider.BouncyCastleProvider";
    
    /** Provider */
    Provider mBouncyCastleProvider = null;
    
    
    /** Creates a new instance of SecurityHelper */
    public SecurityHelper()
        throws Exception
    {
        
        // -- Load the Provider
        mBouncyCastleProvider = 
            (Provider) (Class.forName(BOUNCY_CASTLE_PROVIDER)).newInstance();
        
        mBouncyCastleProvider.put(
            "Alg.Alias.Cipher.RSA/ECB/OAEPWithSHA1AndMGF1Padding","RSA/OAEP");

    }
    
    /**
     * Load the Bouncy Castle Provider as the second provider in the list of 
     * providers managed by java.security.Security
     */
    public void loadProvider()
        throws Exception
    {
        
        if ( Security.getProviders().length >= 1)
        {
            Security.insertProviderAt(mBouncyCastleProvider, 2);
        }
        else
        {
            Security.addProvider(mBouncyCastleProvider);
        }
    }
    
    /**
     * Unload the Bouncy Castle Provider
     */
    public void unloadProvider()
        throws Exception
    {
        Security.removeProvider(mBouncyCastleProvider.getName());
    }
    
    /**
     * Print all the Providers 
     */
    public void printProviders()
    {
        Provider[] providers = Security.getProviders();
        StringBuffer strBuffer = new StringBuffer();
        java.util.logging.Logger logger = java.util.logging.Logger.getLogger(
                "com.sun.jbi.stockquote.client.service");
        for ( int i=0; i < providers.length ; i++ )
        {       
            strBuffer.append("Provider [" + i + "] = " + providers[i].toString() + "\n");    
        }      
        logger.info(strBuffer.toString());
    }
    
    
    
}
