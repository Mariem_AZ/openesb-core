/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestLoader.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.internal.security.util;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class TestLoader
    extends junit.framework.TestCase
{

    /**
     */
    private String config = null;
    private File tmpFile = null;
    private File newFile = null;
    private String newFileName = null;
    private String configSchema = null;
    private String xwssSchema = null;
    private String instConfig = null;
    private String instConfigSchema  = null;
    
    /**
     */
    public TestLoader(String aTestName)
    {
        super(aTestName);
        String srcroot = System.getProperty("junit.srcroot");
        String security = "/runtime/esb-security";        // open-esb build

        java.io.File f = new java.io.File(srcroot + security);
        if (! f.exists())
        {
            security = "/shasta/security";       // mainline/whitney build
        }

        config = srcroot + security + "/regress/deployconfig/EndptDeployConfig.xml";
        configSchema = srcroot + security + "/schema/secdeployconfig.xsd";
        xwssSchema = srcroot + security + "/schema/xwssconfig.xsd";
        instConfig = srcroot + security + "/regress/installconfig/InstallConfig1.xml";
        instConfigSchema = srcroot + security + "/schema/secinstallconfig.xsd";
    }
    
    /**
     * Test the load without any validation
     * @throws Exception if an unexpected error occurs
     */
    public void testLoad()
           throws Exception
    {
        String testname = "testLoad";
        try 
        {
          // -- Test the Load method
          Loader ldr = new Loader(); 
          org.w3c.dom.Document doc = ldr.load(config, false, null, null);
          assertNotNull("The Document was not loaded correctly.", doc); 
        }
        catch (Exception aEx)
        {
            aEx.printStackTrace();
            fail(testname + ": failed due to -" + aEx.getMessage());
            throw aEx;
        }
    }   
    
    /**
     * Test the load with validation.
     *
     * @throws Exception if an unexpected error occurs
     */
    public void testLoadWithValidation()
           throws Exception
    {
        String testname = "testLoadWithValidation";
        try 
        {

            Loader ldr = new Loader(); 
            org.w3c.dom.Document doc = ldr.load(instConfig, true, 
                new com.sun.jbi.internal.security.config.ConfigErrorHandler(
                    ( new com.sun.jbi.internal.security.
                        SampleBindingContext("sb-111")).getStringTranslator(    
                            "com.sun.jbi.internal.security")), 
                                new String[]{instConfigSchema});
            assertNotNull("The Document was not loaded correctly.", doc); 

        }
        catch (Exception aEx)
        {
            aEx.printStackTrace();
            fail(testname + ": failed due to -" + aEx.getMessage());
            throw aEx;
        }
    }
    
    /**
     * Test the load with validation.
     *
     * @throws Exception if an unexpected error occurs
     */
    public void testLoadWithValidation2()
           throws Exception
    {
        String testname = "testLoadWithValidation2";
        try 
        {
            Loader ldr = new Loader(); 
            org.w3c.dom.Document doc = ldr.load(config, true, 
                new com.sun.jbi.internal.security.config.ConfigErrorHandler(
                    ( new com.sun.jbi.internal.security.
                        SampleBindingContext("sb-111")).getStringTranslator(    
                            "com.sun.jbi.internal.security")), 
                                new String[]{configSchema});
            assertNotNull("The Document was not loaded correctly.", doc); 

        }
        catch (Exception aEx)
        {
            aEx.printStackTrace();
            fail(testname + ": failed due to -" + aEx.getMessage());
            throw aEx;
        }
    }
     
     
}
