/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DeploymentListenerImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  DeploymentListener.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 */

package com.sun.jbi.internal.security;

import com.sun.jbi.binding.security.Endpoint;

import com.sun.jbi.internal.security.config.EndpointSecurityConfig;
import com.sun.jbi.internal.security.config.EndptSecConfigReader;

import java.util.Vector;

/**
 * No-op implementation of Deployment Listener.
 *
 * @author Sun Microsystems, Inc.
 */
public class DeploymentListenerImpl 
    implements com.sun.jbi.binding.security.DeploymentListener, DeploymentNotifier
{
    /** Observers. */
    private Vector mObservers = null;
    
    /** Schema Dir. */
    private String mSchemaDir;
    
    /** The Logger. */
    private java.util.logging.Logger mLogger;
    
    /**
     * @param schemaDir directory for the deployment schema.
     */
    public DeploymentListenerImpl (String schemaDir)
    {
        mLogger = java.util.logging.Logger.getLogger(Constants.PACKAGE);
        mObservers = new Vector();
        mSchemaDir = schemaDir;
    }    
    
    
    /**
     * Notification for a new Endpoint Deployment.
     *
     * @param endpoint is the Endpoint being deployed.
     * @throws Exception on errors
     */
    public void addDeployment (Endpoint endpoint) 
        throws Exception
    {      
        try
        {

            // -- Create the EndpointSecurityConfig
            EndptSecConfigReader reader = new EndptSecConfigReader(
                SecurityService.getStringTranslator(Constants.PACKAGE), mSchemaDir);
            reader.setValidating(true);

            EndpointSecurityConfig epSecConfig = 
                new com.sun.jbi.internal.security.config.EndptSecConfig();
            if ( endpoint.getSecurityConfigFileName() != null )
            {
                epSecConfig  =
                    reader.read(new java.io.File(endpoint.getSecurityConfigFileName()));
            }

            // -- notify Observers of event
            java.util.Iterator itr = mObservers.iterator ();
            while (itr.hasNext())
            {
                DeploymentObserver obs = (DeploymentObserver) itr.next();
                obs.registerEndpointDeployment(endpoint, epSecConfig);
            }
        }
        catch (Exception ex)
        {
            // -- Print a error Message Here
            mLogger.severe("Error occured in deploying the endpoint to the Security" +
                " Service, details " + ex.getMessage());
            ex.printStackTrace();
            throw ex;
        }
        
    }
    
    /**
     * Notification for removal of an Endpoint Deployment.
     *
     * @param endpoint is the Endpoint being undeployed.
     */
    public void removeDeployment(Endpoint endpoint)
    {
        // -- notify Observers of event
        java.util.Iterator itr = mObservers.iterator ();
        
        while (itr.hasNext())
        {
            DeploymentObserver obs = (DeploymentObserver) itr.next();
            obs.unregisterEndpointDeployment(endpoint);
        }
    }
 
    /**
     * @param obs is an observer of deployments.
     */
    public void addDeploymentObserver (DeploymentObserver obs)
    {
        mObservers.add(obs);
    }
    
    /**
     * @param obs is an observer of deploy events.
     */
    public void removeDeploymentObserver (DeploymentObserver obs)
    {
        mObservers.remove(obs);
    }
    
}
