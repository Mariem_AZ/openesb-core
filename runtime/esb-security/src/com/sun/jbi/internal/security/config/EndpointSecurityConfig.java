/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EndpointSecurityConfig.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  EndpointSecurityConfiguration.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on March 18, 2005, 11:56 AM
 */

package com.sun.jbi.internal.security.config;

/**
 * This interface defines the Endpoint's Security Configuration. 
 * @author Sun Microsystems, Inc.
 */
public interface EndpointSecurityConfig
{
    /**
     * Get the Security Environment for the Endpoint.
     *
     * @return the SecurityContext for the Endpoint
     */
    SecurityContext getSecurityContext();
    
    /**
     * Get the Message Protection Policy for a particular operation
     * provided by the Endpoint.
     * 
     * @param opName is the Operation Name.
     * @return the Message Protection Policy for the Endpoint Operation.
     */
    MessageSecPolicy getMessagePolicy(String opName);
    
    /**
     * Get the Default Message Protection Policy.
     * 
     * @return the default Message Protection Policy.
     */
    MessageSecPolicy getDefaultMessagePolicy();    
    
    /**
     * Get the EndpointInfo to which this config applies.
     * 
     * @return the Endpoint Information
     */
    EndpointInfo getEndpointInfo();
   
}
