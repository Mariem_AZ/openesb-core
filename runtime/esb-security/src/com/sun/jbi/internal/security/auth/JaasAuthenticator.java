/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JaasAuthenticator.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  Authenticator.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on January 25, 2005, 10:29 PM
 */

package com.sun.jbi.internal.security.auth;

import com.sun.jbi.internal.security.UserDomain;
import com.sun.jbi.internal.security.callback.UsrPwdCallbackHandler;

// -- This will be replaced with the JSR 196 Callback later.
import com.sun.enterprise.security.jauth.callback.PasswordValidationCallback;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;
import javax.security.auth.login.Configuration;
import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;
import javax.security.auth.Subject;

/**
 * This class enforces user authentication.
 *
 * @author Sun Microsystems, Inc.
 */
public class JaasAuthenticator
    implements Authenticator
{
    /** The User Domain. */
    private UserDomain mDomain;
    
    /**
     * The Privileged Action to create a Login Context etc.
     *
     * @author Sun Microsystems, Inc.
     */
    class LoginPrivilegedAction 
        implements java.security.PrivilegedExceptionAction
    {
        /** Callback Handler. */
        private CallbackHandler mHandler = null;

        /** Subject. */
        private Subject mSubject = null;

        /**
         * Ctor.
         *
         * @param subj - Subject.
         * @param hndlr - CallbackHandler
         */
        public LoginPrivilegedAction(Subject subj, CallbackHandler hndlr)
        {
            mSubject = subj;
            mHandler = hndlr;
        }

        /**
         * @throws LoginException if Login fails.
         * @return null.
         */
        public Object run() throws LoginException
        {
            try
            {
                login();
            }
            catch ( LoginException lex )
            {
                // -- reattempt login after refreshing configuration
                Configuration.getConfiguration().refresh();
                login();
            }
            return null;

        }

        /**
         * login.
         *
         * @throws LoginException if login fails.
         */
        private void login() throws LoginException
        {
            LoginContext lc = new LoginContext(mDomain.getName(), 
                mSubject, mHandler);
            lc.login();
            lc = null;
        }


    } 
    
    /**
     * Authenticate a user and update the Subject with the authenticated
     * Identity. This authenticator uses the name of the User Domain as 
     * the JAAS Context used for authentication.
     *
     * @param subject is the Subject to be authneticated. If the passed Subject is null 
     * then the TLS is checked to see if there is a Subject set there, if not then 
     * a new Subject is created.
     * @param handler is the CallbackHandler the authneticator can use for
     * authentication. The 
     * @return true if authentication succeeds, false otherwise.
     */
    public boolean authenticate(Subject subject, CallbackHandler handler)
    {
        try
        {            
            java.security.AccessController.doPrivileged( 
                new LoginPrivilegedAction(subject, handler), 
                    java.security.AccessController.getContext());
            return true;
        }
        catch (Throwable lex)
        {
            lex.printStackTrace();
        }
        return false;
        
    }
    
    /**
     * Initialize the authenticator with the UserDomain.
     *
     * @param domain is the UserDomain which has the Authentication Context.
     * @throws IllegalStateException if initilaization fails.
     */
    public void initialize(UserDomain domain)
        throws IllegalStateException 
    {
        mDomain = domain;
    }
    
    /**
     * The implementation on the CallbackHandlerInterface. This class only supports 
     * PasswordValidationCallback
     * 
     *
     * @param callbacks - array of Callbacks to be handled.
     * @throws java.io.IOException - if an input or output error occurs.
     * @throws UnsupportedCallbackException - if the implementation of this method
     * does not support one or more of the Callbacks specified in the callbacks
     * parameter.
     */
    public void handle (Callback[] callbacks) 
        throws java.io.IOException, UnsupportedCallbackException
    {
        for (int i = 0; i < callbacks.length; i++)
        {
            CallbackHandler handler = null;

            if  (callbacks[i] instanceof PasswordValidationCallback)
            {
                PasswordValidationCallback authCb = 
                    (PasswordValidationCallback) callbacks[i];
                
                authCb.setResult(authenticate(
                    com.sun.jbi.internal.security.ThreadLocalContext.getLocalSubject(), 
                    new UsrPwdCallbackHandler(authCb.getUsername(),
                        authCb.getPassword())) );  
            }
            else
            {
                throw new UnsupportedCallbackException (callbacks[i]);
            }
        }
    }
}
