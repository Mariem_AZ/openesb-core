/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SecretKeyCallbackHandler.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  SecretKeyCallbackHandler.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on February 23, 2005, 1:53 PM
 */

package com.sun.jbi.internal.security.callback;

import com.sun.enterprise.security.jauth.callback.SecretKeyCallback;
import com.sun.jbi.internal.security.KeyStoreManager;

import java.io.IOException;
import java.security.Key;
import java.security.KeyStore;
import javax.crypto.SecretKey;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;

/**
 * This CallbackHandler handles the SecretKey Callback.
 *
 * @author Sun Microsystems, Inc.
 */
public class SecretKeyCallbackHandler
    implements CallbackHandler
{
     /** The reference to the KeyStoreManager. */
    private KeyStoreManager mKeyMgr;
    
    /** The X509 Certificate Type. */
    private static final String X509 = "X509";
    
    /** 
     * Creates a new instance of SecretKeyCallbackHandler.
     * 
     * @param mgr - KeyStoreManager instance which provides the handle
     * to the KeyStores.
     */
    public SecretKeyCallbackHandler (KeyStoreManager mgr)
    {
        mKeyMgr = mgr;
    }
    
    /**
     * The implementation on the CallbackInterface. This method only handles 
     * SecretKeyCallback.
     *
     * @param callbacks - array of Callbacks to be handled.
     * @throws IOException - if an input or output error occurs. 
     * @throws UnsupportedCallbackException - if the implementation of this method
     * does not support one or more of the Callbacks specified in the callbacks 
     * parameter.
     */
    public void handle(Callback[] callbacks)
        throws IOException, UnsupportedCallbackException
    {
        for (int i = 0; i < callbacks.length; i++) 
        {
            CallbackHandler handler = null;
            
            if  (callbacks[i] instanceof SecretKeyCallback)
            {
                SecretKeyCallback cb = (SecretKeyCallback) callbacks[i];
                Object req = cb.getRequest();
                cb.setKey(null);
                if ( req instanceof SecretKeyCallback.AliasRequest )
                {
                    handleAliasRequest( (SecretKeyCallback.AliasRequest) req, cb);
                }
            } 
            else
            {
                throw new UnsupportedCallbackException(callbacks[i]);
            }
        }
    }
    
    
    /**
     * Handle a request for a Secret Key based on alias. Go through the KeyStore and
     * get the Secret Key for the alias. If there is no key entry for the alias then
     * the key the Callback is null.
     *
     * @param req is the alias request from the SecretKeyCallback
     * @param cb is the SecretKeyCallback
     * @throws IOException - if an input or output error occurs. This would indicate
     * that the Key could not be retrieved from the store.
     */
    private void handleAliasRequest(
        SecretKeyCallback.AliasRequest req, SecretKeyCallback cb)
        throws IOException
    {
        KeyStore ks = mKeyMgr.getKeyStore();
        
        if ( req.getAlias() == null )
        {
            cb.setKey(null);
        }
        
        try
        {
            if ( ks.containsAlias(req.getAlias()))
            {
                Key key = ks.getKey(req.getAlias(), 
                    mKeyMgr.getKeyStorePassword().toCharArray());
                if ( key instanceof SecretKey )
                {
                    cb.setKey( (SecretKey) key );
                }
            }
        }
        catch ( Exception ex )
        {
            throw new IOException (ex.getMessage ());
        }
        
    }
    
}
