/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestGenericQueryImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.registry.xml;

import com.sun.jbi.management.ConfigurationCategory;
import com.sun.jbi.management.registry.GenericQuery;
import com.sun.jbi.management.registry.Registry;
import com.sun.jbi.management.registry.RegistryBuilder;
import com.sun.jbi.management.repository.ArchiveType;
import com.sun.jbi.management.repository.Repository;
import com.sun.jbi.management.system.Util;

import java.io.File;
import java.math.BigInteger;
import java.util.List;
import org.junit.After;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class TestGenericQueryImpl
{
    /**
     * The sample Configuration Directory.
     */
    private String mConfigDir = null;
    private File mRegFile;
    String mRegFilePath;
    String mRegGoodFilePath;
    String mComponentZipPath;
    String mSharedLibraryZipPath;
    String mServiceAssemblyZipPath;
    
    static final String COMPONENT_NAME = "SunSequencingEngine";
    static final String SHARED_LIBRARY_NAME = "SunWSDLSharedLibrary";
    static final String SERVICE_ASSEMBLY_NAME = "CompositeApplication";
    static final String SERVICE_UNIT_NAME = "ESB_ADMIN_SERVICE_UNIT_1";

    @Before
    public void setUp()
        throws Exception
    {
        String srcroot = Util.getSourceRoot();
        mConfigDir = srcroot + "/target/test-classes/testdata";

        mRegFilePath        = mConfigDir + File.separator + "jbi-registry.xml";
        mRegGoodFilePath    = mConfigDir + File.separator + "jbi-registry-good.xml";
        mComponentZipPath   = mConfigDir + File.separator + "component.zip";
        mServiceAssemblyZipPath   = mConfigDir + File.separator + "service-assembly.zip";
        mSharedLibraryZipPath     = mConfigDir + File.separator + "wsdlsl.jar";
      
        mRegFile = new File(mRegFilePath);
        
        if ( mRegFile.exists())
        {
            mRegFile.delete();
        }
        Util.fileCopy(mRegGoodFilePath, mRegFilePath);
    }

    @After
    public void tearDown()
        throws Exception
    {
        // -- restore registry.xml
       RegistryBuilder.destroyRegistry(); 
       Util.fileCopy(mRegGoodFilePath, mRegFilePath );
    }

    /**
     * @throws Exception if an unexpected error occurs
     */
    @Test
    public void getClustersDeployingServiceAssembly()
           throws Exception
    {
        Registry reg = Util.createRegistry(true);
        GenericQuery query = reg.getGenericQuery();
        
        List<String> clusters = query.getClustersDeployingServiceAssembly(SERVICE_ASSEMBLY_NAME);
        
        assertTrue(clusters.contains("clusterA"));
    }
    
    /**
     * @throws Exception if an unexpected error occurs
     */
    @Test
    public void getClustersInstallingComponent()
           throws Exception
    {
        Registry reg = Util.createRegistry(true);
        GenericQuery query = reg.getGenericQuery();
        
        List<String> clusters = query.getClustersInstallingComponent(COMPONENT_NAME);
        assertTrue(clusters.contains("clusterA"));
    }
    
    /**
     * @throws Exception if an unexpected error occurs
     */
    @Test
    public void getClustersInstallingSharedLibrary()
           throws Exception
    {
        Registry reg = Util.createRegistry(true);
        GenericQuery query = reg.getGenericQuery();
        
        List<String> clusters = query.getClustersInstallingSharedLibrary(SHARED_LIBRARY_NAME);
        assertTrue(clusters.contains("clusterA"));
    }
    
    @Test
    public void getComponentInstallationDescriptor()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        GenericQuery query = reg.getGenericQuery();
        
        Repository repository = reg.getRegistrySpec().getManagementContext().getRepository();
        
        repository.addArchive(ArchiveType.COMPONENT, mComponentZipPath);
        
         String jbiXml = query.getComponentInstallationDescriptor(COMPONENT_NAME);
        assertTrue(jbiXml.startsWith("<?xml"));
        repository.purge();
    }
    
    @Test
    public void getSharedLibraryInstallationDescriptor()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        GenericQuery query = reg.getGenericQuery();
        
        Repository repository = reg.getRegistrySpec().getManagementContext().getRepository();
        
        repository.addArchive(ArchiveType.SHARED_LIBRARY, mSharedLibraryZipPath);
        
        String jbiXml = query.getSharedLibraryInstallationDescriptor(SHARED_LIBRARY_NAME);
        assertTrue(jbiXml.startsWith("<?xml"));
        repository.purge();
    }
    
    @Test
    public void getServiceAssemblyDeploymentDescriptor()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        GenericQuery query = reg.getGenericQuery();
        
        Repository repository = reg.getRegistrySpec().getManagementContext().getRepository();
        
        repository.addArchive(ArchiveType.SERVICE_ASSEMBLY, mServiceAssemblyZipPath);
        
        String jbiXml = query.getServiceAssemblyDeploymentDescriptor(SERVICE_ASSEMBLY_NAME);
        assertTrue(jbiXml.startsWith("<?xml"));
        repository.purge();
    }
    
    @Test
    public void getServiceUnitDeploymentDescriptor()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        GenericQuery query = reg.getGenericQuery();
        
        Repository repository = reg.getRegistrySpec().getManagementContext().getRepository();
        
        repository.addArchive(ArchiveType.SERVICE_ASSEMBLY, mServiceAssemblyZipPath);
        
        String jbiXml = query.getServiceUnitDeploymentDescriptor(SERVICE_ASSEMBLY_NAME, SERVICE_UNIT_NAME);
        assertTrue(jbiXml.startsWith("<?xml"));
        repository.purge();
    }
    
    @Test
    public void isServiceAssemblyDeployed()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        GenericQuery query = reg.getGenericQuery();
        
        assertFalse(query.isServiceAssemblyDeployed("SA"));
        assertTrue(query.isServiceAssemblyDeployed(SERVICE_ASSEMBLY_NAME));
    }
  
    /**
     * This method is used to test the schemaorg_apache_xmlbeans.system-install attribute
     */    
    @Test
    public void isSystemComponent() throws Exception
    {
        Registry reg = Util.createRegistry(true);
        GenericQuery query = reg.getGenericQuery();
        
        assertFalse(query.isSystemComponent("SunSequencingEngine"));
        assertFalse(query.isSystemComponent("SunJMSBinding"));
    }
    
    /**
     * This method is used to test the schemaorg_apache_xmlbeans.system-install attribute for a
     * shared library
     */    
    @Test
    public void isSystemSharedLibrary() throws Exception
    {
        Registry reg = Util.createRegistry(true);
        GenericQuery query = reg.getGenericQuery();
        
        assertFalse(query.isSystemSharedLibrary("SunWSDLSharedLibrary"));

    }
    
    /**
     * Test getting the component file name
     */
    @Test
    public void getComponentFileName()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        GenericQuery query = reg.getGenericQuery();
        
        assertEquals(query.getComponentFileName(COMPONENT_NAME), "sequenceengine.zip");
        assertNull(query.getComponentFileName("xyz"));
    }

    
    /**
     * Test getting the shared library file name
     */
    @Test
    public void getSharedLibraryFileName()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        GenericQuery query = reg.getGenericQuery();
        
        assertEquals(query.getSharedLibraryFileName(SHARED_LIBRARY_NAME), 
            "wsdlsl.jar");
        
        assertNull(query.getSharedLibraryFileName("xyz"));
    }
    
    /**
     * Test getting the service assembly file name
     */
    @Test
    public void getServiceAssemblyFileName()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        GenericQuery query = reg.getGenericQuery();
        
        assertEquals(query.getServiceAssemblyFileName(SERVICE_ASSEMBLY_NAME), 
            "service-assembly.zip");
        
        assertNull(query.getServiceAssemblyFileName("xyz"));
    }
    
    /**
     * Test isComponentRegistered(), the component should be removed from the
     * registry, since there is no repos entry and a false is returned
     */
    @Test
    public void isComponentRegistered()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        GenericQuery query = reg.getGenericQuery();
        
        assertTrue(query.isComponentRegistered(COMPONENT_NAME));
    }
    
    /**
     * Test isSharedLibraryRegistered(), the library should be removed from the
     * registry, since there is no repos entry and a false is returned
     */
    @Test
    public void isSharedLibraryRegistered()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        GenericQuery query = reg.getGenericQuery();
        
        assertTrue(query.isSharedLibraryRegistered(SHARED_LIBRARY_NAME));
    }
    
    /**
     * Test isServiceAssemblyRegistered(), the sa should be removed from the
     * registry, since there is no repos entry and a false is returned
     */
    @Test
    public void isServiceAssemblyRegistered()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        GenericQuery query = reg.getGenericQuery();
        
        Repository repository = reg.getRegistrySpec().getManagementContext().getRepository();
        
        repository.addArchive(ArchiveType.COMPONENT, mComponentZipPath);
        assertTrue(query.isServiceAssemblyRegistered(SERVICE_ASSEMBLY_NAME));
        
        com.sun.jbi.ComponentQuery compQuery = reg.getComponentQuery("server");
        
        com.sun.jbi.ComponentInfo compInfo = compQuery.getComponentInfo(COMPONENT_NAME);
        assertNotNull(compInfo);
        
        List<com.sun.jbi.ServiceUnitInfo> suList = compInfo.getServiceUnitList();
        
        assertFalse(suList.isEmpty());
        repository.purge();
    }
    
    /**
     * Test getting a configuration attribute from the domain-configuration
     */
    @Test
    public void getAttribute()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        GenericQuery query = reg.getGenericQuery();
        
        String attrValue = query.getAttribute("domain", ConfigurationCategory.Installation, 
            "componentTimeout");
        
        assertTrue("5000".equals(attrValue));
    }
    
    /**
     * Test getting a configuration attribute for a target which is not overriden.
     */
    @Test
    public void getAttributeForTarget()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        GenericQuery query = reg.getGenericQuery();
        
        String attrValue = query.getAttribute("clusterB", ConfigurationCategory.Installation, 
            "componentTimeout");
        
        assertTrue("5000".equals(attrValue));
    }
    
    /**
     * Test getting a configuration attribute for a target which is overriden.
     */
    @Test
    public void getAttributeForOverridenTarget()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        GenericQuery query = reg.getGenericQuery();
        
        String attrValue = query.getAttribute("clusterA", ConfigurationCategory.Installation, 
            "componentTimeout");
        
        assertTrue("4000".equals(attrValue));
    }
    
    /**
     * Test getting a missing configuration attribute, should get back a null.
     */
    @Test
    public void getNonExistantAttribute()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        GenericQuery query = reg.getGenericQuery();
        
        String attrValue = query.getAttribute("clusterA", ConfigurationCategory.Installation, 
            "someTimeout");
        
        assertNull(attrValue);
    }
    
    /**
     * Test if domain config exists
     */
    @Test
    public void isGlobalConfigurationDefined()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        GenericQuery query = reg.getGenericQuery();
        assertTrue(query.isGlobalConfigurationDefined());
    }
    
    /**
     * This method is used to test the method getComponentUpgradeNumber.
     * upgrade-number is an optional attribute. For a component that does not have
     * it would return 0.
     */
    @Test
    public void getComponentUpgradeNumber()
        throws Exception
    {
        Registry reg = Util.createRegistry(true);
        GenericQuery query = reg.getGenericQuery();
        assertEquals(query.getComponentUpgradeNumber("SunSequencingEngine"), BigInteger.ZERO);        
        assertEquals(query.getComponentUpgradeNumber("SunJMSBinding"), BigInteger.ONE);        
    }        
}
