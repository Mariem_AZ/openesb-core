/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MessageServiceMBean.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.system;

/**
 * MessageServiceMBean defines the interface that must be implemented
 * for MessageService management in a JBI Framework.
 *
 * @author Sun Microsystems, Inc.
 */
public interface MessageServiceMBean
{
    /** Returns the total number of DeliveryChannels that have been activated
     *  in the NMR.
     *  @return number of active DeliveryChannels
     */
    public int getActiveChannelCount();
    
    /** Returns the total number of EndpointReferences that have been activated
     *  in the NMR.
     *  @return number of active EndpointReferences
     */
    public int getActiveEndpointCount();
    
    /** Returns a list of component IDs corresponding to active channels in the NMR.
     *  @return list of component IDs
     */
    public String[] getActiveChannels();
    
    /** Returns a list of active endpoints in the NMR.  
     *  @return list of activated endpoints
     */
    public String[] getActiveEndpoints();
    
    /** Returns a list of active consuming endpoints in the NMR.  
     *  @return list of activated consuming endpoints
     */
    public String[] getActiveConsumingEndpoints();
    
    /** Identical to getActiveEndpoints(ownerid), but list is limited to consuming endpoints 
     *  registered by the specified component.
     *  @param ownerId component identifier
     *  @return list of activated consuming endpoints
     */
    public String[] getActiveConsumingEndpoints(String ownerId);
    
    /** Identical to getActiveEndpoints(), but list is limited to endpoints 
     *  registered by the specified component.
     *  @param ownerId component identifier
     *  @return list of activated endpoints
     */
    public String[] getActiveEndpoints(String ownerId);
    
    /** Provides metadata query facility for endpoints registered with the NMR.
     *  This method returns the contents of an XML descriptor as a string.
     *  @param service string representation of service QName
     *  @param endpoint endpoint name
     *  @return XML descriptor as string
     */
    public String getDescriptor(String service, String endpoint);
    
    /** Dumps state information to the AS log.
     */
    public void dumpState();
}
