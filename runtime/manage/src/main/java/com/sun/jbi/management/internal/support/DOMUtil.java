/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DOMUtil.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.internal.support;


import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.Writer;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.StringTokenizer;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.namespace.NamespaceContext;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import org.xml.sax.InputSource;
import org.w3c.dom.Text;
import org.w3c.dom.Element;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/**
 * This object provides utility methods to manipulate DOM tree.
 * @author Sun Microsystems, Inc.
 */
public class DOMUtil
{
    /** static object to access the methods of this object. */
    public static final DOMUtil UTIL = new DOMUtil ();
    
    /** The namespaceURI represented by the prefix <code>xmlns</code>. */
  public static final String NS_URI_XMLNS = "http://www.w3.org/2000/xmlns/";
  
    /**
     * this is an instance of the namespacecontext class
     */
    private static final NamespaceContext NAMESPACE_CONTEXT = new JBINamespaceContext();
  
    /** Creates a new instance of DOMUtil. */
    public DOMUtil ()
    {
    }
    
    /** gets the element.
     * @param aParentDocument Document for parent node
     * @param aTagName String for tagname
     * @return Element with tagname
     */
    public Element getElement (Document aParentDocument, String aTagName)
    {
        NodeList nodeList = aParentDocument.getElementsByTagName (aTagName);
        return ( nodeList != null ) ? (Element) nodeList.item (0) : null;
    }
    
    /** gets the element.
     * @param elem Element for parent node
     * @param tagName String for tagname
     * @return Element with tagname
     */
    public Element getElement (Element elem, String tagName)
    {
        NodeList nl = elem.getElementsByTagName (tagName);
        Element childElem = (Element) nl.item (0);
        
        return childElem;
    }
    
    /** gets the element value.
     * @param doc Document for parent node
     * @param elemName String for element name
     * @return Element value
     */
    public String getElementValue (Document doc, String elemName)
    {
        String elemVal = null;
        
        Element elem = getElement (doc, elemName);
        elemVal = getTextData (elem);
        
        return elemVal;
    }
    
    /**
     * get attribute
     * @param aElement Element object
     * @param aAttribute String value of attribute
     * @param aPrefix String value of prefix
     * @return String value of Attr value
     */
    public String getAttribute (Element aElement, String aAttribute, String aPrefix )
    {
        String attr = getName (aAttribute,aPrefix);
        return aElement.getAttribute (attr);
    }
    
    /**
     * get attribute
     * @param aElement Element object
     * @param aAttribute String value of attribute
     * @return String value of Attr value
     */
    public String getAttribute (Element aElement, String aAttribute )
    {
        return getAttribute (aElement, aAttribute, aElement.getPrefix ());
    }
    
    /** use this util method to create/retrieve a Text Node in a element.
     * @param aElement Element node
     * @return Text node for text data
     */
    private Text getText (Element aElement )
    {
        Node node = null;
        aElement.normalize ();
        node = aElement.getFirstChild ();
        if ( node == null || !(node instanceof Text) )
        {
            node = aElement.getOwnerDocument ().createTextNode ("");
            aElement.appendChild (node);
        }
        return (Text) node;
    }
    
    /** use this util method to set a Text Data in a element.
     * @param aElement Element for text node
     * @param aData String contains text
     */
    public void setTextData (Element aElement, String aData)
    {
        getText (aElement).setData (aData);
    }
    
    /** use this util method to retrieve a Text Data in a element.
     * @param aElement Element for text node
     * @return String contains text
     */
    public String getTextData (Element aElement)
    {
        return getText (aElement).getData ();
    }
   
    /**
     * Gets the local name from the quname.
     *
     * @param qname Qualified name of service.
     *
     * @return String local name
     */
    public String getLocalName(String qname)
    {
        StringTokenizer tok = new StringTokenizer(qname, ":");

        try
        {
            if (tok.countTokens() == 1)
            {
                return qname;
            }
            tok.nextToken();
            return tok.nextToken();
        }
        catch (Exception e)
        {
            return "";
        }
    }

    /**
     * Gets the namespace from the qname.
     *
     * @param qname Qname of service
     *
     * @return namespace namespace of service
     */
    public String getNamespace(Element el, String qname)
    {
        String namespace = "";
        String prefix;
        
        if (qname.indexOf(':') > 0)
        {
            prefix = qname.split(":")[0];
            namespace = el.lookupNamespaceURI(prefix);
        }

        return namespace;
    }
    
    /** use this util method to retrieve a QName in a attribute.
     * @param aElement Element containing the attribute
     * @param attrName attribute name
     */
    public QName getQualifiedAttributeValue(Element el, String attrName)
      throws IllegalArgumentException 
    {
        String attrValue = getAttribute(el, attrName);
        String attrValueLocalPart = getLocalName(attrValue);
        String attrValueNamespaceURI = getNamespace(el, attrValue);
        
        if (attrValueNamespaceURI != null) 
        {
            return new QName(attrValueNamespaceURI, attrValueLocalPart);
        }
           
        
        return null;
    }
      
    /** save document to stream.
     * @param aDocument Document
     * @param aWriter is what the aDocument is serialized to.
     * @return String representation of Document
     * @throws Exception If fails to construct a string.
     */
    public String DOM2String ( Document aDocument, Writer aWriter )
        throws Exception
    {
        TransformerFactory transformerFactory = TransformerFactory.newInstance ();
        Transformer transformer;
        transformer = transformerFactory.newTransformer ();
        
        transformer.setOutputProperty (OutputKeys.METHOD, "xml");
        transformer.setOutputProperty (OutputKeys.OMIT_XML_DECLARATION, "no");
        transformer.setOutputProperty (OutputKeys.STANDALONE, "yes");
        transformer.setOutputProperty (OutputKeys.CDATA_SECTION_ELEMENTS, "");
        transformer.setOutputProperty (OutputKeys.INDENT, "no");

        transformer.transform (new DOMSource (aDocument), new StreamResult (aWriter));
        return aWriter.toString ();
    }
    
    /**
     * Convert an element to String
     *
     * @param element - the element to convert to String
     */
    public String elementToString(Element element)
        throws Exception
    {
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        transformer.setOutputProperty (OutputKeys.METHOD, "xml");
        transformer.setOutputProperty (OutputKeys.INDENT, "yes");
        StringWriter sw = new StringWriter();
        transformer.transform(new DOMSource(element), new StreamResult(sw)); 
        return sw.toString();
    }
    
    /** gets list of elements.
     * @param aParentElement Element for parent
     * @param aTagName String for tagname
     * @return NodeList with tagname
     */
    public NodeList getElements (Element aParentElement, String aTagName)
    {
        return aParentElement.getElementsByTagNameNS (
            aParentElement.getNamespaceURI (), aTagName);
    }
    
    /** gets set of elements.
     * @param aParentDocument Document for parent node
     * @param aTagName String for tagname
     * @return NodeList with tagname
     */
    public NodeList getElements (Document aParentDocument, String aTagName)
    {
        return aParentDocument.getElementsByTagNameNS ("*", aTagName);
    }
    
    /** get the children of the same type element tag name.
     * @param aElement Element for parent node
     * @param aElementTagName String for tagname
     * @return NodeList for list of children with the tagname
     */
    public NodeList getChildElements (Element aElement, String aElementTagName)
    {
        NodeList nodeList = aElement.getChildNodes ();
        NodeListImpl list = new NodeListImpl ();
        int count = nodeList.getLength ();
        for ( int i = 0; i < count; ++i )
        {
            Node node = nodeList.item (i);
            if ( node instanceof Element )
            {
                String tagName = getElementName ((Element) node);
                if ( tagName.equals (aElementTagName) )
                {
                    list.add (node);
                }
            }
        }
        return list;
    }
    
    /**
     * get Element Tag Name with striped prefix.
     * @param aElement Element object
     * @return String with stripped prefix
     */
    public String getElementName (Element aElement)
    {
        String tagName = aElement.getTagName ();
        return getName (tagName);
    }
    
    /**
     * strips the prefix of the name if present.
     * @param aName String value of Name with prefix
     * @return String for name after striping prefix
     */
    public String getName (String aName)
    {
        int lastIdx = aName.lastIndexOf (':');
        if ( lastIdx >= 0 )
        {
            return aName.substring (lastIdx + 1);
        }
        return aName;
    }
    
    /** adds the prefix to the name
     * @param aName String value of Name with prefix
     * @param aPrefix String value of prefix
     * @return  name with prefix prefixed*/
    public String getName (String aName, String aPrefix)
    {
        // TODO: check if the prefix already exists and throw exception
        if ( aPrefix != null && aPrefix.length () > 0 )
        {
            return aPrefix + ":" + aName;
        } else
        {
            return aName;
        }
    }
    
    
    /**
     * NodeListImpl.
     *
     */
    public static class NodeListImpl extends ArrayList implements NodeList
    {
        /** Default Constructor.
         */
        public NodeListImpl ()
        { super (); }
        /** nodelist length.
         * @return int for number of nodes in nodelist
         */
        public int getLength ()
        {
            return this.size ();
        }
        /** return a node.
         * @param aIndex int for the index of the node
         * @return Node at the index
         */
        public Node item (int aIndex)
        {
            return (Node) this.get (aIndex);
        }
    }
    
    
    /**
     * This method is used to check if the value of the an element is same in the
     * both of the documents provided.
     * @param file1 that contains a descriptor
     * @param file2 that contains the other descriptor
     * @param elementName the element name
     * @return boolean if the elements are equal
     * @throws ManagementException if there are problems in comparing the documents
     */
    public static boolean areElementsEqual(
            File file1, 
            File file2, 
            String xpathExp)
    throws com.sun.jbi.management.system.ManagementException
    {
        try 
        {
            XPathFactory factory = XPathFactory.newInstance();
            XPath xpath = factory.newXPath();
            
            //inner class for namespace prefix to uri mapping
            xpath.setNamespaceContext(NAMESPACE_CONTEXT);
   
            InputSource inputSource1 = 
                    new InputSource(new FileInputStream(file1));            
            InputSource inputSource2 =
                    new InputSource(new FileInputStream(file2));            
            
            String element1 = xpath.evaluate(xpathExp, inputSource1);           
            String element2 = xpath.evaluate(xpathExp, inputSource2);

            return element1.equals(element2);
        } 
        catch (javax.xml.xpath.XPathExpressionException xpathEx)
        {
            Logger.getLogger("com.sun.jbi.management").log(
                    Level.WARNING,
                    xpathEx.getMessage(),
                    xpathEx);
            throw new com.sun.jbi.management.system.ManagementException(xpathEx.toString());
        }
        catch (java.io.IOException ioEx)
        {
            Logger.getLogger("com.sun.jbi.management").log(
                    Level.WARNING,
                    ioEx.getMessage(),
                    ioEx);
            throw new com.sun.jbi.management.system.ManagementException(ioEx.toString());
        }       
        catch (Exception ex)
        {
            //if the supplied document is bad, we may end up with a NPE
            Logger.getLogger("com.sun.jbi.management").log(
                    Level.WARNING,
                    ex.getMessage(),
                    ex);
            throw new com.sun.jbi.management.system.ManagementException(ex.toString());
        }                    
                
    }

    /**
     * This namespacecontext class is used to map the 
     * prefix jbi to the URI http://java.sun.com/xml/ns/jbi
     */
    static class JBINamespaceContext implements NamespaceContext 
    {

        /**
         * This method maps the prefix jbi to the URI
         *  http://java.sun.com/xml/ns/jbi
         */
        public String getNamespaceURI(String prefix) 
        {
            if (prefix.equals("jbi"))
            {
                return "http://java.sun.com/xml/ns/jbi";
            }
            else
            {
                return NS_URI_XMLNS;
            }
        }

        public String getPrefix(String uri) {
            throw new UnsupportedOperationException();
        }

        public Iterator getPrefixes(String uri) {
            throw new UnsupportedOperationException();
        }

}            
    
}
