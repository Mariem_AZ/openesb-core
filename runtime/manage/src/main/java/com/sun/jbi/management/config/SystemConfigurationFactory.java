/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SystemConfigurationFactory.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.config;

import com.sun.jbi.management.ConfigurationCategory;
import com.sun.jbi.management.LocalStringKeys;

import java.util.Properties;

import javax.management.Descriptor;
import javax.management.modelmbean.ModelMBeanAttributeInfo;


/**
 * SystemConfigurationFactory defines the common JBI runtime configuration parameters.
 * Any new parameters would require updates to this class.
 * 
 * @author Sun Microsystems, Inc.
 */
public class SystemConfigurationFactory 
    extends ConfigurationFactory
{
    /** Attribute Names */
    public static final String JBI_HOME              = "jbiHome";
    public static final String HEART_BEAT_INTERVAL   = "heartBeatInterval";
    public static final String MSG_SVC_STATS_ENABLED = "msgSvcTimingStatisticsEnabled";
    
    private static final int sNumAttributes = 3;
    
    /**
     * Constructor 
     *
     * @param defProps - default properties
     */
    public SystemConfigurationFactory(Properties defProps)
    {
        super(defProps, ConfigurationCategory.System);
    }
    
    /**
     *
     */
    public ModelMBeanAttributeInfo[] createMBeanAttributeInfo()
    {
         ModelMBeanAttributeInfo[] attributeInfos = new ModelMBeanAttributeInfo[sNumAttributes];
         DescriptorSupport descr;
         String attrDescr;
         
         // -- JBI Home
         descr = new DescriptorSupport();
         attrDescr = getString(LocalStringKeys.JBI_HOME_DESCR);
         descr.setAttributeName(JBI_HOME);
         descr.setDisplayName(getString(LocalStringKeys.JBI_HOME_DISPLAY_NAME));
         descr.setDisplayNameId(getToken(LocalStringKeys.JBI_HOME_DISPLAY_NAME));
         descr.setDescriptionId(getToken(LocalStringKeys.JBI_HOME_DESCR));
         descr.setToolTip(getString(LocalStringKeys.JBI_HOME_TOOLTIP));
         descr.setToolTipId(getToken(LocalStringKeys.JBI_HOME_TOOLTIP));
         descr.setResourceBundleName("com.sun.jbi.management");
         descr.setIsStatic(true);
         descr.setIsPassword(false);
         descr.setDefault(getJbiHome());
         
         attributeInfos[0] = new ModelMBeanAttributeInfo(
            // name                type   descr      R      W    isIs  Descriptor      
            JBI_HOME, "java.lang.String", attrDescr, true, false, false, descr);
         
         
         // Heart Beat
         descr = new DescriptorSupport();
         attrDescr = getString(LocalStringKeys.HEART_BEAT_INTERVAL_DESCR);
         descr.setAttributeName(HEART_BEAT_INTERVAL);
         descr.setDisplayName(getString(LocalStringKeys.HEART_BEAT_INTERVAL_DISPLAY_NAME));
         descr.setDisplayNameId(getToken(LocalStringKeys.HEART_BEAT_INTERVAL_DISPLAY_NAME));
         descr.setDescriptionId(getToken(LocalStringKeys.HEART_BEAT_INTERVAL_DESCR));
         descr.setToolTip(getString(LocalStringKeys.HEART_BEAT_INTERVAL_TOOLTIP));
         descr.setToolTipId(getToken(LocalStringKeys.HEART_BEAT_INTERVAL_TOOLTIP));
         descr.setResourceBundleName("com.sun.jbi.management");
         descr.setIsStatic(false);
         descr.setIsPassword(false);
         String defHeartBeat = mDefaults.getProperty(getQualifiedKey(HEART_BEAT_INTERVAL), "5500");
         descr.setDefault(Integer.parseInt(defHeartBeat));
		 descr.setMinValue(1000);
		 descr.setMaxValue(Integer.MAX_VALUE);
         descr.setUnit("milliseconds");
         
         
         attributeInfos[1] = new ModelMBeanAttributeInfo(
            // name               type  descr      R      W    isIs  Descriptor      
            HEART_BEAT_INTERVAL, "int", attrDescr, true, true, false, descr);
         
         // Message Service Timing Statistics
         descr = new DescriptorSupport();
         attrDescr = getString(LocalStringKeys.MSG_SVC_STATS_ENABLED_DESCR);
         descr.setAttributeName(MSG_SVC_STATS_ENABLED);
         descr.setDisplayName(getString(LocalStringKeys.MSG_SVC_STATS_ENABLED_DISPLAY_NAME));
         descr.setDisplayNameId(getToken(LocalStringKeys.MSG_SVC_STATS_ENABLED_DISPLAY_NAME));
         descr.setDescriptionId(getToken(LocalStringKeys.MSG_SVC_STATS_ENABLED_DESCR));
         descr.setToolTip(getString(LocalStringKeys.MSG_SVC_STATS_ENABLED_TOOLTIP));
         descr.setToolTipId(getToken(LocalStringKeys.MSG_SVC_STATS_ENABLED_TOOLTIP));
         descr.setResourceBundleName("com.sun.jbi.management");
         descr.setIsStatic(false);
         descr.setIsPassword(false);
         String defMsgSvcStatsEnabled 
                 = mDefaults.getProperty(getQualifiedKey(MSG_SVC_STATS_ENABLED), "false");
         descr.setDefault(Boolean.parseBoolean(defMsgSvcStatsEnabled));
         
         
         attributeInfos[2] = new ModelMBeanAttributeInfo(
            // name                type       descr      R      W    isIs  Descriptor      
            MSG_SVC_STATS_ENABLED, "boolean", attrDescr, true, true, true, descr);

         return attributeInfos;
    }
    
    /*--------------------------------------------------------------------------------*\
     *                         private helpers                                        *
    \*--------------------------------------------------------------------------------*/
    
    /**
     * @return the abosolute path to the JBI Home directory
     */
    private String getJbiHome()
    {
        com.sun.jbi.EnvironmentContext envCtx = com.sun.jbi.util.EnvironmentAccess.getContext();
        return envCtx.getJbiInstallRoot();
    }
}
