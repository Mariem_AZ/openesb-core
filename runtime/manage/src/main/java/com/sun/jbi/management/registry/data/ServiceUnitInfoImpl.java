/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServiceUnitInfoImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.registry.data;

import com.sun.jbi.ServiceUnitState;
import com.sun.jbi.management.registry.Registry;
import com.sun.jbi.management.system.ManagementContext;

/**
 * This interface provides information on Service Units.
 * @author Sun Microsystems, Inc.
 */
public class ServiceUnitInfoImpl
    implements com.sun.jbi.ServiceUnitInfo
{

    private ServiceUnitState mServiceUnitState;
    private String           mServiceUnitName;
    private String           mServiceAssemblyName;
    private String           mTargetComponent;
    private String           mFilePath;
    
    /**
     * Determine if the supplied Object is equal to this one.
     * @param object - the Object to be compared with this one.
     * @return true if the supplied Object is equal to this one.
     */
    public boolean equals(Object object)
    {
        return ( object.hashCode() == this.hashCode() );
    }

    /**
     * Get the name of the Service Unit.
     * @return the name of the Service Unit.
     */
    public String getName()
    {
        return mServiceUnitName;
    }

    public void setName(String name)
    {
        mServiceUnitName = name;
    }
    /**
     * Get the name of the Service Assembly containing this Service Unit.
     * @return the name of the Service Assembly.
     */
    public String getServiceAssemblyName()
    {
        return mServiceAssemblyName;
    }
    
    public void setServiceAssemblyName(String name)
    {
        mServiceAssemblyName = name;
    }

    /**
     * Get the state of the Service Unit.
     * @return current state.
     */
    public ServiceUnitState getState()
    {
        return mServiceUnitState;
    }
    
    public void setState(ServiceUnitState state)
    {
        mServiceUnitState = state;
    }

    /**
     * Get the state of the Service Unit as a string.
     * @return current state, as one of the values: "shutdown", "stopped",
     * "started".
     */
    public String getStateAsString()
    {
        return mServiceUnitState.toString();
    }
    
    /**
     * Get the Service Units target component
     *
     * @return the target component name
     */
    public String getTargetComponent()
    {
        return mTargetComponent;
    }
    
    /**
     * Set the Service Units target component
     *
     * @param compName - the target component name
     */
    public void setTargetComponent(String compName)
    {
        mTargetComponent = compName;
    }
    
    /**
     * Get the path to the Service Unit archive.
     * @return the file path.
     */
    public String getFilePath()
    {
    	if (mFilePath == null) {
    		try {
    			updateFilePath();
    		} catch (Exception e) {
    			e.printStackTrace();
    		}
        	if (mFilePath == null)
        		return "";
    	}
    	return mFilePath;
    }
    
    /**
     * Find the file path from the Service Unit archive. 
     * Moved this logic from ComponentQueryImpl to here, to improve start up performance 
     * by avoiding unnecessary path set up
     * @param suList - List of ServiceUnitInfos, which need to be updated
     * @param componentName - the target component of the ServiceUnitInfos
     */
    private void updateFilePath() 
    {
    	Registry reg = (Registry)ManagementContext.getEnvironmentContext().getRegistry();
    	String suRelPath = this.getServiceAssemblyName() + java.io.File.separator 
                            + this.getName();
    	String suArchiveDir = reg.getRepository().findArchiveDirectory(
    			                com.sun.jbi.management.repository.ArchiveType.SERVICE_UNIT, suRelPath);
        String filePath = suArchiveDir + java.io.File.separator + this.getTargetComponent();
        this.setFilePath(filePath);
    }
    
    /**
     * @param filePath - the path to the ServiceUnit archive
     */
    public void setFilePath(String filePath)
    {
        mFilePath = filePath;
    }
}
