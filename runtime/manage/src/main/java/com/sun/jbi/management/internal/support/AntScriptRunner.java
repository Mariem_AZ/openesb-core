/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)AntScriptRunner.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.internal.support;

import java.io.File;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.ProjectHelper;
import org.apache.tools.ant.DefaultLogger;
import org.apache.tools.ant.BuildException;
import java.util.logging.Logger;
import java.util.logging.Level;

/**
 * AntScriptRunner is a class that can be used to run
 * an ant task from a specified ant file in a specified directory.
 *
 * @author Sun Microsystems, Inc.
 */
public class AntScriptRunner
{

    /**
     * Run a target in an ant file.
     *
     * @param basedir the ant basedir
     * @param antfn the name of the ant file.
     * @param targetname the target you want to run
     * @return true if ant task is successfully run
     */
    public static boolean
    runAntTarget(String basedir, String antfn, String targetname)
    {
        Project project = new Project();
        boolean taskstatus = true;

        project.setBaseDir( new File(basedir));
        project.init(); 

        File buildFile = new File(antfn);
        ProjectHelper.configureProject( project, buildFile);

        if (targetname == null || targetname.equals(""))
        {
            targetname = project.getDefaultTarget();
        }

        DefaultLogger mylogger = new DefaultLogger();

        /*
        ** this defaults to MSG_ERR
        ** The order of the levels, from least to most verbose:
        **  {MSG_ERR, MSG_WARN, MSG_INFO, MSG_VERBOSE, MSG_DEBUG}
        */
        
        Level logLevel = Level.parse(System.getProperty(
                "com.sun.jbi.defaultLogLevel", "INFO"));

        if (logLevel.intValue() >= Level.INFO.intValue())
        {
            //this is the normal setting:
            mylogger.setMessageOutputLevel(org.apache.tools.ant.Project.MSG_WARN);
        }
        else if (logLevel.intValue() >= Level.FINE.intValue())
        {
            mylogger.setMessageOutputLevel(org.apache.tools.ant.Project.MSG_INFO);
        }
        else
        {
            mylogger.setMessageOutputLevel(org.apache.tools.ant.Project.MSG_VERBOSE);
        }

        //you must set these out, err!!
        mylogger.setOutputPrintStream(System.out);
        mylogger.setErrorPrintStream(System.err);
        mylogger.setEmacsMode(true);

        project.addBuildListener(mylogger); 

        //TODO:  read in a set of properties, and set each one
        project.setUserProperty( "fooprop", "foovalue");

        try
        {
            project.executeTarget(targetname);
        }
        catch (Exception e)
        {
            System.out.println("AntScriptRunner:  ERROR: " + e.getMessage());
            taskstatus = false;
        }

        return taskstatus;
    }
}
