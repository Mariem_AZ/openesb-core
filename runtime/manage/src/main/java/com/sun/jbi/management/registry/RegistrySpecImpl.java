/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)RegistrySpecImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  RegistrySpec.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on July 21, 2005, 5:19 PM
 */

package com.sun.jbi.management.registry;

import com.sun.jbi.management.system.ManagementContext;
import java.util.Properties;

/**
 * A Registry Specification.
 *
 * @author Sun Microsystems, Inc.
 */
public class RegistrySpecImpl
    implements RegistrySpec
{
    /**
     * Registry Type.
     */
    private RegistryType mType = null;
    
    /**
     * Properties.
     */
    private Properties mProps = null;
    
    // -- Environment Context ??
    /**
     * Management Ctx.
     */
    private ManagementContext mMgmtCtx;
    
    /**
     * @param type is the Registry Type.
     * @param props are the registry properties.
     */
    public RegistrySpecImpl (RegistryType type, Properties props, ManagementContext ctx)
    {
        mType = type;
        mProps = props;
        mMgmtCtx = ctx;
    }    
    
    /**
     * @return the type of the Registry.
     */
    public RegistryType getType()
    {
        return mType;
    }
    
    /**
     * @return the Registry properties.
     */
    public Properties getProperties()
    {
        return mProps;
    }
    
    /**
     * @return the ManagementContext
     */
    public ManagementContext getManagementContext()
    {
        return mMgmtCtx;
    }
    
}
