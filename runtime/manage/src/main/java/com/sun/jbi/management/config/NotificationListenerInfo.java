/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DynamicRuntimeConfiguration.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.config;

import javax.management.NotificationListener;
import javax.management.NotificationFilter;

/**
 * This class encapsulates three pieces of information pertinent to a 
 * notification listener
 *   <ul>
 *    <li> The NotificationListener instance </li>
 *    <li> The Notfication Filter for this listener </li>
 *    <li> The callback Object for the Listener </li>
 *   </ul>
 *
 * @author Sun Microsystems, Inc.
 */
public class NotificationListenerInfo 
{
    private NotificationListener mListener;
    private NotificationFilter   mFilter;
    private Object               mCallback;
    
    
    /**
     *
     */
    public NotificationListenerInfo(NotificationListener listener,  
                NotificationFilter filter, Object cb)
    {
        mListener = listener;
        mFilter   = filter;
        mCallback = cb;
    }
    
    /**
     * @return the notification listener instance
     */
    public NotificationListener getNotificationListener()
    {
        return mListener;
    }
    
    /**
     * @return the notification filter instance
     */
    public NotificationFilter getNotificationFilter()
    {
        return mFilter;
    }
    
    /**
     * @return the callback instance
     */
    public Object getHandback()
    {
        return mCallback;
    }
    
    
}
