<?xml version="1.0"?>
<!--
 # BEGIN_HEADER - DO NOT EDIT
 #
 # The contents of this file are subject to the terms
 # of the Common Development and Distribution License
 # (the "License").  You may not use this file except
 # in compliance with the License.
 #
 # You can obtain a copy of the license at
 # https://open-esb.dev.java.net/public/CDDLv1.0.html.
 # See the License for the specific language governing
 # permissions and limitations under the License.
 #
 # When distributing Covered Code, include this CDDL
 # HEADER in each file and include the License file at
 # https://open-esb.dev.java.net/public/CDDLv1.0.html.
 # If applicable add the following below this CDDL HEADER,
 # with the fields enclosed by brackets "[]" replaced with
 # your own identifying information: Portions Copyright
 # [year] [name of copyright owner]
-->

<!--
 # @(#)manage00602.xml
 # Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 #
 # END_HEADER - DO NOT EDIT
-->

<project name="installation tests - jbicalc" default="runtests" basedir="." >

    <import file="inc/testsetup.ant" />
    
    <target name="init" depends="manage_test_init">
        <property name="jmsbinding.name"  value="SunJMSBinding" />
        <property name="xformengine.name" value="SunTransformationEngine" />
        <property name="seqengine.name"   value="SunSequencingEngine" />
        <property name="jmsbinding.path"
                  value="${file.uri}${JBI_HOME}/components/bindings/jmsbinding/installer/jmsbinding.jar" />
        <property name="xformengine.path"
                  value="${file.uri}${JBI_HOME}/components/engines/transformationengine/installer/transformationengine.jar" />
        <property name="seqengine.path"
                  value="${file.uri}${JBI_HOME}/components/engines/sequencingengine/installer/sequencingengine.jar" />
        <property name="xformengine.log"  value="${manage_bld_dir}/xslt_install.log" />
        <property name="seqengine.log"    value="${manage_bld_dir}/seq_install.log" />
        <property name="jmsbinding.log"   value="${manage_bld_dir}/jms_install.log" />
    </target>

    <!-- params:
           o component.path -> required, path to component archive
           o component.name -> required, name of component
     -->
    <target name="install-uninstall-component">
        <echo file="${log.file}" message="install-uninstall of ${component.name} ${line.separator}"/>
        <invokeMBean operation="loadNewInstaller" resultProperty="installerMBean" mbeanref="InstallationService">
            <parameter value="${component.path}"/>
        </invokeMBean>        
        <echo file="${log.file}" append="true" message="installer bean is '${installerMBean}' ${line.separator}"/>
        
        <invokeMBean
                name="${installerMBean}"
                providerUrl="${jmx.provider}"
                serverType="rjmx"
                user="${jmx.user}"
                password="${jmx.password}"
                operation="install"
                resultProperty="installresults"
            />
            
        <echo file="${log.file}" append="true" message="install() result : '${installresults}' ${line.separator}"/>
                
        <invokeMBean
                operation="unloadInstaller"
                resultProperty="unload"
                mbeanref="InstallationService"
            >
            <parameter value="${component.name}"/>
            <parameter value="false"/>
        </invokeMBean>
        
        <invokeMBean
                operation="loadInstaller"
                resultProperty="loadresult"
                mbeanref="InstallationService"
            >
            <parameter value="${component.name}"/>
        </invokeMBean>
                
        <echo file="${log.file}" append="true" message="loadInstaller() result : '${loadresult}' ${line.separator}"/>

        <invokeMBean
                name="${loadresult}"
                operation="uninstall"
                resultProperty="uninstallresults"
                providerUrl="${jmx.provider}"
                serverType="rjmx"
                user="${jmx.user}"
                password="${jmx.password}"
            />
        <echo file="${log.file}" append="true" message="uninstall() result : '${uninstallresults}' ${line.separator}"/>
        
        <invokeMBean
                operation="unloadInstaller"
                resultProperty="unload"
                mbeanref="InstallationService"
            >
            <parameter value="${component.name}"/>
            <parameter value="true"/>
        </invokeMBean>
        
        <echo file="${log.file}" append="true" message="unloadInstaller() result : '${unload}' ${line.separator}"/>
        
    </target>
   
    <target name="install-shared-library">        
        <echo message="installing shared library"/>
        <invokeMBean operation="installSharedLibrary" resultProperty="installResult" mbeanref="InstallationService">
            <parameter value="${soaplibrary.path}"/>
        </invokeMBean> 
        <echo message="result : '${installResult}'"/>
    </target>

    <target name="uninstall-shared-library">
        <echo message="uninstalling shared library"/>
        <invokeMBean operation="uninstallSharedLibrary" resultProperty="uninstallResult" mbeanref="InstallationService">
            <parameter value="${soaplibrary.name}"/>
        </invokeMBean> 
        <echo message="result : '${uninstallResult}'"/>
    </target>

    <target name="runtests" depends="init">
    
        
        <!-- execute the two tasks in parallel -->
<!--  reverting to sequential until glassfish errors can be investigated.  RT 5/17/06
        <parallel>
-->
            <antcall target="install-uninstall-component">
                <param name="component.name" value="${xformengine.name}"/>
                <param name="component.path" value="${xformengine.path}"/>
                <param name="log.file" value="${xformengine.log}"/>
            </antcall>
            <antcall target="install-uninstall-component">
                <param name="component.name" value="${seqengine.name}"/>
                <param name="component.path" value="${seqengine.path}"/>
                <param name="log.file" value="${seqengine.log}"/>
            </antcall>
            <antcall target="install-uninstall-component">
                <param name="component.name" value="${jmsbinding.name}"/>
                <param name="component.path" value="${jmsbinding.path}"/>
                <param name="log.file" value="${jmsbinding.log}"/>
            </antcall>
<!--  reverting to sequential until glassfish errors can be investigated.  RT 5/17/06
        </parallel>
-->
        
        <!-- Dump the output of our installation runs.  This is not done
             inside the parallel task because the order of the messages
             is non-deterministic.
         -->

        <antcall target="print-log">
            <param name="path" value="${xformengine.log}"/>
        </antcall>
        <antcall target="print-log">
            <param name="path" value="${seqengine.log}"/>
        </antcall>
        <antcall target="print-log">
            <param name="path" value="${jmsbinding.log}"/>
        </antcall>
        
        
    </target>
    
    <target name="print-log">
        <loadfile property="content" srcFile="${path}"/>
        <echo message="${content}"/>
    </target>

        
</project>
