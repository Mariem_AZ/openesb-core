/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestVerifier.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package java4ant;

import java.util.Iterator;
import java.util.Set;

import javax.management.MBeanServerConnection;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import javax.management.ObjectName;

import javax.management.openmbean.ArrayType;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeDataSupport;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.OpenType;
import javax.management.openmbean.OpenDataException;
import javax.management.openmbean.SimpleType;
import javax.management.openmbean.TabularData;

/**
 * This class is used to test Application Verifier.
 */
public class TestVerifier
{
    /**  MBean Server Connection */
    private MBeanServerConnection mbns;
    
    /** Result Prefix */
    private static final String RESULT_PREFIX = "##### Result of ";
    
    /** schemaorg_apache_xmlbeans.system properties used by this class */
    private static final String USER     = "jmx.user";
    private static final String PASSWORD = "jmx.password";
    private static final String PROVIDER = "jmx.provider";
    
    private static final String APPLICATION_URL = "application_url";
    private static final String TARGET   = "target";
    private static final String GENERATE_TEMPLATES = "generate_templates";    
    private static final String TEMPLATE_DIR = "template_dir";    
    private static final String INCLUDE_DEPLOY_COMMAND = "include_deploy_command";    
    private static final String APPLICATION_NAME = "application_name";
    private static final String CONFIG_DIR = "config_dir";    

    /**
     * Initialize connection to MBean Server
     */
    public void initMBeanServerConnection()
        throws Exception
    {
        java.util.Map<String, String[]> env = new java.util.HashMap();
        String user = System.getProperty(USER);
        String pass = System.getProperty(PASSWORD);
        String[] credentials = new String[] { user, pass};
        env.put("jmx.remote.credentials", credentials);   
        
        String jmxProvider = System.getProperty(PROVIDER);
        
        JMXConnector connector = JMXConnectorFactory.connect(new JMXServiceURL(jmxProvider), env);

        mbns = connector.getMBeanServerConnection();
    }
    
    /**
     * Verify Application
     */
            
    public CompositeData verifyApplication()
        throws Exception
    {
        String info = "com.sun.jbi:ServiceName=JbiReferenceAdminUiService,ComponentType=System";
        return (CompositeData)
                mbns.invoke( 
                    new ObjectName(info),
                    "verifyApplication",
                    new Object[] {
                        System.getProperty(APPLICATION_URL),
                        System.getProperty(TARGET),
                        Boolean.getBoolean(System.getProperty(GENERATE_TEMPLATES)),
                        System.getProperty(TEMPLATE_DIR),
                        Boolean.getBoolean(System.getProperty(INCLUDE_DEPLOY_COMMAND))},
                    new String[] {
                         "java.lang.String",
                         "java.lang.String",
                         "boolean",
                         "java.lang.String",
                         "boolean"
                    }
               );

    }
    
    /**
     * parse and print the verifier report
     */
    public void printVerifierReport(CompositeData verifierReport)
    {
        String[] attributeNames = {
            "ServiceAssemblyName",
            "ServiceAssemblyDescription",
            "NumServiceUnits",
            "AllComponentsInstalled",
            "MissingComponentsList",
            "EndpointInfo",
            "TemplateZIPID",
        };   
        
        String[] endpointNames = {
            "EndpointName",
            "ServiceUnitName",
            "ComponentName",
            "Status",
            "MissingApplicationVariables",
            "MissingApplicationConfigurations"
        };
        Object[] reportValues = verifierReport.getAll(attributeNames);

        String saName = (String)reportValues[0];
        String saDesc = (String)reportValues[1];
        int numSUs    = ((Integer)reportValues[2]).intValue();
        boolean allComponentsInstalled = ((Boolean)reportValues[3]).booleanValue();
        String[] missingComponents = (String[])reportValues[4];
        CompositeData[] endpointInfo = (CompositeData[])reportValues[5];
        String templateZipID = (String)reportValues[6];
        
        System.out.println("Service Assembly Information");
        System.out.println("-----------------------------");
        System.out.println("Name: " + saName);
        System.out.println("Service Units: " + numSUs);
        System.out.println("Description: " + saDesc);

        
        if (!allComponentsInstalled) 
        {
            StringBuffer missingCompString = new StringBuffer();
            for (int i=0 ; i< missingComponents.length; i++)
            {
                missingCompString.append(" " + missingComponents[i]);
            }
            System.out.println("The following components are not installed:" + 
                    missingCompString.toString());
        }
        System.out.println();        
        for (int i=0; i<endpointInfo.length; i++)
        {
            System.out.println("Endpoint Configuration");
            System.out.println("-----------------------");
            
            Object[] endpointValues = endpointInfo[i].getAll(endpointNames);
            System.out.println("Name: " + (String)endpointValues[0]);
            System.out.println("Service Unit: " + (String)endpointValues[1]);
            System.out.println("Component: " + (String)endpointValues[2]);
            System.out.println("Status: " + (String)endpointValues[3]);
            
            String[] appVars = (String[]) endpointValues[4];
            if (appVars != null && appVars.length > 0)
            {
                System.out.println("Missing Application Variables ");
                for (int j=0; j<appVars.length; j++)
                {
                    System.out.print("\t" + appVars[j]);
                }
                System.out.println();
            }

            String[] appConfigs = (String[]) endpointValues[5];
            if (appConfigs != null && appConfigs.length > 0)
            {
                System.out.println("Missing Application Configurations ");
                for (int j=0; j<appConfigs.length; j++)
                {
                    System.out.print("\t" + appConfigs[j]);
                }
                System.out.println();                
            }            

        }
        
        
        //JavaEEVerificationReport is optional element
        if (verifierReport.containsKey("JavaEEVerificationReport"))
        {
            CompositeData[] javaEEVerifierReports = (CompositeData[])verifierReport.get("JavaEEVerificationReport");

            if (javaEEVerifierReports !=null && javaEEVerifierReports.length >0)
            {
                System.out.println();                
                System.out.println("JavaEE Verifier Reports");
                System.out.println("-----------------------");            
            }
            for (int i=0; i<javaEEVerifierReports.length; i++)
            {
                System.out.println("Service Unit Name: " + javaEEVerifierReports[i].get("ServiceUnitName"));
                TabularData reportTable = (TabularData) javaEEVerifierReports[i].get("JavaEEVerifierReport");
                Set rows = reportTable.keySet();
                for ( Object row : rows )
                {
                    Object[] key = ( (java.util.List) row).toArray();
                    CompositeData compData = reportTable.get(key);
                    System.out.println();
                    printCompositeData(compData,0);
                }

            }
        }
 
    }
    
    /**
     * parse and print the report
     */
    public void printCompositeData(TabularData statsReport)
    {
        Set names = statsReport.keySet();
        for ( Object name : names )
        {
            Object[] key = ( (java.util.List) name).toArray();
            CompositeData compData = statsReport.get(key);
            System.out.println();
            printCompositeData(compData,1);
        }
    }        
    
    /**
     * parse and print the report
     */
    protected void printCompositeData(CompositeData compData, int recursiveLevel)
    {

	String tabT = "";
	for(int i=0; i<recursiveLevel; ++i)
	{
	    tabT = tabT + "\t";
	}
	recursiveLevel++;

	CompositeType compType = compData.getCompositeType();
        Iterator itr = compType.keySet().iterator();
        while (itr.hasNext())
        {
	    String keyName = (String) itr.next();
            OpenType openType = compType.getType(keyName);
            Object itemValue = compData.get(keyName);
            String className = null;
            if (itemValue != null)
            {
                className = itemValue.getClass().getName();
            }
            
            if (openType.isArray())
            {
		System.out.println(tabT + keyName + ":");
                if ((openType.getTypeName() != null) &&
			(openType.getTypeName().compareTo("[Ljava.lang.String;") == 0))
		{
		    String [] strArray = (String []) compData.get(keyName);
		    for (int i=0; i < strArray.length; ++i)
		    {
			System.out.println(tabT+"\t"+strArray[i]);
		    }
		}
		else if ((openType.getTypeName() != null) &&       
                        (openType.getTypeName().compareTo("[Ljavax.management.openmbean.CompositeData;") == 0))
		{
		    CompositeData [] compDataArray = (CompositeData []) compData.get(keyName);
		    for (int i=0; i < compDataArray.length; ++i)
                    {
                        printCompositeData(compDataArray[i], recursiveLevel);
                    }
                }
		    
            }
            else
            {

		if (className != null &&
			(className.equals("javax.management.openmbean.CompositeDataSupport") || 
                        className.equals("javax.management.openmbean.CompositeData")))
		{
		    printCompositeData((CompositeData) compData.get(keyName), recursiveLevel);
		}
                else if (className != null &&
			(className.equals("javax.management.openmbean.TabularDataSupport") || 
                        className.equals("javax.management.openmbean.TabularData")))
                {
                    printCompositeData((TabularData)compData.get(keyName));
                }
                else
                {
                    System.out.println(tabT + keyName + "=" + compData.get(keyName));
                }
            }
        }


    }        
    
    /**
     * Export Application Configuration
     */
    public String exportApplication()
        throws Exception
    {
        String info = "com.sun.jbi:ServiceName=JbiReferenceAdminUiService,ComponentType=System";
        return (String)
                mbns.invoke( 
                    new ObjectName(info),
                    "exportApplicationConfiguration",
                    new Object[] 
                        {
                        System.getProperty(APPLICATION_NAME),
                        System.getProperty(CONFIG_DIR),
                        System.getProperty(TARGET),
                        },
                    new String[] 
                        {
                         "java.lang.String",
                         "java.lang.String",
                         "java.lang.String"
                        }
               );

    }    
    
    /**
     * The main method
     */
    public static void main (String[] params)
        throws Exception 
    {
       
        TestVerifier testVerifier = new TestVerifier();
        testVerifier.initMBeanServerConnection();
        if (params.length ==0)
        {
            throw new Exception("Usage: TestVerifier verify|export");
        }
        if (params[0].equals("verify"))
        {
            testVerifier.printVerifierReport(testVerifier.verifyApplication());
        }
        else
        {
            testVerifier.exportApplication();
        }
    
    }
    
}
