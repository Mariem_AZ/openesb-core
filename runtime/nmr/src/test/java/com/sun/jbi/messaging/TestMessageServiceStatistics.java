/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestMessageServiceStatistics.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.messaging;

/**
 * Tests for MessageServiceStatistics.
 *
 * @author Sun Microsystems, Inc.
 */
public class TestMessageServiceStatistics
    extends junit.framework.TestCase
{
    /**
     * Instance of MessageServiceStatistics.
     */
    private MessageServiceStatistics mStats;

    /**
     * The constructor for this testcase, forwards the test name to
     * the jUnit TestCase base class.
     * @param aTestName String with the name of this test.
     */
    public TestMessageServiceStatistics(String aTestName)
    {
        super(aTestName);
    }

    /**
     * Setup for the test. This creates the MessageServiceStatistics instance
     * and other objects needed for the tests.
     * @throws Exception when set up fails for any reason.
     */
    public void setUp()
        throws Exception
    {
        super.setUp();

        mStats = new MessageServiceStatistics(null, "MessageService");
    }

    /**
     * Cleanup for the test.
     * @throws Exception when tearDown fails for any reason.
     */
    public void tearDown()
        throws Exception
    {
        super.tearDown();
    }

// =============================  test methods ================================

    /**
     * Tests get/setLastRestartTime.
     * @throws Exception if an unexpected error occurs.
     */
    public void testLastRestartTime()
    {
        java.util.Date d = new java.util.Date();
        mStats.setLastRestartTime(d);
        assertEquals("Failure on set/getLastRestartTime: ",
            d, mStats.getLastRestartTime());
    }

    /**
     * Tests get/increment/decrementRegisteredEndpoints.
     * @throws Exception if an unexpected error occurs.
     */
    public void testRegisteredEndpoints()
        throws Exception
    {
        int n = 0;
        assertEquals("Failure on getRegisteredEndpoints: ", n, mStats.getRegisteredEndpoints());
        mStats.incrementRegisteredEndpoints();
        ++n;
        assertEquals("Failure on incrementRegisteredEndpoints: ", n, mStats.getRegisteredEndpoints());
        mStats.decrementRegisteredEndpoints();
        --n;
        assertEquals("Failure on decrementRegisteredEndpoints: ", n, mStats.getRegisteredEndpoints());
    }

    /**
     * Tests get/increment/decrementRegisteredServices.
     * @throws Exception if an unexpected error occurs.
     */
    public void testRegisteredServices()
        throws Exception
    {
        int n = 0;
        assertEquals("Failure on getRegisteredServices: ", n, mStats.getRegisteredServices());
        mStats.incrementRegisteredServices();
        ++n;
        assertEquals("Failure on incrementRegisteredServices: ", n, mStats.getRegisteredServices());
        mStats.decrementRegisteredServices();
        --n;
        assertEquals("Failure on decrementRegisteredServices: ", n, mStats.getRegisteredServices());
    }

    /**
     * Test resetStatistics.
     * @throws Exception if an unexpected error occurs.
     */
    public void testResetStatistics()
    {
        // First, populate all the fields with values. These methods are all
        // tested in other junit tests so they are assumed to work here.

        mStats.incrementRegisteredEndpoints();
        mStats.incrementRegisteredServices();
        mStats.getMessagingStatisticsInstance().incrementActiveExchanges();

        // Now reset all the fields, then check to see if they all got reset.

        mStats.resetStatistics();
        assertEquals("RegisteredEndpoints not reset", 0, mStats.getRegisteredEndpoints());
        assertEquals("RegisteredServices not reset", 0, mStats.getRegisteredServices());
        assertEquals("ActiveExchanges not reset", 0,
            mStats.getMessagingStatisticsInstance().getActiveExchanges());
    }
}
