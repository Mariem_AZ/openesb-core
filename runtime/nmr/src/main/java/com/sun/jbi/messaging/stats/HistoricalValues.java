package com.sun.jbi.messaging.stats;

import java.util.EnumMap;
import java.util.Map;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class HistoricalValues {
    
    private Map<Interval, TimedValue> values = new EnumMap<Interval, TimedValue>(Interval.class);
    
    {
        for (Interval interval : Interval.values()) {
            values.put(interval, new TimedValue());
        }
    }
            
    public void addSample(long sample) {
        for (Map.Entry<Interval, TimedValue> value : values.entrySet()) {
            value.getValue().addSample(sample);
        }
    }
    
    public void zero() {
        for (Map.Entry<Interval, TimedValue> value : values.entrySet()) {
            value.getValue().zero();
        }
    }
    
    public Map<Interval, TimedValue> getValues() {
        return this.values;
    }
    
    public void zeroOutdated() {
        long now = System.currentTimeMillis();
        
        for (Map.Entry<Interval, TimedValue> value : values.entrySet()) {
            long last = value.getValue().getLastTime();
            
            if ((last + value.getKey().getTime()) > now) {
                value.getValue().zero();
            }
        }
    }
    
    public enum Interval {
        GLOBAL(0l),
        MINUTE(60000l),
        HOUR(3600000l),
        DAY(86400000l);
        
        long time;
        
        Interval(long time) {
            this.time = time;
        }

        public long getTime() {
            return time;
        }
    }
}
