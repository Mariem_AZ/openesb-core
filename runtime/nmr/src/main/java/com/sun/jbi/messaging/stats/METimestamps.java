/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)METimestamps.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.messaging.stats;


/** This is used to record a sequence of timestamps and the associated tag.
 *  Used by the NMR performance monitoring schemaorg_apache_xmlbeans.system to capture timings information along
 *  the primary send()->accept() codepaths.
 *
 * @author Sun Microsystems, Inc.
 */
public class METimestamps
{
    private static final int    STAMP_COUNT= 12;
               
    private int                 mIndex;
    private long                mStamps[];
    private byte                mTags[];
    public long                 mConsumerTime;
    public long                 mProviderTime;
    public long                 mNMRTime;
    public long                 mConsumerChannelTime;
    public long                 mProviderChannelTime;
    public long                 mResponseTime;
    public long                 mStatusTime;
    
    private static String       mStrings[] = { "???" , "Csend", "Pqueue", "Paccept", "Psend", "Cqueue", "Caccept" };
    public static final byte   TAG_CSEND=1;
    public static final byte   TAG_PQUEUE=2;
    public static final byte   TAG_PACCEPT=3;
    public static final byte   TAG_PSEND=4;
    public static final byte   TAG_CQUEUE=5;
    public static final byte   TAG_CACCEPT=6;
                
                
    public METimestamps()
    {
        mStamps = new long[STAMP_COUNT + 1];
        mTags = new byte[STAMP_COUNT + 1];
    }
    
    public void capture(byte tag)
    {
        if (mIndex < STAMP_COUNT)
        {
            mStamps[mIndex] = System.nanoTime();
            mTags[mIndex] = tag;
            mIndex++;
        }
    }
    
    public String toString()
    {
        StringBuffer        sb = new StringBuffer();
        if (mStamps != null)
        {
            sb.append("Timestamps(" + mIndex + "): ");
            for (int i = 0 ; i < mIndex; i++)
            {
                sb.append(mStrings[(mTags[i] <= TAG_CACCEPT) ? mTags[i] : 0]);
                sb.append("(");
                sb.append(i == 0 ? mStamps[0] : mStamps[i] - mStamps[0]);
                sb.append(")" );
            }
        }
        else
        {
            sb.append("Timing: Consumer: " + mConsumerTime);
            sb.append(" +Channel: " + mConsumerChannelTime);
            sb.append(" Provider: " + mProviderTime);
            sb.append(" +Channel: " + mProviderChannelTime);
            sb.append(" NMR: " + mNMRTime);
            sb.append(" Response: " + mResponseTime);
            sb.append(" Status: " + mStatusTime);
        }
        
        return (sb.toString());
    }
    
    public void compute()
    {
        int     statusStart = 0;
        boolean response = false;
        
        for (int i = 0; i < mIndex; i++)
        {
            if ((mTags[i] == TAG_CSEND && mTags[i+1] == TAG_PQUEUE) ||
                (mTags[i] == TAG_PSEND && mTags[i+1] == TAG_CQUEUE))
            {
                mNMRTime += mStamps[i+1] - mStamps[i];
            }
            else if (mTags[i] == TAG_PQUEUE && mTags[i+1] == TAG_PACCEPT)
            {
                mProviderChannelTime += mStamps[i+1] - mStamps[i];
            }
            else if (mTags[i] == TAG_PACCEPT)
            {
                if (mTags[i+1] == TAG_PSEND)
                {
                    mProviderTime += mStamps[i+1] - mStamps[i];
                }
                if (statusStart != 0)
                {
                    mStatusTime = mStamps[i] - mStamps[statusStart];
                }
            }
            else if (mTags[i] == TAG_CACCEPT)
            {
                if (mTags[i+1] == TAG_CSEND)
                {
                    mConsumerTime += mStamps[i+1] - mStamps[i];
                }
                if (!response)
                {
                    mResponseTime += mStamps[i] - mStamps[0];
                    response = true;
                }
            }
            else if (mTags[i] == TAG_CQUEUE && mTags[i+1] == TAG_CACCEPT)
            {
                mConsumerChannelTime += mStamps[i+1] - mStamps[i];
            }
            if (mTags[i] == TAG_PSEND)
            {
                statusStart = i;
            }
        }
        mTags = null;
        mStamps = null;        
    }
}

    
