/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ScaffoldedEnvironmentContext.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  SampleEnvironmentContext.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on December 19, 2004, 12:48 PM
 */

package com.sun.jbi.management.system;

import com.sun.jbi.JBIProvider;
import com.sun.jbi.management.MBeanNames;
import com.sun.jbi.management.support.MBeanNamesImpl;

import java.util.Properties;
import javax.management.MBeanServer;
import javax.management.MBeanServerFactory;

/**
 *
 * @author Sun Microsystems, Inc.
 */
public class ScaffoldedEnvironmentContext implements com.sun.jbi.EnvironmentContext
{
    /** App Server Install Root. */
    private String mAsInstRoot;
    private String mJbiInstallRoot;
    private String mJbiInstanceRoot;
    private MBeanNames  mMBeanNames;
    private MBeanServer mMBeanServer;
    private Properties  mProps;
    private ScaffoldPlatformContext mPlatform;
    
    /** Creates a new instance of SampleEnvironmentContext */
    public ScaffoldedEnvironmentContext ()
    {
        mMBeanNames = new com.sun.jbi.management.support.MBeanNamesImpl(
            "com.sun.jbi", "NULL_INSTANCE");
        
        mPlatform = new ScaffoldPlatformContext();
        mMBeanServer = java.lang.management.ManagementFactory.getPlatformMBeanServer();
    }

    /** Creates a new instance of SampleEnvironmentContext */
    public ScaffoldedEnvironmentContext (String jbiInstallRoot)
        throws Exception
    {
        mMBeanNames = new com.sun.jbi.management.support.MBeanNamesImpl(
            "com.sun.jbi", "NULL_INSTANCE");
        
        mMBeanServer = java.lang.management.ManagementFactory.getPlatformMBeanServer();
        setJbiInstanceRoot(jbiInstallRoot);
        setAppServerInstanceRoot(jbiInstallRoot);
        setProperty("com.sun.jbi.home", jbiInstallRoot);
        createRegistry();
        
    }
    
    
    /**
     * Get the platform-specific context for this implementation.
     * @return The PlatformContext.
     */
    public com.sun.jbi.platform.PlatformContext getPlatformContext()
    {
        return mPlatform;
    }

    
    /**
     * Get a reference to the persisted JBI registry.
     * @return Registry instance
     */
    public com.sun.jbi.registry.Registry getRegistry()
    {
        return null;
    }
    
    /**
     * Get a read-only reference to the persisted JBI Registry. A DOM registry document 
     * object is returned. 
     * @return the registry document
     */
    public org.w3c.dom.Document getReadOnlyRegistry()
    {
        // return null, if the DOM registry is needed then more stuff goes here
        return null;
    }
    
    /**
     * Indicates whether or not the JBI framework has been fully started.  This
     * method provides clients with a way of determining if the JBI framework
     * started up in passive mode as a result of on-demand initialization.  
     * The 'start' parameter instructs the framework to
     * start completely if it has not already done so.  If the framework has
     * already been started, the request to start again is ignored.
     * @param start requests that the framework start completely before
     *  returning.
     * @return true if the framework is completely started, false otherwise.
     */
    public boolean isFrameworkReady(boolean start)
    {
        return true;
    }
    
    public JBIProvider getProvider()
    {
        return JBIProvider.OTHER;
    }
    
    public String getAppServerInstallRoot()
    {
        return mAsInstRoot;
    }
    
    public void setAppServerInstallRoot(String root)
    {
        mAsInstRoot = root;
    }
    
    public String getAppServerInstanceRoot()
    {
        return mAsInstRoot;
    }
    
    public void setAppServerInstanceRoot(String root)
    {
        mAsInstRoot = root;
    }
    
    public com.sun.jbi.ComponentManager getComponentManager()
    {
        return null;
    }
    
    public String getComponentId ()
    {
        return "";
    }
    
    public String getComponentName ()
    {
        return "notNeeded";
    }
    
    public com.sun.jbi.ComponentQuery getComponentQuery ()
    {
        return null;
    }
    
    public com.sun.jbi.ComponentQuery getComponentQuery (String targetName)
    {
        return null;
    }
    
    public String getComponentRoot ()
    {
        return null;
    }
    
    public com.sun.jbi.messaging.ConnectionManager getConnectionManager()
    {
        return null;
    }

    public java.util.Properties getInitialProperties()
    {
       if ( mProps == null )
       {
           mProps = new Properties();
       }
       return mProps;
    }
    
    public void setProperty(String key, String value)
    {
        getInitialProperties().setProperty(key, value);
    }
    
    public String getJbiInstallRoot ()
    {
        return mJbiInstallRoot;
    }
    
    public void setJbiInstallRoot(String jbiRoot)
    {
        mJbiInstallRoot = jbiRoot;
    }
    
    public String getJbiInstanceRoot ()
    {
        return mJbiInstanceRoot;
    }
    
    public void setJbiInstanceRoot(String jbiRoot)
    {
        mJbiInstanceRoot = jbiRoot;
    }
    
    public com.sun.jbi.management.MBeanHelper getMBeanHelper ()
    {
        return null;
    }
    
    public com.sun.jbi.management.MBeanNames getMBeanNames ()
    {
        return mMBeanNames;
    }
    
    public javax.management.MBeanServer getMBeanServer ()
    {
        return mMBeanServer;
    }
    
    public Object getManagementClass (String aServiceName)
    {
        return null;
    }
    
    public com.sun.jbi.management.ManagementMessageFactory getManagementMessageFactory ()
    {
        return null;
    }
    
    public javax.naming.InitialContext getNamingContext ()
    {
        return null;
    }

    /**
     * Get the Event Notifier instance.
     * @return The EventNotifer instance.
     */
    public com.sun.jbi.framework.EventNotifierCommon getNotifier()
    {
        return null;
    }
    
    public com.sun.jbi.ServiceUnitRegistration getServiceUnitRegistration()
    {
        return null;
    }
    
    public com.sun.jbi.StringTranslator getStringTranslator (String packageName)
    {
        return Util.getStringTranslator(packageName);
    }
    
    public com.sun.jbi.StringTranslator getStringTranslatorFor (Object object)
    {
        return Util.getStringTranslator(object.getClass().getPackage().getName());
    }
    
    /**
     * Get the VersionInfo for this runtime.
     * @return The VersionInfo instance.
     */
    public com.sun.jbi.VersionInfo getVersionInfo()
    {
        return (com.sun.jbi.VersionInfo) this;
    }

    /**
     * Get the Jbi Init Time.
     * @return Jbi Init Time
     */
    public long getJbiInitTime()
    {
     	return System.currentTimeMillis();
    }

    /**
     * Get a TransactionManager from the AppServer.
     * @return A TransactionManager instance.
     */
    public javax.transaction.TransactionManager getTransactionManager()
    {
        return null;
    }

    /**
     * Gets  a Wsdl factory. 
     * @return wsdl factory.
     */

    public com.sun.jbi.wsdl2.WsdlFactory getWsdlFactory()
	throws com.sun.jbi.wsdl2.WsdlException
    {
       return null;
    }
    
       /**
     * Create a Registry Instance.
     *
     * @return a Registry Instance
     * @throws Exception if an unexpected error occurs
     */
    public  com.sun.jbi.management.registry.Registry 
    createRegistry()
           throws Exception
    {
        com.sun.jbi.management.registry.Registry registry = null;
        try 
        {
            String regFolder = getJbiInstanceRoot() + java.io.File.separator + "config";
            
            // -- Create a XML Registry Spec.
            java.util.Properties props = new java.util.Properties();
            props.setProperty(com.sun.jbi.management.registry.Registry.REGISTRY_FOLDER_PROPERTY, regFolder );
            
            com.sun.jbi.management.system.ManagementContext 
                mgtCtx = new com.sun.jbi.management.system.ManagementContext(this);
            
            com.sun.jbi.management.repository.Repository 
                repository = new com.sun.jbi.management.repository.Repository(mgtCtx);
        
            mgtCtx.setRepository(repository);
            registry = 
                com.sun.jbi.management.registry.RegistryBuilder.buildRegistry( new com.sun.jbi.management.registry.RegistrySpecImpl(
                com.sun.jbi.management.registry.RegistryType.XML, props, mgtCtx));
            
            java.util.List<String> servers = registry.getGenericQuery().getServers();
            if ( !servers.contains("server") )
            {
                registry.getUpdater().addServer("server");
            }
            
        }
        catch (Exception aEx)
        {
            aEx.printStackTrace();
            throw aEx;
        }
        return registry;
    }
    
    /**
     * This method is used to scaffold EnvironmentContext.isStartOnDeployEnabled()
     * and always returns true
     */
    public boolean isStartOnDeployEnabled()
    {
        return true;
    }    
    
    /**
     * This method is used to find out if start-onverify is enabled.
     * When this is enabled components are started automatically when 
     * an application has to be verified for them 
     * This is controlled by the property com.sun.jbi.startOnVerify.
     * By default start-on-verify is enabled. 
     * It is disabled only if com.sun.jbi.startOnVerify=false.
     */
    public boolean isStartOnVerifyEnabled()
    {
        return true;
    }         
    
}
