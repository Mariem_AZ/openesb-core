/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ManagementRuntimeService.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.facade;

import com.sun.esb.eventmanagement.impl.EventForwarderMBean;
import com.sun.esb.eventmanagement.impl.EventManagementControllerMBean;
import com.sun.jbi.*;
import com.sun.jbi.management.ConfigurationCategory;
import com.sun.jbi.management.EventNotifierMBean;
import com.sun.jbi.management.ExtensionService;
import com.sun.jbi.management.MBeanNames;
import com.sun.jbi.management.config.ConfigurationBuilder;
import com.sun.jbi.management.config.ConfigurationFactory;
import com.sun.jbi.management.internal.support.AntScriptRunner;
import com.sun.jbi.management.registry.GenericQuery;
import com.sun.jbi.management.registry.Registry;
import com.sun.jbi.management.registry.data.ComponentInfoImpl;
import com.sun.jbi.management.repository.*;
import com.sun.jbi.management.system.ManagementContext;
import com.sun.jbi.management.util.FacadeMbeanHelper;
import com.sun.jbi.platform.PlatformContext;
import com.sun.jbi.ui.common.JBIJMXObjectNames;
import com.sun.jbi.ui.runtime.mbeans.JBIAdminCommandsUIMBean;
import com.sun.jbi.ui.runtime.mbeans.JBIStatisticsMBean;
import com.sun.jbi.ui.runtime.mbeans.UIMBeanFactory;
import java.util.logging.Level;

import javax.jbi.JBIException;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.management.StandardMBean;
import javax.management.modelmbean.ModelMBeanInfo;
import javax.management.modelmbean.RequiredModelMBean;
import java.io.File;
import java.util.*;
import java.util.logging.Logger;

//import com.sun.jbi.ui.runtime.mbeans.JBIAdminCommandsUIBackPortMBean;

/**
 * This schemaorg_apache_xmlbeans.system service is responsible for bootstrapping the Management Facade MBeans
 * for the integrated JBI Runtime.
 *
 * The JBI Framework invokes the (init/start/stop)Service operations in sync. with the
 * init/start/stop of the Domain Application Server.
 *
 *
 * @author  Sun Microsystems, Inc.
 */
public class ManagementRuntimeService
    implements ServiceLifecycle, com.sun.jbi.platform.PlatformEventListener,
               com.sun.jbi.util.Constants
{
    // Old name for ArchiveUploadMBean that is still used by NB EP 5.5 clients
    private static final String ESB_UPLOAD_MBEAN_NAME =
            "com.sun.jbi.esb:ServiceType=ArchiveUpload";

    private EnvironmentContext mEnvCtx;
    private MBeanServer        mMBeanSvr;
    private Logger             mLog;
    private MBeanNames         mMBeanNames;
    private ManagementContext  mMgtCtx;
    private StringTranslator   mTranslator;
    private PlatformContext    mPlatform;
    private EventNotifier      mNotifier;

    /** Registered Configuration MBean service types */
    private Set<MBeanNames.ServiceType> mConfigSvcTypes;

    /** Default Properties */
    private Properties         mDefaults;

    /** The default configuration file name */
    private static final String DEFAULT_CONFIG_FILE = "runtime-config.defaults";

    /** Dynamic Service loader */

   private ServiceLoader<ExtensionService> loader;





    /** Creates a new instance of ManagementRuntimeService */
    public ManagementRuntimeService()
    {
        mLog = Logger.getLogger("com.sun.jbi.management");
    }

    /**
     * Initialize the Service
     *
     * @param aContext contex
     * @throws JBIException on error
     */
    public void initService(EnvironmentContext aContext) throws JBIException
    {
        mLog.finest("Initializing Management Runtime Service.");
        mEnvCtx = aContext;
        mMgtCtx = new ManagementContext(mEnvCtx);
        mPlatform = mEnvCtx.getPlatformContext();
        mMBeanSvr = mEnvCtx.getMBeanServer();
        mMBeanNames = mEnvCtx.getMBeanNames();
        mTranslator = mEnvCtx.getStringTranslator("com.sun.jbi.management.facade");

        mLog.log(Level.FINEST, " This is target {0}", mPlatform.getTargetName());
        mPlatform.addListener(this);

        try
        {
            if (mPlatform.isAdminServer())
            {
                registerUploadDownloadMBeans("domain");
                registerDomainFacadeMBeans();
                registerInstanceFacadeMBeans();
                registerClusterFacadeMBeans();
                registerJavaCAPSManagementServiceMBeans();
                registerJbiAdminUiMBeans();
//                registerJbiAdminUiBackPortMBeans();
                createClientConfigFiles();
                // register event management controller and forwarder mbeans
                // must be registered in this order to insure persistence
                // configuration properly set.
                registerEventManagementControllerMbean();
                registerEventManagementForwarderMbean();
            }
            else
            {
                registerUploadDownloadMBeans(mPlatform.getInstanceName());                    
                // on non admin server register only event management forwarder mbean.
                registerEventManagementForwarderMbean();
            }
            
            initExtensionServices();
            //registerRuntimeConfigMBeans();
        }
        catch(Exception ex)
        {
            throw new JBIException(ex);
        }
    }

    /**
     * Service startup. Load all the JSR 208 Facade MBeans here.
     *
     * @throws JBIException on error
     */
    public void startService() throws JBIException
    {
        mLog.finest("Starting Management Runtime Service.");

        updateRegistry();

        try
        {
            if ( mPlatform.isAdminServer() )
            {
                //Register facade MBeans for all installed components
                registerComponentFacadeMBeans();
            }
            
            startExtensionServices();
        }
        catch(Exception ex)
        {
            throw new JBIException(ex);
        }

    }

    /**
     * Service stopped. Unregister the JSR 208 Facade MBeans here.
     *
     * @throws JBIException on error
     */
    public void stopService() throws JBIException
    {
        mLog.finest("Stopping Management Runtime Service.");
        try
        {
            if ( mPlatform.isAdminServer() )
            {
                mNotifier.stop();
//                unregisterJbiAdminUiBackPortMBeans();
                unregisterJbiAdminUiMBeans();
                unregisterJavaCAPSManagementServiceMBeans();
                unregisterClusterFacadeMBeans();
                unregisterInstanceFacadeMBeans();
                unregisterDomainFacadeMBeans();
                unregisterUploadDownloadMBeans("domain");                
                unregisterEventManagementForwarderMbean();            
                unregisterEventManagementControllerMbean();
                stopExtensionServices();
            }
            else
            {
                unregisterUploadDownloadMBeans(mPlatform.getInstanceName());
                unregisterEventManagementForwarderMbean();
            }
            //unregisterRuntimeConfigMBeans();
        }
        catch (Exception ex)
        {
            throw new JBIException(ex);
        }
    }


    /*--------------------------------------------------------------------------------*\
     *                       Register all the MBeans                                  *
    \*--------------------------------------------------------------------------------*/


    /**
     * Register the 'domain' facade MBeans
     */
    private void registerDomainFacadeMBeans()
        throws Exception
    {
        String domain = "domain";
        
//        if (mPlatform.supportsMultipleServers())
        {
            mLog.finest("Loading Facade MBeans");
            // -- Instantiate the MBeans
            // -- Use a factory to create the MBeans for a given target
            com.sun.jbi.management.InstallationServiceMBean instSvc = new InstallationService(mEnvCtx, domain);
            com.sun.jbi.management.DeploymentServiceMBean dplySvc   = new DeploymentService(mEnvCtx, domain);
            com.sun.jbi.management.AdminServiceMBean  adminSvc      = new AdminService(mEnvCtx, domain);

            StandardMBean instStd =  new StandardMBean(instSvc,  com.sun.jbi.management.InstallationServiceMBean.class);
            StandardMBean dplyStd =  new StandardMBean(dplySvc,  com.sun.jbi.management.DeploymentServiceMBean.class);
            StandardMBean adminStd = new StandardMBean(adminSvc, com.sun.jbi.management.AdminServiceMBean.class);

            // -- Register them
            mMBeanSvr.registerMBean(adminStd,
                mMBeanNames.getSystemServiceMBeanName(
                    MBeanNames.ServiceName.AdminService,
                    MBeanNames.ServiceType.Admin, domain));
            mMBeanSvr.registerMBean(instStd,
                mMBeanNames.getSystemServiceMBeanName(
                    MBeanNames.ServiceName.InstallationService,
                    MBeanNames.ServiceType.Installation, domain));
            mMBeanSvr.registerMBean(dplyStd,
                mMBeanNames.getSystemServiceMBeanName(
                    MBeanNames.ServiceName.DeploymentService,
                    MBeanNames.ServiceType.Deployment, domain));
        }

        //
        //  Register the event notifier for the domain.
        //
        EventNotifierMBean notifier      = new EventNotifier(mEnvCtx, domain);
        StandardMBean notifierStd = new StandardMBean(notifier,
            com.sun.jbi.management.EventNotifierMBean.class);
        mMBeanSvr.registerMBean(notifierStd,
            mMBeanNames.getSystemServiceMBeanName(
                MBeanNames.ServiceName.Framework,
                MBeanNames.ServiceType.Notification, domain));
        //
        // Call the event notifier to register itself to receive notifications
        // from all active instances (this MUST be done after the event notifier
        // MBean has been registered with the MBean server)
        //
        mNotifier = (EventNotifier) notifier;
        mNotifier.start();
        
        //
        //  These are needed because any instance inherits from the domain.
        //
        registerRuntimeConfigFacadeMBeansForTarget(domain);
    }

    /**
     * Register the Upload/Download MBeans
     */
    private void registerUploadDownloadMBeans(String target)
        throws Exception
    {
        mLog.finest("Loading Upload / Download MBeans");
        // -- Instantiate the MBeans
        // -- Use a factory to create the MBeans for a given target

        ArchiveUploadMBean   uploadSvc          = new ArchiveUpload(mMgtCtx);
        ArchiveDownloadMBean downloadSvc        = new ArchiveDownload(mMgtCtx);


        StandardMBean uploadStd   = new StandardMBean(uploadSvc,   ArchiveUploadMBean.class);
        StandardMBean downloadStd = new StandardMBean(downloadSvc, ArchiveDownloadMBean.class);

        // -- Register them
        mMBeanSvr.registerMBean(uploadStd,
            mMBeanNames.getSystemServiceMBeanName(
                MBeanNames.ServiceName.FileTransferService,
                MBeanNames.ServiceType.Upload, target));
        mMBeanSvr.registerMBean(downloadStd,
                mMBeanNames.getSystemServiceMBeanName(
                MBeanNames.ServiceName.FileTransferService,
                MBeanNames.ServiceType.Download, target));

//        // Provide backwards compatibility for NB EP 5.5 clients which
//        // depend on the old com.sun.esb: UploadMBean.
//        if (target.equals("domain"))
//        {
//            mMBeanSvr.registerMBean(uploadStd,
//                    new ObjectName(ESB_UPLOAD_MBEAN_NAME));
//        }
    }

    /**
     * Case to be considered : When the remote instance is down when the
     * ManagementRuntimeService starts up, the facade MBeans cannot be registered for the
     * instance. Need a notification mechanism when an instance is started so
     * we can register all the MBeans for the instance.
     */

    /**
     * Register all the Instance facade MBeans
     */
    private void registerInstanceFacadeMBeans()
        throws Exception
    {

        mLog.finest("Loading Instance Facade MBeans");

        Set<String> instances = mPlatform.getStandaloneServerNames();

        for ( String instance : instances )
        {
            registerFacadeMBeansForTarget(instance);
        }
    }

    /**
     * Register all the Cluster facade MBeans
     */
    private void registerClusterFacadeMBeans()
        throws Exception
    {
        if (mPlatform.supportsMultipleServers())
        {
            mLog.finest("Loading Cluster Facade MBeans");

            Set<String> clusters = mPlatform.getClusterNames();

            for ( String cluster : clusters )
            {
                registerFacadeMBeansForTarget(cluster);
            }
        }
    }

    /**
     * Register the ComponentLifeCycle and ComponentExtension facade MBeans
     * for components on all targets.
     */
    private void registerComponentFacadeMBeans()
        throws Exception
    {
        // Domain-level component MBeans
        registerComponentLifeCycleFacadeMBeansForTarget("domain");
        registerComponentExtensionFacadeMBeansForTarget("domain");

        // Instance-level component MBeans
        mLog.finest("Loading Instance ComponentLifeCycle Facade MBeans");
        for ( String target : mPlatform.getStandaloneServerNames() )
        {
            registerComponentLifeCycleFacadeMBeansForTarget(target);
            registerComponentExtensionFacadeMBeansForTarget(target);
            registerComponentConfigurationFacadeMBeansForTarget(target);
        }

        if (!mPlatform.isAdminServer() || mPlatform.supportsMultipleServers())
        {
            // Cluster-level component MBeans
            mLog.finest("Loading Cluster ComponentLifecycle Facade MBeans");
            for ( String target : mPlatform.getClusterNames() )
            {
                registerComponentLifeCycleFacadeMBeansForTarget(target);
                registerComponentExtensionFacadeMBeansForTarget(target);
                registerComponentConfigurationFacadeMBeansForTarget(target);

                // Register a component configuration facade MBean for each
                // instance in the cluster
                Set<String> clusterInstances =
                        mPlatform.getServersInCluster(target);

                for ( String clusterInstance : clusterInstances)
                {
                    registerComponentConfigurationFacadeMBeansForClusterInstance(clusterInstance);
                }
            }
        }
    }

    /**
     * Register the facade MBeans for a target
     */
    private void registerFacadeMBeansForTarget(String target)
        throws Exception
    {
        ObjectName adminSvcObjName = mMBeanNames.getSystemServiceMBeanName(
            MBeanNames.ServiceName.AdminService, MBeanNames.ServiceType.Admin, target);
        ObjectName instSvcObjName = mMBeanNames.getSystemServiceMBeanName(
            MBeanNames.ServiceName.InstallationService, MBeanNames.ServiceType.Installation, target);
        ObjectName dplySvcObjName = mMBeanNames.getSystemServiceMBeanName(
            MBeanNames.ServiceName.DeploymentService, MBeanNames.ServiceType.Deployment, target);

        if ( !mMBeanSvr.isRegistered(adminSvcObjName) )
        {
            com.sun.jbi.management.AdminServiceMBean  adminSvc      = new AdminService(mEnvCtx, target);
            StandardMBean adminStd = new StandardMBean(adminSvc,
                com.sun.jbi.management.AdminServiceMBean.class);
            mMBeanSvr.registerMBean(adminStd, adminSvcObjName);
        }

        if (!mMBeanSvr.isRegistered(instSvcObjName))
        {
            com.sun.jbi.management.InstallationServiceMBean instSvc = new InstallationService(mEnvCtx, target);
            StandardMBean instStd = new StandardMBean(instSvc,
                com.sun.jbi.management.InstallationServiceMBean.class);
            mMBeanSvr.registerMBean(instStd, instSvcObjName);
        }


        if (!mMBeanSvr.isRegistered(dplySvcObjName))
        {
            com.sun.jbi.management.DeploymentServiceMBean dplySvc   = new DeploymentService(mEnvCtx, target);
            StandardMBean dplyStd = new StandardMBean(dplySvc,
                com.sun.jbi.management.DeploymentServiceMBean.class);
            mMBeanSvr.registerMBean(dplyStd, dplySvcObjName);
        }

        registerRuntimeConfigFacadeMBeansForTarget(target);
    }

    /**
     * Unregister the 'domain' Facade MBeans.
     */
    private void unregisterDomainFacadeMBeans()
        throws Exception
    {
        String domain = "domain";
        
//        if (mPlatform.supportsMultipleServers())
        {
            mLog.finest("Unloading domain facade MBeans");

            // -- Unregister the ComponentLifeCycle MBeans registered for target=domain
            unregisterComponentLifeCycleFacadeMBeansForTarget(domain);

            // -- Unregister the ComponentExtension MBeans for all registered components
            unregisterComponentExtensionFacadeMBeansForTarget(domain);

            // -- Unregister them
            mMBeanSvr.unregisterMBean(
                mMBeanNames.getSystemServiceMBeanName(
                    MBeanNames.ServiceName.AdminService,
                    MBeanNames.ServiceType.Admin,
                    domain));
            mMBeanSvr.unregisterMBean(
                mMBeanNames.getSystemServiceMBeanName(
                    MBeanNames.ServiceName.InstallationService,
                    MBeanNames.ServiceType.Installation,
                    domain));
            mMBeanSvr.unregisterMBean(
                mMBeanNames.getSystemServiceMBeanName(
                    MBeanNames.ServiceName.DeploymentService,
                    MBeanNames.ServiceType.Deployment,
                    domain));
        }
        
        mMBeanSvr.unregisterMBean(
            mMBeanNames.getSystemServiceMBeanName(
                MBeanNames.ServiceName.Framework,
                MBeanNames.ServiceType.Notification,
                domain));

        unregisterRuntimeConfigFacadeMBeansForTarget(domain);
    }

        /**
     * Unregister the Upload/Download MBeans
     */
    private void unregisterUploadDownloadMBeans(String target)
        throws Exception
    {
        mLog.finest("Unloading Upload / Download MBeans");

        mMBeanSvr.unregisterMBean(
            mMBeanNames.getSystemServiceMBeanName(
                MBeanNames.ServiceName.FileTransferService,
                MBeanNames.ServiceType.Upload, target));
        mMBeanSvr.unregisterMBean(
                mMBeanNames.getSystemServiceMBeanName(
                MBeanNames.ServiceName.FileTransferService,
                MBeanNames.ServiceType.Download, target));

//        // Provide backwards compatibility for NB EP 5.5 clients which
//        // depend on the old com.sun.esb: UploadMBean.
//        if (target.equals("domain"))
//        {
//            mMBeanSvr.unregisterMBean(new ObjectName(ESB_UPLOAD_MBEAN_NAME));
//        }
    }

    /**
     * Unregister all the Instance Facade MBeans.
     */
    private void unregisterInstanceFacadeMBeans()
        throws Exception
    {


        mLog.finest("Unloading instance facade MBeans");

        Set<String> instances = mPlatform.getStandaloneServerNames();

        for ( String instance : instances )
        {
            unregisterFacadeMBeansForTarget(instance);
        }
    }

    /**
     * Unregister all the Cluster Facade MBeans.
     */
    private void unregisterClusterFacadeMBeans()
        throws Exception
    {
       if (mPlatform.supportsMultipleServers())
       {  
            mLog.finest("Unloading cluster facade MBeans");

            Set<String> clusters = mPlatform.getClusterNames();

            for ( String cluster : clusters )
            {
                unregisterFacadeMBeansForTarget(cluster);
            }
       }
    }

    /**
     * De-register Facade MBeans for a target.
     */
    private void unregisterFacadeMBeansForTarget(String target)
        throws Exception
    {
            // -- Unregister the ComponentLifeCycle MBeans registered in the targets MBean Server
            unregisterComponentLifeCycleFacadeMBeansForTarget(target);

            // -- Unregister the ComponentExtension MBeans for all registered components
            unregisterComponentExtensionFacadeMBeansForTarget(target);

            // -- Unregister the Component Configuration MBeans for all registered components
            unregisterComponentConfigurationFacadeMBeansForTarget(target);

            // -- Unregister the cluster instance Component Configuration MBeans for all registered components
            if ( mPlatform.isCluster(target) )
            {
                Set<String> clusterInstances =
                        mPlatform.getServersInCluster(target);

                for ( String clusterInstance : clusterInstances)
                {
                    unregisterComponentConfigurationFacadeMBeansForClusterInstance(clusterInstance);
                }
            }

            // -- Unregister them
            mMBeanSvr.unregisterMBean(
                mMBeanNames.getSystemServiceMBeanName(
                    MBeanNames.ServiceName.InstallationService,
                    MBeanNames.ServiceType.Installation,
                    target));
            mMBeanSvr.unregisterMBean(
                mMBeanNames.getSystemServiceMBeanName(
                    MBeanNames.ServiceName.DeploymentService,
                    MBeanNames.ServiceType.Deployment,
                    target));
            mMBeanSvr.unregisterMBean(
                mMBeanNames.getSystemServiceMBeanName(
                    MBeanNames.ServiceName.AdminService,
                    MBeanNames.ServiceType.Admin,
                    target));

            // -- Unregister the facade MBeans for a target
            unregisterRuntimeConfigFacadeMBeansForTarget(target);
    }

    /**
     * Registers the UI MBean
     * @throws JBIException on error
     */
    private void
    registerJbiAdminUiMBeans() throws JBIException
    {
        try
        {
            JBIAdminCommandsUIMBean jbiAdminCommandsUIMBeanImpl =
                createJBIAdminCommandsUIMBean(mEnvCtx);

            StandardMBean mbean = new StandardMBean(jbiAdminCommandsUIMBeanImpl,
                JBIAdminCommandsUIMBean.class);

            mMBeanSvr.registerMBean(mbean,
                JBIJMXObjectNames.getJbiReferenceAdminUiMBeanObjectName() );

            //register statistics mbean
            JBIStatisticsMBean jbiStatisticsMBeanImpl =
                    createJBIStatisticsMBean(mEnvCtx);

            StandardMBean statisticsMBean = new StandardMBean(
                    jbiStatisticsMBeanImpl,
                    JBIStatisticsMBean.class);

            mMBeanSvr.registerMBean(
                    statisticsMBean,
                    JBIJMXObjectNames.getJbiStatisticsMBeanObjectName() );

        }
        catch (Exception ex)
        {

            throw new JBIException(ex);
        }
    }

//    /**
//     * Registers the UI Back Port MBean
//     * @throws JBIException on error
//     */
//    private void
//    registerJbiAdminUiBackPortMBeans() throws JBIException
//    {
//        try
//        {
//            JBIAdminCommandsUIBackPortMBean jbiAdminCommandsUIBackPortMBeanImpl =
//                createJBIAdminCommandsUIBackPortMBean(mEnvCtx);
//
//            StandardMBean mbean = new StandardMBean(jbiAdminCommandsUIBackPortMBeanImpl,
//                JBIAdminCommandsUIBackPortMBean.class);
//
//            mMBeanSvr.registerMBean(mbean,
//                JBIJMXObjectNames.getJbiAdminUiMBeanObjectName() );
//        }
//        catch (Exception ex)
//        {
//            throw new JBIException(ex);
//        }
//    }

    /**
     * Registers the Java CAPS Common Management Service MBeans
     * @throws JBIException on error
     */
    private void
    registerJavaCAPSManagementServiceMBeans() throws JBIException
    {
        StandardMBean mbean = null;
        try
        {
            com.sun.esb.management.api.administration.AdministrationService administrationServiceMBeanImpl = createJavaCAPSAdministrationServiceMBean(mEnvCtx);
            mbean = new StandardMBean(administrationServiceMBeanImpl, com.sun.esb.management.api.administration.AdministrationService.class);
            mMBeanSvr.registerMBean(mbean, JBIJMXObjectNames.getJavaCapsAdministrationServiceMBeanObjectName() );
        }
        catch (Exception ex)
        {
            throw new JBIException(ex);
        }

        try
        {
            com.sun.esb.management.api.configuration.ConfigurationService configurationServiceMBeanImpl = createJavaCAPSConfigurationServiceMBean(mEnvCtx);
            mbean = new StandardMBean(configurationServiceMBeanImpl, com.sun.esb.management.api.configuration.ConfigurationService.class);
            mMBeanSvr.registerMBean(mbean, JBIJMXObjectNames.getJavaCapsConfigurationServiceMBeanObjectName() );
        }
        catch (Exception ex)
        {
            throw new JBIException(ex);
        }

        try
        {
            com.sun.esb.management.api.deployment.DeploymentService deploymentServiceMBeanImpl = createJavaCAPSDeploymentServiceMBean(mEnvCtx);
            mbean = new StandardMBean(deploymentServiceMBeanImpl, com.sun.esb.management.api.deployment.DeploymentService.class);
            mMBeanSvr.registerMBean(mbean, JBIJMXObjectNames.getJavaCapsDeploymentServiceMBeanObjectName() );
        }
        catch (Exception ex)
        {
            throw new JBIException(ex);
        }

        try
        {
            com.sun.esb.management.api.installation.InstallationService installationServiceMBeanImpl = createJavaCAPSInstallationServiceMBean(mEnvCtx);
            mbean = new StandardMBean(installationServiceMBeanImpl, com.sun.esb.management.api.installation.InstallationService.class);
            mMBeanSvr.registerMBean(mbean, JBIJMXObjectNames.getJavaCapsInstallationServiceMBeanObjectName() );
        }
        catch (Exception ex)
        {
            throw new JBIException(ex);
        }

        try
        {
            com.sun.esb.management.api.runtime.RuntimeManagementService runtimeManagementServiceMBeanImpl = createJavaCAPSRuntimeManagementServiceMBean(mEnvCtx);
            mbean = new StandardMBean(runtimeManagementServiceMBeanImpl, com.sun.esb.management.api.runtime.RuntimeManagementService.class);
            mMBeanSvr.registerMBean(mbean, JBIJMXObjectNames.getJavaCapsRuntimeManagementServiceMBeanObjectName() );
        }
        catch (Exception ex)
        {
            throw new JBIException(ex);
        }

        try
        {
            com.sun.esb.management.api.performance.PerformanceMeasurementService performanceMeasurementServiceMBeanImpl = createJavaCAPSPerformanceMeasurementServiceMBean(mEnvCtx);
            mbean = new StandardMBean(performanceMeasurementServiceMBeanImpl, com.sun.esb.management.api.performance.PerformanceMeasurementService.class);
            mMBeanSvr.registerMBean(mbean, JBIJMXObjectNames.getJavaCapsPerformanceMeasurementServiceMBeanObjectName() );
        }
        catch (Exception ex)
        {
            throw new JBIException(ex);
        }

        try
        {
            com.sun.esb.management.api.notification.NotificationService notificationService = new com.sun.esb.management.impl.notification.NotificationServiceMBeanImpl(mEnvCtx);
            mMBeanSvr.registerMBean(notificationService,JBIJMXObjectNames.getJavaCapsNotificationServiceMBeanObjectName()); 
        }
        catch (Exception ex)
        {
            throw new JBIException(ex);
        }

    }

    /**
     * Registers the Java CAPS Common Management Service MBeans
     * @throws JBIException on error
     */
    private void unregisterJavaCAPSManagementServiceMBeans()  throws JBIException
    {
        try
        {
            mMBeanSvr.unregisterMBean(JBIJMXObjectNames.getJavaCapsAdministrationServiceMBeanObjectName());
        }
        catch (Exception ex)
        {
            throw new JBIException(ex);
        }

        try
        {
            mMBeanSvr.unregisterMBean(JBIJMXObjectNames.getJavaCapsConfigurationServiceMBeanObjectName());
        }
        catch (Exception ex)
        {
            throw new JBIException(ex);
        }

        try
        {
            mMBeanSvr.unregisterMBean(JBIJMXObjectNames.getJavaCapsDeploymentServiceMBeanObjectName());
        }
        catch (Exception ex)
        {
            throw new JBIException(ex);
        }

        try
        {
            mMBeanSvr.unregisterMBean(JBIJMXObjectNames.getJavaCapsInstallationServiceMBeanObjectName());
        }
        catch (Exception ex)
        {
            throw new JBIException(ex);
        }

        try
        {
            mMBeanSvr.unregisterMBean(JBIJMXObjectNames.getJavaCapsRuntimeManagementServiceMBeanObjectName());
        }
        catch (Exception ex)
        {
            throw new JBIException(ex);
        }

        try
        {
            mMBeanSvr.unregisterMBean(JBIJMXObjectNames.getJavaCapsPerformanceMeasurementServiceMBeanObjectName());
        }
        catch (Exception ex)
        {
            throw new JBIException(ex);
        }

        try
        {
            mMBeanSvr.unregisterMBean(JBIJMXObjectNames.getJavaCapsNotificationServiceMBeanObjectName());
        }
        catch (Exception ex)
        {
            throw new JBIException(ex);
        }
    }

    /**
     * Unregister the UI MBean
     * @throws JBIException on error
     */
    private void unregisterJbiAdminUiMBeans()  throws JBIException
    {

        try
        {
            mMBeanSvr.unregisterMBean(
                JBIJMXObjectNames.getJbiReferenceAdminUiMBeanObjectName());
        }
        catch (Exception ex)
        {
            throw new JBIException(ex);
        }

        try
        {
            //unregister statistics mbean
            mMBeanSvr.unregisterMBean(
                JBIJMXObjectNames.getJbiStatisticsMBeanObjectName());
        }
        catch (Exception ex)
        {
            throw new JBIException(ex);
        }
    }

//    /**
//     * Unregister the UI MBean
//     * @throws JBIException on error
//     */
//    private void unregisterJbiAdminUiBackPortMBeans()  throws JBIException
//    {
//        try
//        {
//            mMBeanSvr.unregisterMBean(
//                JBIJMXObjectNames.getJbiAdminUiMBeanObjectName());
//        }
//        catch (Exception ex)
//        {
//            throw new JBIException(ex);
//        }
//    }

    protected com.sun.esb.management.api.administration.AdministrationService
    createJavaCAPSAdministrationServiceMBean(EnvironmentContext aContext ) throws JBIException {
        return UIMBeanFactory.getInstance().createJavaCAPSAdministrationServiceMBean(aContext);
    }

    protected com.sun.esb.management.api.configuration.ConfigurationService
    createJavaCAPSConfigurationServiceMBean(EnvironmentContext aContext ) throws JBIException {
        return UIMBeanFactory.getInstance().createJavaCAPSConfigurationServiceMBean(aContext);
    }

    protected com.sun.esb.management.api.deployment.DeploymentService
    createJavaCAPSDeploymentServiceMBean(EnvironmentContext aContext ) throws JBIException {
        return UIMBeanFactory.getInstance().createJavaCAPSDeploymentServiceMBean(aContext);
    }

    protected com.sun.esb.management.api.installation.InstallationService
    createJavaCAPSInstallationServiceMBean(EnvironmentContext aContext ) throws JBIException {
        return UIMBeanFactory.getInstance().createJavaCAPSInstallationServiceMBean(aContext);
    }

    protected com.sun.esb.management.api.runtime.RuntimeManagementService
    createJavaCAPSRuntimeManagementServiceMBean(EnvironmentContext aContext ) throws JBIException {
        return UIMBeanFactory.getInstance().createJavaCAPSRuntimeManagementServiceMBean(aContext);
    }

    protected com.sun.esb.management.api.performance.PerformanceMeasurementService
    createJavaCAPSPerformanceMeasurementServiceMBean(EnvironmentContext aContext ) throws JBIException {
        return UIMBeanFactory.getInstance().createJavaCAPSPerformanceMeasurementServiceMBean(aContext);
    }

    protected JBIAdminCommandsUIMBean
    createJBIAdminCommandsUIMBean(EnvironmentContext aContext ) throws JBIException {
        return UIMBeanFactory.getInstance().createJBIAdminCommandsUIMBean(aContext);
    }

//    protected JBIAdminCommandsUIBackPortMBean
//    createJBIAdminCommandsUIBackPortMBean(EnvironmentContext aContext ) throws JBIException {
//        return UIMBeanFactory.getInstance().createJBIAdminCommandsUIBackPortMBean(aContext);
//    }

    /** This method is used to return an instance of the JBI statistics MBean
     * @param aContext EnvironmentContext
     * @returns the mbean
     */
    protected JBIStatisticsMBean createJBIStatisticsMBean(
                EnvironmentContext aContext )
    throws JBIException
    {
        return UIMBeanFactory.getInstance().createJBIStatisticsMBean(aContext);
    }

    // -- AdminEventListener ops
    /**
     * Notification on the creation of a standalone / clustered instance
     *
     * @param instanceName - name of the standalone / clustered instance.
     */
    public void createdInstance(String instanceName)
    {
        try
        {
            if (mEnvCtx.isFrameworkReady(false))
            {
                if ( mPlatform.isStandaloneServer(instanceName) )
                {
                    getRegistry().getUpdater().addServer(instanceName);
                    addSystemComponents(instanceName);
                    registerFacadeMBeansForTarget(instanceName);
                    registerComponentLifeCycleFacadeMBeansForTarget(instanceName);
                    registerComponentExtensionFacadeMBeansForTarget(instanceName);
                    registerComponentConfigurationFacadeMBeansForTarget(instanceName);
                }
                else if ( mPlatform.isClusteredServer(instanceName) )
                {
                    registerComponentConfigurationFacadeMBeansForClusterInstance(instanceName);
                }

            }
            else
            {
                mEnvCtx.isFrameworkReady(true);
            }
        }
        catch (Exception ex)
        {
            String message = mTranslator.getString(
            LocalStringKeys.JBI_ADMIN_CREATE_INSTANCE_EVENT_ERROR);
            mLog.log(Level.WARNING, "{0}{1}", new Object[]{message, ex.getMessage()});
        }
    }

    /**
     * Notification on the deletion of a standalone ( and clustered ) instance.
     * We need to filter out standlaone instance notifications based on which servers
     * are registered.
     *
     * @param instanceName - name of the instance.
     */
    public void deletedInstance(String instanceName)
    {
        try
        {
            if (mEnvCtx.isFrameworkReady(false))
            {
                // perform unregistration only for standalone instances
                List<String> servers = getRegistry().getGenericQuery().getServers();
                if ( servers.contains(instanceName))
                {
                    unregisterFacadeMBeansForTarget(instanceName);
                    unregisterComponentLifeCycleFacadeMBeansForTarget(instanceName);
                    unregisterComponentExtensionFacadeMBeansForTarget(instanceName);
                    unregisterComponentConfigurationFacadeMBeansForTarget(instanceName);
                    getRegistry().getUpdater().removeServer(instanceName);
                }
                else
                {
                    // This is a clustered instance
                    unregisterComponentConfigurationFacadeMBeansForClusterInstance(instanceName);
                }
            }
            else
            {
                mEnvCtx.isFrameworkReady(true);
            }
        }
        catch (Exception ex)
        {
            String message = mTranslator.getString(
            LocalStringKeys.JBI_ADMIN_DELETE_INSTANCE_EVENT_ERROR);
            mLog.log(Level.WARNING, "{0}{1}", new Object[]{message, ex.getMessage()});
        }
    }


    /**
     * Notification on the creation of a cluster
     *
     * @param clusterName - name of the cluster
     */
    public void createdCluster(String clusterName)
    {
        try
        {
            if (mEnvCtx.isFrameworkReady(false))
            {
                getRegistry().getUpdater().addCluster(clusterName);
                addSystemComponents(clusterName);
                registerFacadeMBeansForTarget(clusterName);
                registerComponentLifeCycleFacadeMBeansForTarget(clusterName);
                registerComponentExtensionFacadeMBeansForTarget(clusterName);
                registerComponentConfigurationFacadeMBeansForTarget(clusterName);
            }
            else
            {
                mEnvCtx.isFrameworkReady(true);
            }
        }
        catch (Exception ex)
        {
            String message = mTranslator.getString(
            LocalStringKeys.JBI_ADMIN_CREATE_CLUSTER_EVENT_ERROR);
            mLog.log(Level.WARNING, "{0}{1}", new Object[]{message, ex.getMessage()});
        }
    }

    /**
     * Notification on the deletion of a cluster
     *
     * @param clusterName - name of the cluster
     */
    public void deletedCluster(String clusterName)
    {
        try
        {
            if (mEnvCtx.isFrameworkReady(false))
            {
                unregisterFacadeMBeansForTarget(clusterName);
                unregisterComponentLifeCycleFacadeMBeansForTarget(clusterName);
                unregisterComponentExtensionFacadeMBeansForTarget(clusterName);
                unregisterComponentConfigurationFacadeMBeansForTarget(clusterName);
                getRegistry().getUpdater().removeCluster(clusterName);
            }
            else
            {
                mEnvCtx.isFrameworkReady(true);
            }
        }
        catch (Exception ex)
        {
            String message = mTranslator.getString(
            LocalStringKeys.JBI_ADMIN_DELETE_CLUSTER_EVENT_ERROR);
            mLog.log(Level.WARNING, "{0}{1}", new Object[]{message, ex.getMessage()});
        }

    }
    
    
    /**
     * Update the registry with any missing cluster-refs and server-refs which
     * were added when the JBI Runtime was disabled.
     * Also this method removes cluster-refs and server-refs for non-existing
     * clusters/servers that could have been deleted when JBI runtime was disabled.
     */
    private void updateRegistry()
        throws JBIException
    {
        try
        {
            if ( mPlatform.isAdminServer() )
            {
                // -- Add any missing server-refs
                Set<String> asServers = mPlatform.getStandaloneServerNames();
                List<String> regServers = getRegistry().getGenericQuery().getServers();

                for ( String asServer : asServers )
                {
                    if ( !regServers.contains(asServer) )
                    {
                        getRegistry().getUpdater().addServer(asServer);
                        addSystemComponents(asServer);
                        registerFacadeMBeansForTarget(asServer);
                    }
                }

                for ( String regServer : regServers )
                {
                    if ( !asServers.contains(regServer) )
                    {
                        getRegistry().getUpdater().removeServer(regServer);
                        unregisterFacadeMBeansForTarget(regServer);
                        //component lifecycle MBeans and extension MBeans are not
                        //yet registered - need not unregister them
                    }
                }

                // -- Add and missing cluster-refs
                Set<String>  asClusters  = mPlatform.getClusterNames();
                List<String> regClusters = getRegistry().getGenericQuery().getClusters();

                for ( String asCluster : asClusters )
                {
                    if ( !regClusters.contains(asCluster) )
                    {
                        getRegistry().getUpdater().addCluster(asCluster);
                        addSystemComponents(asCluster);
                        registerFacadeMBeansForTarget(asCluster);
                    }
                }
                for ( String regCluster : regClusters )
                {
                    if ( !asClusters.contains(regCluster) )
                    {
                        getRegistry().getUpdater().removeCluster(regCluster);
                        unregisterFacadeMBeansForTarget(regCluster);
                    }
                }
            }
            else
            {
                String thisTargetName = mPlatform.getTargetName();

                if ( thisTargetName.equals(mPlatform.getInstanceName() ))
                {
                    List<String> regServers = getRegistry().getGenericQuery().getServers();

                    if ( !regServers.contains(thisTargetName) )
                    {
                        getRegistry().getUpdater().addServer(thisTargetName);
                    }
                }
                else
                {
                    List<String> regClusters = getRegistry().getGenericQuery().getClusters();

                    if ( !regClusters.contains(thisTargetName) )
                    {
                        getRegistry().getUpdater().addCluster(thisTargetName);
                    }
                }
            }

        }
        catch (Exception ex)
        {
            throw new JBIException(ex);
        }
    }

    /**
     * When an instance starts up the DAS needs to register a facade
     * ComponentExtension MBean for each installed component on each target.
     *
     * @param target - name of the target
     */
    private void registerComponentExtensionFacadeMBeansForTarget(String target)
        throws Exception
    {
        ComponentQuery compQuery       = getRegistry().getComponentQuery(target);
        List<String> bindingComponents = compQuery.getComponentIds(ComponentType.BINDING);
        List<String> engineComponents  = compQuery.getComponentIds(ComponentType.ENGINE);

        for ( String compName : bindingComponents )
        {
            ComponentExtension compExt = new ComponentExtension(mEnvCtx,
                    target, compName, ComponentType.BINDING);

            StandardMBean compExtMBean = new StandardMBean(compExt,
                com.sun.jbi.management.ComponentExtensionMBean.class);

            ObjectName compExtName = mEnvCtx.getMBeanNames().getBindingMBeanName(
                    compName,
                    MBeanNames.ComponentServiceType.Extension,
                    target);

            mMBeanSvr.registerMBean(compExtMBean, compExtName);
        }

        for ( String compName : engineComponents  )
        {
            ComponentExtension compExt = new ComponentExtension(mEnvCtx,
                    target, compName, ComponentType.ENGINE);

            StandardMBean compExtMBean = new StandardMBean(compExt,
                com.sun.jbi.management.ComponentExtensionMBean.class);

            ObjectName compExtName = mEnvCtx.getMBeanNames().getEngineMBeanName(
                    compName,
                    MBeanNames.ComponentServiceType.Extension,
                    target);

            mMBeanSvr.registerMBean(compExtMBean, compExtName);
        }

    }

    /**
     * When the DAS shuts down it needs to unregister the facade
     * ComponentExtension MBeans for each installed component on each target.
     *
     * @param target - name of the target
     */
    private void unregisterComponentExtensionFacadeMBeansForTarget(String target)
        throws Exception
    {
        ComponentQuery compQuery       = getRegistry().getComponentQuery(target);
        List<String> bindingComponents = compQuery.getComponentIds(ComponentType.BINDING);
        List<String> engineComponents  = compQuery.getComponentIds(ComponentType.ENGINE);
        List<ObjectName> objNames = new java.util.ArrayList();

        for ( String compName : bindingComponents )
        {

            ObjectName compExtName = mEnvCtx.getMBeanNames().getBindingMBeanName(
                    compName,
                    MBeanNames.ComponentServiceType.Extension,
                    target);
            objNames.add(compExtName);
        }

        for ( String compName : engineComponents  )
        {
            ObjectName compExtName = mEnvCtx.getMBeanNames().getEngineMBeanName(
                    compName,
                    MBeanNames.ComponentServiceType.Extension,
                    target);
            objNames.add(compExtName);
        }

        for ( ObjectName objName : objNames )
        {
            if ( mMBeanSvr.isRegistered(objName) )
            {
                mMBeanSvr.unregisterMBean(objName);
            }
        }

    }
    /**
     * When an instance starts up the ComponentFramework registers a ComponentLifeCycle
     * MBean for each installed instance. The DAS needs to register a facade
     * ComponentLifeCycle MBean for each installed component on each target.
     *
     * @param target - name of the target
     */
    private void registerComponentLifeCycleFacadeMBeansForTarget(String target)
        throws Exception
    {
        // -- Get the Binding / Engine ObjectNames
        Map<String, Map<String, ObjectName>> bindings = getBindingComponentLifeCycleNames(target);
        Map<String, Map<String, ObjectName>> engines  = getEngineComponentLifeCycleNames(target);

        // -- Create and Register the corresponding facade MBeans
        Set<String> bindingNames = bindings.keySet();
        for ( String compName : bindingNames )
        {

            ComponentLifeCycle bindingLC = new ComponentLifeCycle(mEnvCtx,
                target, compName, ComponentType.BINDING, bindings.get(compName));

            StandardMBean compLC = new StandardMBean(bindingLC,
                com.sun.jbi.management.ComponentLifeCycleMBean.class);

            ObjectName
                compLCName = FacadeMbeanHelper.getComponentLifeCycleFacadeMBeanName(
                    compName, ComponentType.BINDING, target, mEnvCtx.getMBeanNames());

            mMBeanSvr.registerMBean(compLC, compLCName);
        }

        Set<String> engineNames = engines.keySet();
        for ( String compName : engineNames  )
        {
            ComponentLifeCycle engineLC = new ComponentLifeCycle(mEnvCtx,
                    target, compName, ComponentType.ENGINE, engines.get(compName));

            StandardMBean compLC = new StandardMBean(engineLC,
                com.sun.jbi.management.ComponentLifeCycleMBean.class);

            ObjectName
                compLCName = FacadeMbeanHelper.getComponentLifeCycleFacadeMBeanName(
                    compName, ComponentType.ENGINE, target, mEnvCtx.getMBeanNames());

            mMBeanSvr.registerMBean(compLC, compLCName);
        }

    }

    /**
     * When the DAS shuts down the MBeans registered for the remote ComponentLifeCycle
     * MBeans need to be unregistered.
     *
     * @param target - name of the target
     */
    private void unregisterComponentLifeCycleFacadeMBeansForTarget(String target)
        throws Exception
    {
        // -- Get the ComponentLifeCycle MBean Names
        ObjectName[]
                compLCNames = FacadeMbeanHelper.getComponentLifeCycleObjectNames(
                    mMBeanSvr, mEnvCtx.getMBeanNames(), target);

        // -- Unregister them
        for ( ObjectName compLC : compLCNames )
        {
            if ( mMBeanSvr.isRegistered(compLC) )
            {
                mMBeanSvr.unregisterMBean(compLC);
            }
        }
    }

    /**
     * When an instance starts up the DAS needs to register a facade
     * Component Configuration MBean for each installed component on each target.
     *
     * @param target - name of the target
     */
    private void registerComponentConfigurationFacadeMBeansForTarget(String target)
        throws Exception
    {
        ComponentQuery compQuery       = getRegistry().getComponentQuery(target);
        List<String> bindingComponents = compQuery.getComponentIds(ComponentType.BINDING);
        List<String> engineComponents  = compQuery.getComponentIds(ComponentType.ENGINE);

        for ( String compName : bindingComponents )
        {
            ComponentConfiguration compCfg= new ComponentConfiguration(mEnvCtx,
                    target, compName, ComponentType.BINDING);

            ObjectName compCfgName = mEnvCtx.getMBeanNames().getBindingMBeanName(
                    compName,
                    MBeanNames.ComponentServiceType.Configuration,
                    target);

            mMBeanSvr.registerMBean(compCfg, compCfgName);
        }

        for ( String compName : engineComponents  )
        {
            ComponentConfiguration compCfg = new ComponentConfiguration(mEnvCtx,
                    target, compName, ComponentType.ENGINE);

            ObjectName compCfgName = mEnvCtx.getMBeanNames().getEngineMBeanName(
                    compName,
                    MBeanNames.ComponentServiceType.Configuration,
                    target);

            mMBeanSvr.registerMBean(compCfg, compCfgName);
        }

    }

    /**
     * When the DAS shuts down it needs to unregister the facade
     * Component Configuration MBeans for each installed component on each target.
     *
     * @param target - name of the target
     */
    private void unregisterComponentConfigurationFacadeMBeansForTarget(String target)
        throws Exception
    {
        ComponentQuery compQuery       = getRegistry().getComponentQuery(target);
        List<String> bindingComponents = compQuery.getComponentIds(ComponentType.BINDING);
        List<String> engineComponents  = compQuery.getComponentIds(ComponentType.ENGINE);
        List<ObjectName> objNames = new java.util.ArrayList();

        for ( String compName : bindingComponents )
        {

            ObjectName compCfgName = mEnvCtx.getMBeanNames().getBindingMBeanName(
                    compName,
                    MBeanNames.ComponentServiceType.Configuration,
                    target);
            objNames.add(compCfgName);
        }

        for ( String compName : engineComponents  )
        {
            ObjectName compCfgName = mEnvCtx.getMBeanNames().getEngineMBeanName(
                    compName,
                    MBeanNames.ComponentServiceType.Configuration,
                    target);
            objNames.add(compCfgName);
        }

        for ( ObjectName objName : objNames )
        {
            if ( mMBeanSvr.isRegistered(objName) )
            {
                mMBeanSvr.unregisterMBean(objName);
            }
        }

    }

    /**
     * When an instance starts up the DAS needs to register a facade
     * Component Configuration MBean for each installed component on each instance.
     *
     * @param clusterInstance - name of the clusterInstance target
     */
    private void registerComponentConfigurationFacadeMBeansForClusterInstance(String clusterInstance)
        throws Exception
    {
        String cluster = mPlatform.getTargetName(clusterInstance);
        ComponentQuery compQuery       = getRegistry().getComponentQuery(cluster);
        List<String> bindingComponents = compQuery.getComponentIds(ComponentType.BINDING);
        List<String> engineComponents  = compQuery.getComponentIds(ComponentType.ENGINE);

        for ( String compName : bindingComponents )
        {
            ClusterInstanceComponentConfiguration compCfg= new ClusterInstanceComponentConfiguration(mEnvCtx,
                    clusterInstance, cluster, compName, ComponentType.BINDING);

            ObjectName compCfgName = mEnvCtx.getMBeanNames().getBindingMBeanName(
                    compName,
                    MBeanNames.ComponentServiceType.Configuration,
                    clusterInstance);

            mMBeanSvr.registerMBean(compCfg, compCfgName);
        }

        for ( String compName : engineComponents  )
        {
            ClusterInstanceComponentConfiguration compCfg = new ClusterInstanceComponentConfiguration(mEnvCtx,
                    clusterInstance, cluster, compName, ComponentType.ENGINE);

            ObjectName compCfgName = mEnvCtx.getMBeanNames().getEngineMBeanName(
                    compName,
                    MBeanNames.ComponentServiceType.Configuration,
                    clusterInstance);

            mMBeanSvr.registerMBean(compCfg, compCfgName);
        }
    }

    /**
     * When the DAS shuts down it needs to unregister the facade
     * Component Configuration MBeans for each installed component on each instance.
     *
     * @param clusterName - name of the clusterInstance target
     */
    private void unregisterComponentConfigurationFacadeMBeansForClusterInstance(String clusterInstance)
        throws Exception
    {
        String cluster = mPlatform.getTargetName(clusterInstance);
        ComponentQuery compQuery       = getRegistry().getComponentQuery(cluster);
        List<String> bindingComponents = compQuery.getComponentIds(ComponentType.BINDING);
        List<String> engineComponents  = compQuery.getComponentIds(ComponentType.ENGINE);
        List<ObjectName> objNames = new java.util.ArrayList();

        for ( String compName : bindingComponents )
        {

            ObjectName compCfgName = mEnvCtx.getMBeanNames().getBindingMBeanName(
                    compName,
                    MBeanNames.ComponentServiceType.Configuration,
                    clusterInstance);
            objNames.add(compCfgName);
        }

        for ( String compName : engineComponents  )
        {
            ObjectName compCfgName = mEnvCtx.getMBeanNames().getEngineMBeanName(
                    compName,
                    MBeanNames.ComponentServiceType.Configuration,
                    clusterInstance);
            objNames.add(compCfgName);
        }

        for ( ObjectName objName : objNames )
        {
            if ( mMBeanSvr.isRegistered(objName) )
            {
                mMBeanSvr.unregisterMBean(objName);
            }
        }

    }

    /**
     * Get the ComponentLifeCycle MBean Object Names for all the Bindings installed on
     * the target.
     *
     * This returns a Map, where each map entry key is a binding name and the value
     * is another map of instance name and the binding object name on the instance
     */
    private Map<String, Map<String, ObjectName>> getBindingComponentLifeCycleNames(String target)
        throws Exception
    {
        ComponentQuery compQuery       = getRegistry().getComponentQuery(target);
        List<String> bindingComponents = compQuery.getComponentIds(ComponentType.BINDING);

        HashMap<String, Map<String, ObjectName>>
            allBindings = new HashMap<String, Map<String, ObjectName>>();
        int i = 0;
        for ( String bindingName : bindingComponents)
        {
            HashMap<String, ObjectName>
                    bindingObjNames = new HashMap<String, ObjectName>();
            if ( "domain".equals(target))
            {
                bindingObjNames.put(target, null);
            }
            else if ( mPlatform.isStandaloneServer(target))
            {
                ObjectName bindingObjName = mMgtCtx.getMBeanNames(target).getBindingMBeanName(bindingName,
                MBeanNames.CONTROL_TYPE_LIFECYCLE);
                bindingObjNames.put(target, bindingObjName);
            }
            else
            {
                Set<String> instances = mPlatform.getServersInCluster(target);
                for ( String instance : instances )
                {
                    ObjectName bindingObjName = mMgtCtx.getMBeanNames(instance).getBindingMBeanName(bindingName,
                        MBeanNames.CONTROL_TYPE_LIFECYCLE);
                    bindingObjNames.put(instance, bindingObjName);
                }

            }
            allBindings.put(bindingName, bindingObjNames);
        }

        return allBindings;
    }


    /**
     * Get the ComponentLifeCycle MBean Object Names for all the Engines installed on
     * the target.
     *
     * This returns a Map, where each map entry key is a engine name and the value
     * is another map of instance name and the engine object name on the instance
     */
    private  Map<String, Map<String, ObjectName>> getEngineComponentLifeCycleNames(String target)
        throws Exception
    {

        ComponentQuery compQuery       = getRegistry().getComponentQuery(target);
        List<String> engineComponents = compQuery.getComponentIds(ComponentType.ENGINE);

        HashMap<String, Map<String, ObjectName>>
            allEngines = new HashMap<String, Map<String, ObjectName>>();

        int i = 0;
        for ( String engineName : engineComponents)
        {
            HashMap<String, ObjectName>
                    engineObjNames = new HashMap<String, ObjectName>();
            if ( "domain".equals(target))
            {
                engineObjNames.put(target, null);
            }
            else if ( mPlatform.isStandaloneServer(target))
            {
                ObjectName engineObjName = mMgtCtx.getMBeanNames(target).getEngineMBeanName(engineName,
                MBeanNames.CONTROL_TYPE_LIFECYCLE);
                engineObjNames.put(target, engineObjName);
            }
            else
            {
                Set<String> instances = mPlatform.getServersInCluster(target);
                for ( String instance : instances )
                {
                    ObjectName engineObjName = mMgtCtx.getMBeanNames(instance).getEngineMBeanName(engineName,
                        MBeanNames.CONTROL_TYPE_LIFECYCLE);
                    engineObjNames.put(instance, engineObjName);
                }

            }
            allEngines.put(engineName, engineObjNames);
        }

        return allEngines;
    }

    /**
     * Register the runtime configuration dynamic MBeans.
     *
     * @param target - target instance / cluster name
     */
    private void registerRuntimeConfigFacadeMBeansForTarget(String target)
        throws Exception
    {
        Map<MBeanNames.ServiceType, ConfigurationFactory>
            configs = ConfigurationBuilder.createConfigurations(readDefaultProperties());
        mConfigSvcTypes = configs.keySet();

        for ( MBeanNames.ServiceType serviceType : mConfigSvcTypes )
        {
            ObjectName name = mEnvCtx.getMBeanNames().
                getSystemServiceMBeanName(
                    MBeanNames.ServiceName.ConfigurationService ,
                    serviceType,
                    target);

            if ( !mMBeanSvr.isRegistered(name))
            {
                // Create and register the facade config MBean
                ModelMBeanInfo mbeanInfo = RuntimeConfiguration.createMBeanInfo(
                    configs.get(serviceType).createMBeanAttributeInfo());
                RequiredModelMBean
                    modelConfigMBean = new RuntimeConfiguration(mbeanInfo,
                        ConfigurationCategory.valueOf(serviceType.toString()), target);
                modelConfigMBean.setManagedResource(modelConfigMBean, "ObjectReference");
                mMBeanSvr.registerMBean(modelConfigMBean, name );
            }
        }
    }


    /**
     * Unregister the runtime configuration dynamic MBeans.
     *
     * @param target - target instance / cluster name
     */
    private void unregisterRuntimeConfigFacadeMBeansForTarget(String target)
        throws Exception
    {
        if ( mConfigSvcTypes != null )
        {
            for ( MBeanNames.ServiceType serviceType : mConfigSvcTypes )
            {
                ObjectName name = mEnvCtx.getMBeanNames().
                    getSystemServiceMBeanName(
                        MBeanNames.ServiceName.ConfigurationService ,
                        serviceType,
                        target);

                // Unregister the config MBean
                if ( mMBeanSvr.isRegistered(name) )
                {
                    mMBeanSvr.unregisterMBean(name);
                }
            }
        }
    }

    
    
    /**	 
      * This method is used to add the component-refs and shared-library-refs 	 
      * for schemaorg_apache_xmlbeans.system components when a new server-ref/cluster-ref is added to the registry
      * @param targetName the name of the target to add the refs to	 
      */	 
    private void addSystemComponents(String targetName) throws Exception	 
    {	 
        
        ComponentQuery compQuery  = getRegistry().getComponentQuery("domain"); 
        GenericQuery genQuery = getRegistry().getGenericQuery();
        ComponentInfoImpl compInfo = null;
        String WORKSPACE = "workspace";
        String INSTALL_ROOT = "install_root";

        List<String> components = compQuery.getComponentIds(ComponentType.BINDINGS_AND_ENGINES);
        for (String componentName : components)
        {
            //check in registry
            if (genQuery.isSystemComponent(componentName))
            {
                //check in repository
                Archive archive =
                        mMgtCtx.getRepository().getArchive(ArchiveType.COMPONENT, componentName);
                if (archive != null)
                {
                    String componentRoot = new File(archive.getPath()).getParent();
                    String componentInstallRoot = componentRoot + File.separator + INSTALL_ROOT;
                    String componentWorkspaceRoot = componentInstallRoot + File.separator + WORKSPACE;

                    compInfo = new ComponentInfoImpl();
                    compInfo.setName(componentName);
                    compInfo.setInstallRoot(componentInstallRoot);
                    compInfo.setWorkspaceRoot(componentWorkspaceRoot);
                    compInfo.setStatus(ComponentState.SHUTDOWN);

                    getRegistry().getUpdater().addComponent(targetName, compInfo);
                }
            }
        }

        List<String> sharedLibs = compQuery.getComponentIds(ComponentType.SHARED_LIBRARY);
        for (String sharedLibName : sharedLibs)
        {
           //check in registry
            if (genQuery.isSystemSharedLibrary(sharedLibName))
            {
                //check in repository
                Archive archive = mMgtCtx.getRepository().getArchive(
                        ArchiveType.SHARED_LIBRARY, sharedLibName);
                if (archive != null)
                {
                    String sharedLibRoot = new File(archive.getPath()).getParent();
                    String sharedLibInstallRoot = sharedLibRoot + File.separator + INSTALL_ROOT;
                    compInfo = new ComponentInfoImpl();
                    compInfo.setName(sharedLibName);
                    compInfo.setInstallRoot(sharedLibInstallRoot);

                    getRegistry().getUpdater().addSharedLibrary(targetName, compInfo);
                }
            }
        }
     }

    /**
     * This path is only executed in test environments when the domain
     * ant startup script exists.
     */
    private void createClientConfigFiles()
    {
        if (!new File(getJbiDomainUpdateScriptPath()).canRead())
        {
            return;
        }

        // Set the JMX/RMI port value for the DAS JMX connector server
        System.setProperty("com.sun.jbi.management.JmxRmiPort", mPlatform.getJmxRmiPort());

        // Call out to ant script to create config files
        mLog.fine("Running ant admin startup tasks ...");
        String antfn = getJbiDomainUpdateScriptPath();
        AntScriptRunner ant = new AntScriptRunner();

        ant.runAntTarget(mEnvCtx.getJbiInstanceRoot(), antfn, "admin_startup_tasks");
    }

    /** Returns the full path name of the jbi domain update ant script */
    private String getJbiDomainUpdateScriptPath()
    {
        return mEnvCtx.getJbiInstallRoot() +
                java.io.File.separator + "templates" +
                java.io.File.separator + "update_jbi_domain.ant";
    }

    /**
     * Read the default configuration file from $JBI_INSTALL_ROOT/lib/templates
     *
     * @return the loaded properties
     * @exception java.io.IOException on file read errors
     */
    private Properties readDefaultProperties()
        throws java.io.IOException
    {
        if ( mDefaults == null )
        {
            mDefaults = new Properties();
            StringBuffer pathToConfig = new StringBuffer(mEnvCtx.getJbiInstallRoot());
            pathToConfig.append(java.io.File.separatorChar);
            pathToConfig.append("lib");
            pathToConfig.append(java.io.File.separatorChar);
            pathToConfig.append("install");
            pathToConfig.append(java.io.File.separatorChar);
            pathToConfig.append("templates");
            pathToConfig.append(java.io.File.separatorChar);

            File defConfig = new File( pathToConfig.toString() , DEFAULT_CONFIG_FILE);
            if ( defConfig.exists() && defConfig.length() > 0 )
            {
                java.io.InputStream ipStr = new java.io.FileInputStream(defConfig);
                if ( ipStr != null )
                {
                    mDefaults.load(ipStr);
                    ipStr.close();
                }
            }
        }
        return mDefaults;
    }

    /**
     *  Get a reference to the JBI registry.
     */
    protected Registry getRegistry()
    {
        return (com.sun.jbi.management.registry.Registry)mEnvCtx.getRegistry();
    }
    
   private void registerEventManagementControllerMbean()  throws Exception{
       mLog.finest("reg. Event Management Controller MBeans");
       EventManagementControllerMBean emController = new EventManagementControllerMBean(mEnvCtx);
       ObjectName emcObjectName = new ObjectName(mMBeanNames.EVENTMANAGEMENT_CONTROLLER_MBEAN_NAME);  
 
       mMBeanSvr.registerMBean(emController,emcObjectName); 
        
   }
   
   private void unregisterEventManagementControllerMbean() throws Exception {
       mLog.finest("unreg. Event Management Controller MBeans");
       
       ObjectName emcObjectName = new ObjectName(mMBeanNames.EVENTMANAGEMENT_CONTROLLER_MBEAN_NAME);  
       if(mMBeanSvr.isRegistered(emcObjectName)) {
           mMBeanSvr.unregisterMBean(emcObjectName);
       }
 
   }

   private void registerEventManagementForwarderMbean()  throws Exception{
       mLog.finest("reg. Event Management Forwarder MBeans");
       EventForwarderMBean emForwarder = new EventForwarderMBean(mEnvCtx);
       ObjectName emfObjectName = new ObjectName(mMBeanNames.EVENT_MANAGEMENT_MBEAN_NAME);  
 
       mMBeanSvr.registerMBean(emForwarder,emfObjectName); 
       
   }
   
   private void unregisterEventManagementForwarderMbean()throws Exception {
       mLog.finest("unreg. Event Management Forwarder MBeans");
       ObjectName emfObjectName = new ObjectName(mMBeanNames.EVENT_MANAGEMENT_MBEAN_NAME);  
       
       if(mMBeanSvr.isRegistered(emfObjectName)) {
           mMBeanSvr.unregisterMBean(emfObjectName); 
       }
   }
    private void initExtensionServices() {
        mLog.finest("Init Extension Services.");

        // Load extension services
        mLog.log(Level.FINER, "Loading management extension {0}", ExtensionService.class.getName());
        loader = ServiceLoader.load(ExtensionService.class);

        // Init services
        Iterator<ExtensionService> extensions = loader.iterator();
        while (extensions != null && extensions.hasNext()) {
            ExtensionService ext = extensions.next();
            try {
                mLog.log(Level.FINER, "Initializing extension {0}", ext.getClass().getName());
                ext.initService(this.mEnvCtx);
            } catch (Exception ex) {
                mLog.log(Level.WARNING, ex.getMessage(), ex);
            }
        }

        mLog.log(Level.FINER, "Loading management extension : DONE !");
    }

    private void startExtensionServices() {
        mLog.finest("Starting Extension Services.");

        Iterator<ExtensionService> extensions = loader.iterator();
        while (extensions != null && extensions.hasNext()) {
            ExtensionService ext = extensions.next();

            try {
                mLog.log(Level.FINER, "Starting extension {0}", ext.getClass().getName());
                ext.startService();
            } catch (Exception ex) {
                Logger.getLogger(ExtensionService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void stopExtensionServices() {
        mLog.finest("Stopping Extension Services.");

        Iterator<ExtensionService> extensions = loader.iterator();
        while (extensions != null && extensions.hasNext()) {
            ExtensionService ext = extensions.next();
            try {
                mLog.log(Level.FINER, "Stopping extension {0}", ext.getClass().getName());
                ext.stopService();
            } catch (Exception ex) {
                Logger.getLogger(ExtensionService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
