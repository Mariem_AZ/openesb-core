--Drop the user that was created earlier. Note that if you chose a different name for the 
--user while creating the user, you will have to specify that name here.
DROP USER EVENTDB_USER CASCADE;

--Drop the tablespace that was created earlier. Note that if you chose a different name for 
--the tablespace while creating the user, you will have to specify that name here.
DROP TABLESPACE EM_EVENTSTORE_DB INCLUDING CONTENTS AND DATAFILES CASCADE CONSTRAINTS;

--Manually delete the datafiles that were created. If you used the defaults while creating
--the datafiles, the names would be EVENTDB_USER_DB1.dat'and 'EVENTDB_USER_DB2.dat'