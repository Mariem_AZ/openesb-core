#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiadmin01600.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

#############################################################################################
#                     Tests fix for Issue 100                                               #
#############################################################################################

echo "jbiadmin01609 : Test fix for issue 326"

. ./regress_defs.ksh

COMPONENT_CONFIG_PROPS=$JV_SVC_REGRESS/deploytest/httpbc-component-config.properties
COMPONENT_NAME=sun-http-binding

# Start targets
asadmin start-jbi-component  --port $ASADMIN_PORT $ASADMIN_PW_OPTS $COMPONENT_NAME



# Test (a) : Only one port should be set correctly
echo "Expected Result: Only one of the two ports should be set."
$JBI_ANT_NEG  -Djbi.component.name=$COMPONENT_NAME -Djbi.config.params.file=$COMPONENT_CONFIG_PROPS set-component-configuration

# Test (b) : The first port should be set the second command should fail
echo "Expected Result: The HttpDefaultPort set should be a success"
$JBI_ANT_NEG  -Djbi.component.name=$COMPONENT_NAME  -Djbi.config.param.name=HttpDefaultPort -Djbi.config.param.value=5555 set-component-configuration

echo "Expected Result: The HttpsDefaultPort set should fail"
$JBI_ANT_NEG  -Djbi.component.name=$COMPONENT_NAME  -Djbi.config.param.name=HttpsDefaultPort -Djbi.config.param.value=5555 set-component-configuration


