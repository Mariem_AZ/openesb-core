<?xml version="1.0"?>
<!--
 # BEGIN_HEADER - DO NOT EDIT
 #
 # The contents of this file are subject to the terms
 # of the Common Development and Distribution License
 # (the "License").  You may not use this file except
 # in compliance with the License.
 #
 # You can obtain a copy of the license at
 # https://open-esb.dev.java.net/public/CDDLv1.0.html.
 # See the License for the specific language governing
 # permissions and limitations under the License.
 #
 # When distributing Covered Code, include this CDDL
 # HEADER in each file and include the License file at
 # https://open-esb.dev.java.net/public/CDDLv1.0.html.
 # If applicable add the following below this CDDL HEADER,
 # with the fields enclosed by brackets "[]" replaced with
 # your own identifying information: Portions Copyright
 # [year] [name of copyright owner]
-->

<!--
 # @(#)jbiadmin01600.xml
 # Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 #
 # END_HEADER - DO NOT EDIT
-->
<!--
    This tests querying the ComponentConfigurationMBean
-->
<project name="CustomMBeanTest" default="init" basedir=".">
   <import file="inc/jbitestsetup.ant"/>
   <import file="Package.xml"/>
   
   <property name="component-with-custom-mbean.jar" value="component-with-custom-mbean.jar" />
   <property name="component-with-new-config-mbean.jar" value="component-with-new-config-mbean.jar" />
   <property name="component-with-config-mbean-rgstrd-in-start.jar" value="component-with-config-mbean-rgstrd-in-start.jar" />
   <property name="component-with-config-mbean-expose-no-attrs.jar" value="component-with-new-config-mbean-expose-no-attrs.jar" />
   
   <target name="init" depends="manage_test_init">  
        <mkdir dir="${regress.dist.dir}"/>
   </target>
   
   <target name="pkg.test.component" depends="init, pkg.init" description="package the test component">
      <!-- The test component used in this test throws an exception on shutdown.-->
      <delete file="${regress.dist.dir}/${component-with-custom-mbean.jar}" />
      <jar destfile="${regress.dist.dir}/${component-with-custom-mbean.jar}">
         <zipfileset dir="${deploy.test.dir}" includes="binding1.xml" fullpath="META-INF/jbi.xml"/>
         <zipfileset dir="${deploy.test.dir}" includes="BindingComponent.class" fullpath="classes/deploytest/BindingComponent.class"/>
         <zipfileset dir="${deploy.test.dir}" includes="CustomConfigMBean.class" fullpath="classes/deploytest/CustomConfigMBean.class"/>
         <zipfileset dir="${deploy.test.dir}" includes="CustomConfig.class" fullpath="classes/deploytest/CustomConfig.class"/>
         <zipfileset dir="${deploy.test.dir}" includes="CustomConfigurationMBean.class" fullpath="classes/deploytest/CustomConfigurationMBean.class" />
         <zipfileset dir="${deploy.test.dir}" includes="CustomConfiguration.class" fullpath="classes/deploytest/CustomConfiguration.class" />        
         <zipfileset dir="${deploy.test.dir}" includes="componentConfiguration_old.xml" fullpath="META-INF/componentConfiguration.xml" />
         <zipfileset dir="${deploy.test.dir}" includes="componentConfiguration_old.xsd" fullpath="META-INF/componentConfiguration.xsd" />
         <zipfileset dir="${deploy.test.dir}" includes="register-custom-mbean.properties" fullpath="config.properties"/>
      </jar>
   </target>
   
   <target name="pkg.new.test.component" depends="init, pkg.init" description="package the test component">
      <!-- The test component used in this test throws an exception on shutdown.-->
      <delete file="${regress.dist.dir}/${component-with-new-config-mbean.jar}" />
      <jar destfile="${regress.dist.dir}/${component-with-new-config-mbean.jar}">
         <zipfileset dir="${deploy.test.dir}" includes="binding-new-schema.xml" fullpath="META-INF/jbi.xml"/>
         <zipfileset dir="${deploy.test.dir}" includes="BindingComponent.class" fullpath="classes/deploytest/BindingComponent.class"/>
         <zipfileset dir="${deploy.test.dir}" includes="ConsolidatedSchemaComponent.class" fullpath="classes/deploytest/ConsolidatedSchemaComponent.class"/>
         <zipfileset dir="${deploy.test.dir}" includes="CustomConfigMBean.class" fullpath="classes/deploytest/CustomConfigMBean.class"/>
         <zipfileset dir="${deploy.test.dir}" includes="CustomConfig.class" fullpath="classes/deploytest/CustomConfig.class"/>
         <zipfileset dir="${deploy.test.dir}" includes="CustomConfigurationMBean.class" fullpath="classes/deploytest/CustomConfigurationMBean.class" />
         <zipfileset dir="${deploy.test.dir}" includes="CustomConfiguration.class" fullpath="classes/deploytest/CustomConfiguration.class" />
         <zipfileset dir="${deploy.test.dir}" includes="register-new-config-mbean.properties" fullpath="config.properties"/>
      </jar>
   </target>

   <target name="get.component.configuration" depends="init" description="Query the ComponentConfiguration MBean for the component">
        <antcall target="test.component.configuration">
            <param name="target" value="${TARGET}"/>
            <param name ="component_name" value="manage-binding-1"/>
        </antcall>
   </target>

   <target name="pkg.component.configmbeaninstartstop" depends="init" description="component configuration mbean registered/unregistered in start/stop methods">
        <delete quiet="false" file="${regress.dist.dir}/${component-with-config-mbean-rgstrd-in-start.jar}" />
        <jar destfile="${regress.dist.dir}/${component-with-config-mbean-rgstrd-in-start.jar}">
            <zipfileset dir="${cconfigmbeaninstartstop.dir}" includes="test-component.xml" fullpath="META-INF/jbi.xml" />
            <zipfileset dir="${cconfigmbeaninstartstop.dir}" includes="BindingComponent.class" fullpath="classes/componentconfiguration/cconfigmbeaninstartstop/BindingComponent.class" />
            <zipfileset dir="${cconfigmbeaninstartstop.dir}" includes="CustomConfig.class" fullpath="classes/componentconfiguration/cconfigmbeaninstartstop/CustomConfig.class" />
            <zipfileset dir="${cconfigmbeaninstartstop.dir}" includes="CustomConfigMBean.class" fullpath="classes/componentconfiguration/cconfigmbeaninstartstop/CustomConfigMBean.class" />
            <zipfileset dir="${cconfigmbeaninstartstop.dir}" includes="register-config.properties" fullpath="config.properties"/>
        </jar>
   </target>
   <target name="pkg.component.configmbeannoattrs" depends="init" description="component configuration mbean which does not expose any attributes">
        <delete quiet="false" file="${regress.dist.dir}/${component-with-config-mbean-expose-no-attrs.jar}" />
        <jar destfile="${regress.dist.dir}/${component-with-config-mbean-expose-no-attrs.jar}">
            <zipfileset dir="${cconfigmbeannoattrs.dir}" includes="test-component.xml" fullpath="META-INF/jbi.xml" />
            <zipfileset dir="${cconfigmbeannoattrs.dir}" includes="BindingComponent.class" fullpath="classes/componentconfiguration/cconfigmbeannoattrs/BindingComponent.class" />
            <zipfileset dir="${cconfigmbeannoattrs.dir}" includes="CustomConfig.class" fullpath="classes/componentconfiguration/cconfigmbeannoattrs/CustomConfig.class" />
            <zipfileset dir="${cconfigmbeannoattrs.dir}" includes="CustomConfigMBean.class" fullpath="classes/componentconfiguration/cconfigmbeannoattrs/CustomConfigMBean.class" />
            <zipfileset dir="${cconfigmbeannoattrs.dir}" includes="register-config.properties" fullpath="config.properties"/>
        </jar>
   </target>
</project>
