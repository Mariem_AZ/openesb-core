#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiadmin01501.ksh
# Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

echo "jbiadmin01511 : Test for component upgrade in cluster"

####
# This tests component upgrade in a cluster.
# +ve test case
####
. ./regress_defs.ksh

#create a new instance and add to the cluster CAS-cluster1
asadmin create-instance  --port $ASADMIN_PORT --user $AS_ADMIN_USER $ASADMIN_PW_OPTS --cluster CAS-cluster1 --nodeagent agent1 inst2
createInstanceDelay
asadmin start-instance  --port $ASADMIN_PORT -u $AS_ADMIN_USER $ASADMIN_PW_OPTS CAS-cluster1-inst1
startInstanceDelay
asadmin start-instance  --port $ASADMIN_PORT -u $AS_ADMIN_USER $ASADMIN_PW_OPTS inst2
startInstanceDelay

#package component and sa files
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -f jbiadmin01504.xml 1>&2

echo install a component
asadmin install-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target CAS-cluster1 $JV_SVC_TEST_CLASSES/dist/upgrade-original.jar
installComponentDelay

echo deploy the SAs
asadmin deploy-jbi-service-assembly --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target CAS-cluster1 $JV_SVC_TEST_CLASSES/dist/upgrade-sa1.jar
deploySaDelay
asadmin deploy-jbi-service-assembly --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target CAS-cluster1 $JV_SVC_TEST_CLASSES/dist/upgrade-sa2.jar
deploySaDelay

echo shut-down the component 
asadmin shut-down-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target CAS-cluster1 Upgrade-Component
stopComponentDelay

#copy the workspace settings file for future checking
cp $JBI_DOMAIN_ROOT/../../nodeagents/agent1/CAS-cluster1-inst1/jbi/components/Upgrade-Component/install_root/workspace/settings.txt  $SVC_TEST_CLASSES/jbiadmin01511-CAS-cluster1-inst1-settings.txt
cp $JBI_DOMAIN_ROOT/../../nodeagents/agent1/inst2/jbi/components/Upgrade-Component/install_root/workspace/settings.txt  $SVC_TEST_CLASSES/jbiadmin01511-inst2-settings.txt

echo upgrade the component
$JBI_ANT -Djbi.component.name=Upgrade-Component -Djbi.install.file=$JV_SVC_TEST_CLASSES/dist/upgrade-modified.jar upgrade-component 2>&1
installComponentDelay

echo verify if the component upgrades to workspace has been retained
echo "new file created in workspace by upgrade" >  $SVC_TEST_CLASSES/upgrade.txt
diff $JBI_DOMAIN_ROOT/../../nodeagents/agent1/CAS-cluster1-inst1/jbi/components/Upgrade-Component/install_root/workspace/upgrade.txt  $SVC_TEST_CLASSES/upgrade.txt 2>&1
diff $JBI_DOMAIN_ROOT/../../nodeagents/agent1/inst2/jbi/components/Upgrade-Component/install_root/workspace/upgrade.txt  $SVC_TEST_CLASSES/upgrade.txt 2>&1

echo verify the workspace dir has been preserved
diff $JBI_DOMAIN_ROOT/../../nodeagents/agent1/CAS-cluster1-inst1/jbi/components/Upgrade-Component/install_root/workspace/settings.txt  $SVC_TEST_CLASSES/jbiadmin01511-CAS-cluster1-inst1-settings.txt 2>&1
diff $JBI_DOMAIN_ROOT/../../nodeagents/agent1/inst2/jbi/components/Upgrade-Component/install_root/workspace/settings.txt  $SVC_TEST_CLASSES/jbiadmin01511-inst2-settings.txt 2>&1

echo verify that install root has been replaced
asadmin start-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target CAS-cluster1 Upgrade-Component
startComponentDelay

echo undeploy the SAs
asadmin undeploy-jbi-service-assembly --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target CAS-cluster1 Upgrade-SA1
undeploySaDelay
asadmin undeploy-jbi-service-assembly --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target CAS-cluster1 Upgrade-SA2
undeploySaDelay

echo shutdown the component
asadmin shut-down-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target CAS-cluster1 Upgrade-Component
stopComponentDelay

echo uninstall the component
asadmin uninstall-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target CAS-cluster1 Upgrade-Component
uninstallComponentDelay

rm $SVC_TEST_CLASSES/jbiadmin01511-CAS-cluster1-inst1-settings.txt
rm $SVC_TEST_CLASSES/jbiadmin01511-inst2-settings.txt
rm $SVC_TEST_CLASSES/upgrade.txt
