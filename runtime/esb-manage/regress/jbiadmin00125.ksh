#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiadmin00125.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

#############################################################################################
#                              Tests forceful component uninstall                           #
#                                                                                           #             
# This tests the forced component shutdown operation.                                      #
#                                                                                           #
# The test component used in this test throws an exception on shutdown.                    #
#                                                                                           #
# The test :                                                                                #
# (a) Shutdown the component normally - that should fail                                   #
# (b) Shutdown the component with force=true, the JBI Runtime should ignore                #
#     the component exceptions and go ahead with the shutdown which is a success.          #
#                                                                                           #
#############################################################################################

echo "jbiadmin00125 : Install a component, shutdown normally, then shutdown forcefully"

. ./regress_defs.ksh
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -f jbiadmin00125.xml pkg.test.component

COMPONENT_ARCHIVE=$JV_SVC_TEST_CLASSES/dist/bad-shutdown-binding.jar
COMPONENT_NAME=manage-binding-1

# Install the component
$JBI_ANT_NEG -Djbi.install.file=$COMPONENT_ARCHIVE  install-component

# Start the component
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME  start-component

# Stop the component
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME  stop-component

# Shutdown the component, this should fail
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME  shut-down-component


# Shutdown the component forcefully
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -DCOMPONENT_NAME=$COMPONENT_NAME -f jbiadmin00125.xml shutdown.component.with.force


# Uninstall the component
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME uninstall-component
