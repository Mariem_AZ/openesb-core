/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestComponentOperation.java
 * Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

import com.sun.jbi.management.ComponentInstallationContext;

import java.util.ArrayList;

/**
 * Tests for the ComponentOperation class.
 *
 * @author Sun Microsystems, Inc.
 */
public class TestComponentOperation
    extends junit.framework.TestCase
{
    /**
     * OperationCounter.
     */
    private OperationCounter mCounter;

    /**
     * ComponentOperation (extends Operation).
     */
    private ComponentOperation mOperation;

    /**
     * Helper class to setup environment for testing.
     */
    private EnvironmentSetup mSetup;

    /**
     * Instance of the EnvironmentContext class.
     */
    private EnvironmentContext mContext;

    /**
     * Local instance of the ComponentInstallationContext class.
     */
    private ComponentInstallationContext mBindingContext;

    /**
     * Local instance of the ComponentInstallationContext class.
     */
    private ComponentInstallationContext mEngineContext;

    /**
     * Instance of the ComponentFramework class.
     */
    private ComponentFramework mCompFW;

    /**
     * Instance of the ComponentRegistry class.
     */
    private ComponentRegistry mCompReg;

    /**
     * Instance of Component for the binding.
     */
    private Component mBinding;

    /**
     * Instance of Component for the engine.
     */
    private Component mEngine;

    /**
     * Local instance of the binding bootstrap class path array.
     */
    private ArrayList mBindingBootClassPath;

    /**
     * Local instance of the binding lifecycle class path array.
     */
    private ArrayList mBindingLifeClassPath;

    /**
     * Local instance of the engine bootstrap class path array.
     */
    private ArrayList mEngineBootClassPath;

    /**
     * Local instance of the engine lifecycle class path array.
     */
    private ArrayList mEngineLifeClassPath;

    /**
     * Install root for components.
     */
    private String mInstallRoot;

    /**
     * Constant for invalid operation value.
     */
    private static final int BAD_OPER = 999999;

    /**
     * The constructor for this testcase, forwards the test name to
     * the jUnit TestCase base class.
     * @param aTestName String with the name of this test.
     */
    public TestComponentOperation(String aTestName)
    {
        super(aTestName);
    }

    /**
     * Setup for the test. This creates objects needed for the test.
     * @throws Exception when set up fails for any reason.
     */
    public void setUp()
        throws Exception
    {
        super.setUp();
        mCounter = new OperationCounter();
        String srcroot = System.getProperty("junit.srcroot") + "/";
        mInstallRoot = srcroot;

        // Create and initialize the EnvironmentContext. Create, initialize,
        // and start up the Component Registry and Component Framework.

        mSetup = new EnvironmentSetup();
        mContext = mSetup.getEnvironmentContext();
        mSetup.startup(true, true);
        mCompFW = mContext.getComponentFramework();
        mCompReg = mContext.getComponentRegistry();

        // Create binding class path / class name lists

        mBindingBootClassPath = new ArrayList();
        mBindingBootClassPath.add(srcroot + Constants.BC_BOOTSTRAP_CLASS_PATH);
        mBindingLifeClassPath = new ArrayList();
        mBindingLifeClassPath.add(Constants.BC_LIFECYCLE_CLASS_PATH);

        // Create engine class path lists

        mEngineBootClassPath = new ArrayList();
        mEngineBootClassPath.add(srcroot + Constants.SE_BOOTSTRAP_CLASS_PATH);
        mEngineLifeClassPath = new ArrayList();
        mEngineLifeClassPath.add(Constants.SE_LIFECYCLE_CLASS_PATH);

        // Install a test binding and a test engine

        mBindingContext =
            new ComponentInstallationContext(
                Constants.BC_NAME,
                ComponentInstallationContext.BINDING,
                Constants.BC_LIFECYCLE_CLASS_NAME,
                mBindingLifeClassPath,
                null);
        mBindingContext.setInstallRoot(mInstallRoot);
        mBindingContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mBindingContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            mBindingBootClassPath,
            null, false);
        mCompFW.installComponent(mBindingContext);
        mBinding = mCompReg.getComponent(Constants.BC_NAME);
        mEngineContext =
            new ComponentInstallationContext(
                Constants.SE_NAME,
                ComponentInstallationContext.ENGINE,
                Constants.SE_LIFECYCLE_CLASS_NAME,
                mEngineLifeClassPath,
                null);
        mEngineContext.setInstallRoot(mInstallRoot);
        mEngineContext.setIsInstall(true);
        mCompFW.loadBootstrap(
            mEngineContext,
            Constants.SE_BOOTSTRAP_CLASS_NAME,
            mEngineBootClassPath,
            null, false);
        mCompFW.installComponent(mEngineContext);
        mEngine = mCompReg.getComponent(Constants.SE_NAME);
    }

    /**
     * Cleanup for the test.
     * @throws Exception when tearDown fails for any reason.
     */
    public void tearDown()
        throws Exception
    {
        super.tearDown();
        mSetup.shutdown(true, true);
    }


// =============================  test methods ================================

    /**
     * Tests constructor with bad operation parameter. An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testConstructorBadOperation()
        throws Exception
    {
        try
        {
            mOperation = new ComponentOperation(mCounter, mBinding, BAD_OPER);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Unexpected exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("Invalid argument")));
        }
    }

    /**
     * Tests constructor with null component parameter. An exception is
     * expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testConstructorNullComponent()
        throws Exception
    {
        try
        {
            mOperation = new ComponentOperation(mCounter, null,
                ComponentOperation.STARTUP);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Unexpected exception received: " + ex.toString(),
                (-1 < ex.getMessage().indexOf("Null argument")));
        }
    }

    /**
     * Tests getComponent.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetComponent()
        throws Exception
    {
        mOperation = new ComponentOperation(mCounter, mBinding,
            ComponentOperation.STARTUP);
        // Verification
        assertSame("Wrong result received: ",
            mBinding, mOperation.getComponent());
    }

    /**
     * Tests getOperation.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetOperation()
        throws Exception
    {
        mOperation = new ComponentOperation(mCounter, mBinding,
            ComponentOperation.STARTUP);
        // Verification
        assertEquals("Wrong result received: ",
            new Integer(ComponentOperation.STARTUP),
            new Integer(mOperation.getOperation()));
    }
 	 
    /**
     * Tests an INITIALIZE operation on a binding component.
     * @throws Throwable if an unexpected error occurs.
     */
    public void testProcessInitializeBinding()
        throws Throwable
    {
        Object bcInstance = mBinding.getLifeCycleInstance(true);
        mOperation = new ComponentOperation(mCounter, mBinding,
            ComponentOperation.INITIALIZE);

        mOperation.process(null);
        assertTrue("Failed to initialize binding",
            mBinding.isStopped());
    }

    /**
     * Tests a STARTUP operation on a binding component.
     * @throws Throwable if an unexpected error occurs.
     */
    public void testProcessStartupBinding()
        throws Throwable
    {
        mOperation = new ComponentOperation(mCounter, mBinding,
            ComponentOperation.STARTUP);

        mOperation.process(null);
        assertTrue("Failed to startup binding",
            mBinding.isStarted());
    }

    /**
     * Tests a SHUTDOWN operation on a binding component.
     * @throws Throwable if an unexpected error occurs.
     */
    public void testProcessShutdownBinding()
        throws Throwable
    {
        mCompFW.startComponent(mBinding);
        mOperation = new ComponentOperation(mCounter, mBinding,
            ComponentOperation.SHUTDOWN);

        mOperation.process(null);
        assertTrue("Failed to shutdown binding",
            mBinding.isInstalled());
    }

    /**
     * Tests an INITIALIZE operation on a service engine.
     * @throws Throwable if an unexpected error occurs.
     */
    public void testProcessInitializeEngine()
        throws Throwable
    {
        
        Object seInstance = mEngine.getLifeCycleInstance(true);

        mOperation = new ComponentOperation(mCounter, mEngine,
            ComponentOperation.INITIALIZE);

        mOperation.process(null);
        assertTrue("Failed to initialize engine",
            mEngine.isStopped());
    }

    /**
     * Tests a STARTUP operation on a service engine.
     * @throws Throwable if an unexpected error occurs.
     */
    public void testProcessStartupEngine()
        throws Throwable
    {
        mOperation = new ComponentOperation(mCounter, mEngine,
            ComponentOperation.STARTUP);

        mOperation.process(null);
        assertTrue("Failed to startup engine",
            mEngine.isStarted());
    }

    /**
     * Tests a SHUTDOWN operation on a service engine.
     * @throws Throwable if an unexpected error occurs.
     */
    public void testProcessShutdownEngine()
        throws Throwable
    {
        mCompFW.startComponent(mEngine);
        mOperation = new ComponentOperation(mCounter, mEngine,
            ComponentOperation.SHUTDOWN);

        mOperation.process(null);
        assertTrue("Failed to shutdown engine",
            mEngine.isInstalled());
    }
}
