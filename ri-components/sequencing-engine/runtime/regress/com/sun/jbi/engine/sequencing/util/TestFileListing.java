/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestFileListing.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.sequencing.util;

import junit.framework.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;


/**
 * DOCUMENT ME!
 *
 * @author Sun Microsystems, Inc.
 */
public class TestFileListing extends TestCase
{
    /**
     * Creates a new TestFileListing object.
     *
     * @param testName
     */
    public TestFileListing(java.lang.String testName)
    {
        super(testName);
    }

    /**
     * DOCUMENT ME!
     *
     * @return NOT YET DOCUMENTED
     */
    public static Test suite()
    {
        TestSuite suite = new TestSuite(TestFileListing.class);

        return suite;
    }

    /**
     * Test of getFileListing method, of class
     * com.sun.jbi.engine.sequencing.util.FileListing.
     */
    public void testGetFileListing()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of getFolderListing method, of class
     * com.sun.jbi.engine.sequencing.util.FileListing.
     */
    public void testGetFolderListing()
    {
        // Add your test code below by replacing the default call to fail.
    }

    // Add test methods here, they have to start with 'test' name.
    // for example:
    // public void testHello() {}
}
