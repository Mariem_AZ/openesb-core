#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)seq00005.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
echo "sequencingengine00005.ksh- Test MEPs - Sequencing engine test"

. ./regress_defs.ksh

# create test engine

cd $SEQUENCINGENGINE_BLD_DIR/testengine1
cp $SEQUENCINGENGINE_REGRESS_DIR/testengine1/*.xml .
cp -r $SEQUENCINGENGINE_REGRESS_DIR/testengine1/META-INF .
rm -rf CVS
rm -rf META-INF/CVS
mkdir testengine1
mv *.class testengine1
mv payload.xml testengine1
jar cvf testengine1.jar . 

cd $SEQUENCINGENGINE_BLD_DIR/testengine2
cp $SEQUENCINGENGINE_REGRESS_DIR/testengine2/*.xml .
cp -r $SEQUENCINGENGINE_REGRESS_DIR/testengine2/META-INF .
rm -rf CVS
rm -rf META-INF/CVS
mkdir testengine2
mv *.class testengine2
mv payload.xml testengine2
jar cvf testengine2.jar . 

# now install this test engine

$JBI_ANT -Djbi.install.file=$SEQUENCINGENGINE_BLD_DIR/testengine1/testengine1.jar install-component

$JBI_ANT -Djbi.install.file=$SEQUENCINGENGINE_BLD_DIR/testengine2/testengine2.jar install-component

# install the TE

#$AS8BASE/bin/asant -emacs -q -f $JBI_ADMIN_XML -Djbi.port=$JBI_ADMIN_PORT -Djbi.install.file=$JBI_KIT_TE install-component


#$AS8BASE/bin/asant -emacs -q -f $JBI_ADMIN_XML -Djbi.port=$JBI_ADMIN_PORT -Djbi.component.name=SunTransformationEngine start-component


# start out tests tests

$JBI_ANT -Djbi.component.name=TESTENGINE_ONE_FOR_SEQENGINE start-component

$JBI_ANT -Djbi.component.name=TESTENGINE_TWO_FOR_SEQENGINE start-component

sleep 5

echo
echo "Testing in-out with valid message"
grep "Test-SEQ-GOODOUTIN-Passed" $DOMAIN_LOG | wc -l
echo

$JBI_ANT -Djbi.component.name=TESTENGINE_ONE_FOR_SEQENGINE stop-component

$JBI_ANT -Djbi.component.name=TESTENGINE_ONE_FOR_SEQENGINE shut-down-component

$JBI_ANT -Djbi.component.name=TESTENGINE_ONE_FOR_SEQENGINE uninstall-component

$JBI_ANT -Djbi.component.name=TESTENGINE_TWO_FOR_SEQENGINE stop-component

$JBI_ANT -Djbi.component.name=TESTENGINE_TWO_FOR_SEQENGINE shut-down-component

$JBI_ANT -Djbi.component.name=TESTENGINE_TWO_FOR_SEQENGINE uninstall-component

#$AS8BASE/bin/asant -emacs -q -f $JBI_ADMIN_XML -Djbi.port=$JBI_ADMIN_PORT -Djbi.service.assembly.name=9ae51feb-0fc3-4ff6-9104-cfd59d2a7816 undeploy-service-assembly

#$AS8BASE/bin/asant -emacs -q -f $JBI_ADMIN_XML -Djbi.port=$JBI_ADMIN_PORT -Djbi.component.name=SunTransformationEngine stop-component

#$AS8BASE/bin/asant -emacs -q -f $JBI_ADMIN_XML -Djbi.port=$JBI_ADMIN_PORT -Djbi.component.name=SunTransformationEngine shut-down-component

#$AS8BASE/bin/asant -emacs -q -f $JBI_ADMIN_XML -Djbi.port=$JBI_ADMIN_PORT -Djbi.component.name=SunTransformationEngine uninstall-component

# stop shutdown uninstall

echo Completed Testing seq engine MEPs
