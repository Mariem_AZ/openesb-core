/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)InputFileFilter.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.sequencing.util;

import java.io.File;
import java.io.FilenameFilter;

import java.util.regex.Pattern;


/**
 * Filter class to select input files.
 *
 * @author Sun Microsystems, Inc.
 */
public class InputFileFilter
    implements FilenameFilter
{
    /**
     * Creates a new instance of InputFileFilter.
     */
    private Pattern mRegex = null;

    /**
     * Creates a new InputFileFilter object.
     */
    public InputFileFilter()
    {
    }

    /**
     * Sets the filter expression to be used. This is nothing but the regex
     * present in the config file.
     *
     * @param regex regular expression
     */
    public void setFilterexpression(String regex)
    {
        if ((regex == null) || regex.trim().equals(""))
        {
            mRegex = Pattern.compile(".*");
        }
        else
        {
            try
            {
                mRegex = Pattern.compile(regex);
            }
            catch (Exception e)
            {
                e.printStackTrace();
                mRegex = Pattern.compile(".*");
            }
        }
    }

    /**
     * Returns the filter expression.
     *
     * @return filter expression
     */
    public String getFilterexpression()
    {
        return mRegex.pattern();
    }

    /**
     * Returns true if the file needs to be included in the file list for the
     * directory dir.
     *
     * @param dir directory / file
     * @param name pattern
     *
     * @return true if included false otherwise
     */
    public boolean accept(
        File dir,
        String name)
    {
        File tmpFile = new File(name);
        String f = tmpFile.getName();

        /* Check if the file name matches the pattern specified
           in the configuration file. Also check if the file is
           is a file or a folder.
         */
        return (mRegex.matcher(f).matches());
    }
}
