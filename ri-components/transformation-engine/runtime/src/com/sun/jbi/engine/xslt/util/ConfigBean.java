/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ConfigBean.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.xslt.util;

import com.sun.jbi.engine.xslt.TEResources;

import java.util.Hashtable;
import java.util.Vector;
import java.util.logging.Logger;


/**
 * This Bean object holds all the attributes of an endpoint.
 *
 * @author Sun Microsystems, Inc.
 */
public class ConfigBean
    implements TEResources
{
    /**
     * Table for storing the xml tags and values  present in the config file.
     * This table stores  throws filebinding sepcific configuration
     * information correspondint to each endpoint.
     */
    private Hashtable mConfigTable = null;

    /**
     *    
     */
    private StringTranslator mTranslator = null;

    /**
     * Constructor. Creates a new Table.
     */
    public ConfigBean()
    {
        mTranslator =
            new StringTranslator("com.sun.jbi.engine.xslt",
                this.getClass().getClassLoader());
        mConfigTable = new Hashtable();
    }

    /**
     * DOCUMENT ME!
     *
     * @return NOT YET DOCUMENTED
     */
    public Hashtable getTable()
    {
        return mConfigTable;
    }

    /**
     * Sets the value for the key in the table.
     *
     * @param key for which value has to be retrieved.
     * @param value corresponding to the key
     */
    public void setValue(
        String key,
        String value)
    {
        if (key == null)
        {
            return;
        }

        if (value == null)
        {
            value = "";
        }

        Object obj = mConfigTable.get(key);

        if (obj == null)
        {
            Vector vec = new Vector();
            vec.addElement(value);
            mConfigTable.put(key, vec);
        }
        else
        {
            ((Vector) obj).addElement(value);
        }
    }

    /**
     * Returns the value associated with the key from the table.
     *
     * @param key the tag name for wcich value is required.
     *
     * @return value corresponding to the tag as in config file.
     */
    public Vector getValue(String key)
    {
        if (key == null)
        {
            return null;
        }

        return (Vector) mConfigTable.get(key);
    }
}
