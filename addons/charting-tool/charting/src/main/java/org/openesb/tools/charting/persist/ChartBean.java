/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ChartBean.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package org.openesb.tools.charting.persist;

import org.openesb.tools.extchart.property.ChartPropertyGroupsBeanBasic;

/**
 *
 * @author rdwivedi
 */
public class ChartBean {
    
    private String mCID = null;
    private String mCName = null;
    private String mCParentName = null;
    private String mCType = null;
    private ChartPropertyGroupsBeanBasic mChartProps = null;
    private String mDBID = null;
    private String mCParentGrpID = "133";
    
    /** Creates a new instance of ChartBean */
    public ChartBean() {
    }
    
    public void setChartName(String cn) {
        mCName = cn;
    }
    public void setChartParentName(String pn) {
        mCParentName = pn;
    }
    public String getParentID() {
        return mCParentGrpID;
    }
    
    public void setParentID(String id){
        mCParentGrpID = id;
    }
    public void setChartID(String id) {
        mCID = id;
    }
    
    public void createNewChartID() {
        mCID = "_c" + System.currentTimeMillis();
    }
    public void setChartDataBeanID(String d) {
        mDBID = d;
    }
    
    public String getChartDataBeanID() {
        return mDBID;
    }
    public void setChartProperties(ChartPropertyGroupsBeanBasic d) {
        mChartProps = d;
    }
    public String getChartID() {
        return mCID;
    }
    public String getChartName() {
        return mCName;
    }
    public String getChartType() {
        return mCType;
    }
    public String getChartParentName() {
        return mCParentName;
    }
    
    public ChartPropertyGroupsBeanBasic getChartProperties() {
        if(mChartProps == null && mCType!=null ) {
            setChartPropertiesForDataset(null,mCType);
        }
        return mChartProps;
    }

    public void setChartPropertiesForDataset(String dsType, String cType) {
        mCType = cType;
        ChartPropertyGroupsBeanBasic g = new ChartPropertyGroupsBeanBasic();
        g.repopulate(cType);
        this.setChartProperties(g);
    }
    
    public void resetChartType(String cType) {
       mCType = cType;
       ChartPropertyGroupsBeanBasic g = this.getChartProperties();
       if(g==null){
           
          g = new ChartPropertyGroupsBeanBasic();
       }
       g.repopulate(cType); 
       this.setChartProperties(g);
    }
    //public void setChartType(String cType) {
    //     mCType = cType;
    //}
    
}
