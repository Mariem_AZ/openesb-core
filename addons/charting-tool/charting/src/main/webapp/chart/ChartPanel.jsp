<?xml version="1.0" encoding="UTF-8"?>
<jsp:root version="1.2" xmlns:extchart="http://open-esb.dev.java.net/extchart/extchart" 
    xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html"
    xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:ui="http://www.sun.com/web/ui">
    <jsp:directive.page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"/>
    <f:view>
        <ui:page binding="#{chart$ChartPanel.page1}" id="page1">
            <ui:html binding="#{chart$ChartPanel.html1}" id="html1">
                <ui:head binding="#{chart$ChartPanel.head1}" id="head1">
                    <ui:link binding="#{chart$ChartPanel.link1}" id="link1" url="/resources/stylesheet.css"/>
                </ui:head>
                <ui:body binding="#{chart$ChartPanel.body1}" id="body1" style="-rave-layout: grid">
                    <ui:form binding="#{chart$ChartPanel.form1}" id="form1" target="workspaceFrame" >
                        <ui:label binding="#{chart$ChartPanel.label1}" id="label1" text="Chart Type"/>
                        <ui:dropDown action="#{chart$ChartPanel.submit_action}" binding="#{chart$ChartPanel.dropDown1}" id="dropDown1" items="#{chart$ChartPanel.options.options}"/>
                        <ui:button  action="#{chart$ChartPanel.button1_action}" binding="#{chart$ChartPanel.button1}" id="button1" text="Refresh"/>
                        <ui:button  action="#{chart$ChartPanel.button2_action}" binding="#{chart$ChartPanel.button2}" id="button2" text="Save"/>
                        </ui:form>
                        <extchart:extchart dataAccess="#{ChartApplicationBeansContainer.dataAccessBeanForCurrentChart.dataAccessObject}" id="id1" propertyGroups="#{ChartApplicationBeansContainer.currentChartBeanOnEditor.chartProperties}"/>
                </ui:body>
            </ui:html>
        </ui:page>
    </f:view>
</jsp:root>
