/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)PropertyValueConvertor.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package org.openesb.tools.extpropertysheet.ui.convertor;

import org.openesb.tools.extpropertysheet.IExtProperty;

/**
 *
 * @author rdwivedi
 */
public class PropertyValueConvertor {
    
    /** Creates a new instance of PropertyValueConvertor */
    public PropertyValueConvertor() {
    }
    
    public static Convertor getValueConvertor(short mPropType) {
        Convertor convertor = null;
    
       
            switch (mPropType) {
                case IExtProperty.BOOLEAN_RADIO_BUTTON:  convertor = new BooleanConvertor();
                    break;
                case IExtProperty.FONT_TYPE:  convertor = new FontConvertor();    
                    break;    
                case IExtProperty.COLOR_SELECTOR:  convertor = new ColorConvertor();    
                    break;
                default:  convertor = new StringConvertor();
                     break;
            }
        
        return convertor;
  
    }
}
