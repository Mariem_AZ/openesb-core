#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)HOW-TO.txt
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
This file documents common development tasks required to update and
ensure the integrity of the package tests.

First, make sure that you are building based on the correct version of the appserver.
This is the case if you have run a full build that has updated the glassfish install
with bundled jbi components:

    % runjbiBuild -update [-nojregress -nojavadoc]

Alternatively:

    % cd $SRCROOT
    % pull  (update your tools)
    % installas8  (only need this if the bundle has changed)
    % smvn clean
    % smvn

Now you are ready to run the package tests:

    % cd $SRCROOT/esb-test/package-tests/regress
    % jregress

Tests packaging00001 & packaging00003 should always pass - they just copy
$AS8BASE/jbi and $MAVEN_REPOSITORY/open-esb and expand the jars.

The tests packaging00010 and packaging00012 create a directory listing of
the expanded jars and then compare against the reference versions in
the testdat subdirectory.

Thus, if you have reviewed the diffs (in ../bld/*.out) and approve the changes,
you can update the tests by simply copying the output files into testdat:

    % chmod +w testdat/*toc.txt
    % cp ../bld/*toc.txt testdat

Then re-run the diff tests to verify the changes:

    % jregress packaging00010 packaging00012

You do not need to re-run the set-up tests unless you modify the source code
and re-run the top build.

RT 2/12/07
