#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbienv.csh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
#
# WARNING - THIS FILE IS GENERATED.  DO NOT MODIFY
#

setenv JBI_PS "@path.separator@"

#note - csh varibles are limited in length to 20.
setenv AS_INSTALL "@com.sun.aas.installRoot@"
setenv AS_JMX_REMOTE_URL "service:jmx:rmi:///jndi/rmi://@com.sun.aas.hostName@:@com.sun.jbi.management.JmxRmiPort@/management/rmi-jmx-connector"
setenv JBI_HOME "@com.sun.jbi.home@"
setenv JBI_INSTANCE_NAME "@com.sun.aas.instanceName@"
setenv JBI_ADMIN_PORT "@com.sun.jbi.management.JmxRmiPort@"
setenv JBI_ADMIN_HOST "@com.sun.aas.hostName@"
setenv JBI_DOMAIN_ROOT "@com.sun.jbi.domain.root@"
setenv JBI_DOMAIN_DIR "@com.sun.aas.domainsRoot@"
setenv JBI_DOMAIN_NAME "@com.sun.jbi.domain.name@"
setenv JBI_HADAPTOR_PORT "@com.sun.jbi.management.HtmlAdaptorPort@"
setenv JBI_DOMAIN_PROPS "$JBI_DOMAIN_ROOT/jbi/config/jbienv.properties"
setenv JBI_DOMAIN_STARTED "$JBI_DOMAIN_ROOT/jbi/tmp/.jbi_admin_running"
setenv JBI_DOMAIN_STOPPED "$JBI_DOMAIN_ROOT/jbi/tmp/.jbi_admin_stopped"

#setup classpath:
setenv JBI_CLASSPATH ""
foreach jar ( $JBI_HOME/lib/*.jar )
    if ( "$JBI_CLASSPATH" == "" ) then
        setenv JBI_CLASSPATH "$jar"
    else
        setenv JBI_CLASSPATH "$JBI_CLASSPATH${JBI_PS}$jar"
    endif
end
