@echo off
REM Copyright 2004 Sun Microsystems, Inc.  All rights reserved.
REM Use is subject to license terms.
REM
setlocal
java -classpath @com.sun.aas.installRoot@/lib/appserv-rt.jar;@com.sun.aas.installRoot@/lib/j2ee.jar;@com.sun.aas.installRoot@/lib/jmxremote.jar;@com.sun.aas.installRoot@/lib/jmxremote_optional.jar;@com.sun.jbi.home@/lib/jbi_rt.jar %JBIADMIN_SYSTEM_PROPERTIES%  com/sun/jbi/ui/admin/cli/JMXCLI --definitionfile @com.sun.jbi.home@/config/jbicli.xml --defaultport @com.sun.jbi.management.JmxRmiPort@ %*
endlocal
cmd /c exit %errorlevel%

